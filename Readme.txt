Steps to follow:

set_paths
set_params
launch_scatters()
launch_constants(1, 'sep')
launch_beamsum(1, 'sep')
adder
image_plot
plotImage_allInOne
raw_CNR


Random commands:
export_fig('cyst_3_harmonic_sep_comp_fixed_9_bf_ap_32_51', '-png', '-r200', '-transparent');

imagesc(squeeze(frame_3(1:640,1:136)), [-1 1]*100)