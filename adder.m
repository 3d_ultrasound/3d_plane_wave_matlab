function adder()
%%%%%%%%%% Initial Setup  %%%%%%%%%%
paths = set_paths();

load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'cyst_params');

load(sprintf('%s/img_data_firing_1.mat', paths.image), 'sub_image');
undemod_total_frame = sub_image;

num_firings = sys_params.num_angles;

for sub_ap = 2:num_firings
% for sub_ap = 2:5
  load(sprintf('%s/img_data_firing_%d.mat', paths.image, sub_ap), 'sub_image');
  undemod_total_frame = undemod_total_frame + sub_image;
end

%%%%%%%%%% Demodulation  %%%%%%%%%%
load(sprintf('%s/global_params.mat', paths.params), 'phys_params');

c = phys_params.c;
fc = phys_params.fc;
range_resolution = phys_params.lambda/(2*phys_params.fs/phys_params.interp_factor/phys_params.fc);
Ts_line = range_resolution*2/c;
fs_line = 1 / Ts_line;


demodulation_carrier =  repmat(exp(2j*pi*fc*(0:Ts_line:(size(undemod_total_frame,1)-1)*Ts_line)).', ...
    [1, size(undemod_total_frame,2), size(undemod_total_frame,3)]);
% Filter design
% d = fdesign.lowpass('Fp,Fst,Ap,Ast', phys_params.B_6db/2, fc*2-phys_params.B_6db/2, 1, 40, fs_line);
d = fdesign.lowpass('Fp,Fst,Ap,Ast', phys_params.B_6db/2, fc, 1, 40, fs_line);
hd = design(d,'kaiserwin');
b_LPF = hd.Numerator;

% Multiply with carrier
demodulated = undemod_total_frame.*demodulation_carrier;

% Low pass filtering
total_frame = filter(b_LPF, 1, demodulated);

total_frame = abs(total_frame);

save(sprintf('%s/img_complete.mat', paths.image), 'total_frame');
save(sprintf('%s/img_complete_type_%s_%d_angles_%d_bf_ap_%d.mat', paths.image, cyst_params.type, sys_params.num_angles, cyst_params.cyst_num, sys_params.sub_size_x), 'total_frame');
