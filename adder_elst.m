function adder_elst(num_firings, num_angles, overlap)
%%%%%%%%%% Initial Setup  %%%%%%%%%%
paths = set_paths();

if num_angles == 1 && overlap < 0 % no compounding
  num_cpd_frame = floor(abs(num_firings/overlap));
  for cpd_idx = 1:num_cpd_frame
    sub_frame_idx = (cpd_idx-1)*abs(overlap) + 1;
    load(sprintf('%s/img_data_firing_%d.mat', paths.image, sub_frame_idx), 'sub_image');
    cpd_frame = sub_image;
    save(sprintf('%s/cpd_image_%d.mat', paths.image, cpd_idx), 'cpd_frame');
  end
else
  num_cpd_frame = floor(num_firings/(num_angles - overlap));
  for cpd_idx = 1:num_cpd_frame
    sub_frame_set = (cpd_idx-1)*(num_angles - overlap) + (1:num_angles);
    for sub_frame_idx = sub_frame_set
      if sub_frame_idx == sub_frame_set(1)
        load(sprintf('%s/img_data_firing_%d.mat', paths.image, sub_frame_idx), 'sub_image');
        cpd_frame = sub_image;
      else
        load(sprintf('%s/img_data_firing_%d.mat', paths.image, sub_frame_idx), 'sub_image');
        cpd_frame = cpd_frame + sub_image;
      end
    end
    save(sprintf('%s/cpd_image_%d.mat', paths.image, cpd_idx), 'cpd_frame');
  end
end

