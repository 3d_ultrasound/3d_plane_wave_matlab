function generate_beamsum_plain_wave_compressed_2d(firing_id)

addpath('./param_friends');
%%%%%%%%%% Initial Setup  %%%%%%%%%%
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'phys_params');

fc = phys_params.fc;
fs = phys_params.fs;
Ts = phys_params.Ts;
interp_factor = phys_params.interp_factor;

c = phys_params.c;

B_6db = phys_params.B_6db;
Ts_A2D = interp_factor*Ts;

clear phys_params;

%%%%%%%%%% Load data %%%%%%%%%%%%%%%%
load(sprintf('%s/raw_data_firing_%d.mat', paths.echo, firing_id), 'rx_signal', 'rx', 'tx');

%%%%%%%%%% Create Response %%%%%%%%%%

sgm_sq = 3/5/((pi*B_6db)^2*log10(exp(1))); % impulse response
T_sig = 2*ceil(sqrt(6*sgm_sq/log10(exp(1)))*fc)/fc;
t = 0:Ts:T_sig;
h_g = 1e3*exp(-(t-T_sig/2).^2/(2*sgm_sq)); % baseband response

carrier = cos(2*pi*fc*t);
impulse_response = h_g .* carrier;

pulse_excitation = cos(2*pi*fc* ( 0 : (1/fs):(1/fc)) );
excitation_waveform = conv(pulse_excitation, impulse_response);

filter_delay = length(excitation_waveform);

%%%%%%%%%% Interpolation  %%%%%%%%%%

load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params', 'sep_params');

% Time gain control added
if img_params.enable_time_gain == true && img_params.atten_flag == true
    ref_depth = 0.02; % Unit [m], Reference depth, where the time gain is 0 dB
    n_ref = round(ref_depth*2/(Ts_A2D*c));
    n_idx = (1:size(rx_signal, 1)).';
    step_gain = img_params.atten_value*100/1e6 * Ts*interp_factor * c * fc;
    TGC_vec = 10.^((n_idx-n_ref)*step_gain/20);
    rx_signal = rx_signal .* repmat(TGC_vec, 1, size(rx_signal, 2));
end

if isfield(img_params,'enable_freespace_gain') && img_params.enable_freespace_gain == true
    FSG_vec = n_idx / n_ref;
    rx_signal = rx_signal .* repmat(FSG_vec, 1, size(rx_signal, 2));
end

if interp_factor ~= 1
    
    b_INT = [1:interp_factor interp_factor-1:-1:1].'/interp_factor;
    filter_delay = filter_delay + ceil(length(b_INT)/2);
    
    upsampling = zeros(size(rx_signal).*[interp_factor, 1]);
    upsampling(1:interp_factor:end, :) = rx_signal;
    interp_sig = filter(b_INT, 1, upsampling);
    
else
    interp_sig = rx_signal;
end

clear rx_signal;

%%%%%%%%%% Create Image Space  %%%%%%%%%%
[focus_z, focus_x, focus_y] = get_focus(img_params, sys_params);
num_scanline_x = size(focus_x, 2);
num_scanline_y = size(focus_y, 3);

switch img_params.win_type
    case 'rectwin'
        win_handle = @rectwin;
    case 'hamming'
        win_handle = @hamming;
    case 'blackman'
        win_handle = @blackman;
    case 'kaiser40'
        win_handle = @(N)kaiser(N, 4.0);
end
win_x = win_handle(sys_params.sub_size_x);
win_y = win_handle(sys_params.sub_size_y);
apd_win = win_x * win_y';

undemod_total_frame = zeros(size(focus_z));

delay_struc = load(sprintf('%s/2d_delay_comp_tau_firing_%d.mat', paths.delay, firing_id));

tau_tx1 = delay_struc.tau_tx1;
tau_rx_tx2 = delay_struc.tau(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2),:,:);

for scanline_y = 1:num_scanline_y
    % set which transducers affect this scanline in y-dim
    tranducer_y_range(1) = max(1, round(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y/2));
    tranducer_y_range(2) = min(sys_params.rx_size_y, max(1, round(scanline_y/img_params.lateral_interp_rate) - 1 + floor(sys_params.sub_size_y/2)));

    for scanline_x = 1:num_scanline_x
        % set the offset for this scanline (this is the plane angle
        % information for the delays)
        tx1_scanline_offset = tau_tx1(scanline_y, scanline_x);
        
        % set which transducers affect this scanline in x-dim
        tranducer_x_range(1) = max(1, round(scanline_x/img_params.lateral_interp_rate) - sys_params.sub_size_x /2);
        tranducer_x_range(2) = min(sys_params.rx_size_x, max(1, round(scanline_x/img_params.lateral_interp_rate) - 1 + sys_params.sub_size_x /2));
        
        for y_idx = tranducer_y_range(1):tranducer_y_range(2)
            % set the y-dim index of the beamforming aperture
            elem_y_idx = y_idx - (round(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y/2)) + 1;
            
            for x_idx = tranducer_x_range(1):tranducer_x_range(2)
                % set the input data index for this transducer
                rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;
                
                % set the x-dim index of the beamforming aperture
                elem_x_idx = x_idx - (round(scanline_x/img_params.lateral_interp_rate) - sys_params.sub_size_x /2) + 1;
                
                % create the delay vector for this scanline/data channel
                tau = round(((tau_rx_tx2(:, elem_y_idx, elem_x_idx) + tx1_scanline_offset)*interp_factor)) + filter_delay;
                
                % select the relevant data points
                undemod_total_frame(:, scanline_x, scanline_y) = undemod_total_frame(:, scanline_x, scanline_y) + ...
                    apd_win(elem_x_idx, elem_y_idx)*interp_sig(tau, rx_idx);
            end
        end
    end
end

sub_image = undemod_total_frame;
save(sprintf('%s/img_data_firing_%d.mat', paths.image, firing_id), 'sub_image');
