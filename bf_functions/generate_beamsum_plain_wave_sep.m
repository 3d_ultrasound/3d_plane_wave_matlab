function generate_beamsum_plain_wave_sep(firing_id)

%%%%%%%%%% Initial Setup  %%%%%%%%%%
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'phys_params');

fc = phys_params.fc;
Tc = phys_params.Tc;
fs = phys_params.fs;
Ts = phys_params.Ts;
interp_factor = phys_params.interp_factor;

lambda = phys_params.lambda;
c = phys_params.c;

B_6db = phys_params.B_6db;
Ts_A2D = interp_factor*Ts;

clear phys_params;

load(sprintf('%s/raw_data_firing_%d.mat', paths.echo, firing_id), 'rx_signal', 'rx', 'tx');

%%%%%%%%%% Create Response %%%%%%%%%%

sgm_sq = 3/5/((pi*B_6db)^2*log10(exp(1))); % impulse response
T_sig = 2*ceil(sqrt(6*sgm_sq/log10(exp(1)))*fc)/fc;
t = 0:Ts:T_sig;
h_g = 1e3*exp(-(t-T_sig/2).^2/(2*sgm_sq)); % baseband response

carrier = cos(2*pi*fc*t);
impulse_response = h_g .* carrier;

filter_delay = round(length(impulse_response)*3/2);

%%%%%%%%%% Interpolation  %%%%%%%%%%

load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params', 'sep_params');

% Time gain control added
if img_params.enable_time_gain == true && img_params.atten_flag == true
  ref_depth = 0.02; % Unit [m], Reference depth, where the time gain is 0 dB
  n_ref = round(ref_depth*2/(Ts_A2D*c));
  n_idx = (1:size(rx_signal, 1)).';
  step_gain = img_params.atten_value*100/1e6 * Ts*interp_factor * c * fc;
  TGC_vec = 10.^((n_idx-n_ref)*step_gain/20);
  rx_signal = rx_signal .* repmat(TGC_vec, 1, size(rx_signal, 2));
end

if isfield(img_params,'enable_freespace_gain') && img_params.enable_freespace_gain == true
  FSG_vec = n_idx / n_ref;
  rx_signal = rx_signal .* repmat(FSG_vec, 1, size(rx_signal, 2));
end

if interp_factor ~= 1

  b_INT = [1:interp_factor interp_factor-1:-1:1].'/interp_factor;
  filter_delay = filter_delay + ceil(length(b_INT)/2);

  upsampling = zeros(size(rx_signal).*[interp_factor, 1]);
  upsampling(1:interp_factor:end, :) = rx_signal;
  interp_sig = filter(b_INT, 1, upsampling);

else
  interp_sig = rx_signal;
end

clear rx_signal;

%%%%%%%%%% Apodization  %%%%%%%%%%


switch img_params.win_type
  case 'rectwin'
    win_handle = @rectwin;
  case 'hamming'
    win_handle = @hamming;
  case 'blackman'
    win_handle = @blackman;
  case 'kaiser40'
    win_handle = @(N)kaiser(N, 4.0);
end
apd_win_x = win_handle(sys_params.sub_size_x);
apd_win_y = win_handle(sys_params.sub_size_y);

%%%%%%%%%%  Separable delay %%%%%%%%%%%%%%%%%
angle_id = mod(firing_id-1, sys_params.num_angles)+1;

num_scanline_x = sys_params.rx_size_x*img_params.lateral_interp_rate;
num_scanline_y = sys_params.rx_size_x*img_params.lateral_interp_rate;

sep_delay_struc = load(sprintf('%s/sep_delay_tau_firing_%d.mat', paths.delay, angle_id));
switch sep_params.sep_method
  case 'ref'
    tau_a = sep_delay_struc.tau_a_baseline(:, :, 1);
    tau_b = sep_delay_struc.tau_b_baseline(:, 1, :);
  case 'min_rms'
    tau_a = sep_delay_struc.tau_a;
    tau_b = sep_delay_struc.tau_b;
%     focus_z = sep_delay_struc.focus_z;
  otherwise
    error(['Method ' sep_params.sep_method ' is not supported by current function.!']);
end
clear sep_delay_struc;

stg1_pt = [sep_params.sep_z_pt(1)-sep_params.sep_phase1_beamsum_cusion(1), sep_params.sep_z_pt(2)+sep_params.sep_phase1_beamsum_cusion(2)];
partial_beamsum = zeros(stg1_pt(2)-stg1_pt(1)+1, num_scanline_x, sys_params.rx_size_y);

if sys_params.enable_fixed_f_num == false
%   for scanline_y = 1:sys_params.rx_size_y
%     for scanline_x = 1:sys_params.rx_size_x
%       tranducer_x_range(1) = max(1, scanline_x - sys_params.sub_size_x /2);
%       tranducer_x_range(2) = min(sys_params.rx_size_x, scanline_x - 1 + sys_params.sub_size_x /2);
%
%       temp = tau_a{scanline_x,scanline_y}(stg1_pt(1):stg1_pt(2), :, :); % ADDED
%       test = tau_a{scanline_x,scanline_y}(stg1_pt(1):stg1_pt(2), :, :) + delay_ref_tau_a(:,1:size(temp,2)); % ADDED
%       tau_a_idx = round((temp + delay_ref_tau_a(:,1:size(temp,2)))*interp_factor) + filter_delay; % ADDED
% %       tau_a_idx = round((tau_a{scanline_x,scanline_y}(stg1_pt(1):stg1_pt(2), :, :) + delay_ref_tau_a)*interp_factor) + filter_delay;
%       for x_idx = tranducer_x_range(1):tranducer_x_range(2)
%         elem_x_idx = x_idx - (scanline_x - sys_params.sub_size_x /2) + 1;
%         rx_idx = x_idx + (scanline_y-1)*sys_params.rx_size_x;
%         partial_beamsum(:, scanline_x, scanline_y) = partial_beamsum(:, scanline_x, scanline_y) + apd_win_x(elem_x_idx) * ...
%           interp_sig(tau_a_idx(:, x_idx, 1), rx_idx);
%       end
%     end
%   end
  for y_idx = 1:sys_params.rx_size_y % for each receive ap transducer in y-dim
    for x_idx = 1:sys_params.rx_size_x % for each receive ap transducer in x-dim
      scanline_x_range(1) = max(1, (x_idx - floor(sys_params.sub_size_x /2) + 1)*img_params.lateral_interp_rate);
      scanline_x_range(2) = min(num_scanline_x, (x_idx + floor(sys_params.sub_size_x /2))*img_params.lateral_interp_rate);

      tau_a_idx_sep = tau_a{x_idx,y_idx}; % ADDED
      tau_a_idx_sep = round((tau_a_idx_sep)*interp_factor) + filter_delay; % ADDED

      rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;

      for scanline_x = scanline_x_range(1):scanline_x_range(2) % for each scanline within the beamforming ap
        elem_x_idx = x_idx - (floor(scanline_x/img_params.lateral_interp_rate) - floor(sys_params.sub_size_x /2)) + 1;

        partial_beamsum(:, scanline_x, y_idx) = partial_beamsum(:, scanline_x, y_idx) + apd_win_x(elem_x_idx) * ...
          interp_sig(tau_a_idx_sep(:, scanline_x), rx_idx); % for each bf ap scanline,
      end
    end
  end
else
%   for y_idx = 1:sys_params.rx_size_y % for each receive ap transducer in y-dim
%     for x_idx = 1:sys_params.rx_size_x % for each receive ap transducer in x-dim
%       rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;
%       scanline_x_range(1) = max(1,  (x_idx - sys_params.sub_size_x /2 + 1)*img_params.lateral_interp_rate);
%       scanline_x_range(2) = min(num_scanline_x,  (x_idx + sys_params.sub_size_x /2)*img_params.lateral_interp_rate);
%       tau_a_idx = iter_sep_delay_calc(coef_tau_a_struct.coef_struct, [x_idx, y_idx]);
%       tau_a_idx = round(tau_a_idx*interp_factor + filter_delay);
%       for scanline_x = scanline_x_range(1):scanline_x_range(2) % for each scanline in the beamforming ap
%         elem_x_idx = x_idx - (floor(scanline_x/img_params.lateral_interp_rate) - sys_params.sub_size_x /2) + 1;
%         ap_size = 2*(sys_params.rx_width+sys_params.rx_kerf)*abs(x_idx - round(scanline_x/img_params.lateral_interp_rate));
%         start_dist = ap_size*sys_params.fixed_f_num;
%         start_idx = max(find(abs(sep_focus_z) >= start_dist, 1, 'first') - stg1_pt(1)+1, 1);
%         partial_beamsum(start_idx:end, scanline_x, y_idx) = partial_beamsum(start_idx:end, scanline_x, y_idx) + apd_win_x(elem_x_idx) * ...
%           interp_sig(tau_a_idx(start_idx:end, scanline_x, 1), rx_idx); %
%       end
%     end
%   end
end

if interp_factor ~= 1

  b_INT = [1:interp_factor interp_factor-1:-1:1].'/interp_factor;

%   d = fdesign.lowpass('Fp,Fst,Ap,Ast', (phys_params.B_6db/2 + fc) / (interp_factor*fs), (fs - fc)/(interp_factor*fs), 0.1, 40);
%   hd = design(d,'kaiserwin');
%   b_INT = hd.Numerator;

  upsampling = zeros(size(partial_beamsum).*[interp_factor, 1, 1]);
  upsampling(1:interp_factor:end, :, :) = partial_beamsum;
  interp_partial_beamsum = filter(b_INT, 1, upsampling);

else
  interp_partial_beamsum = partial_beamsum;
end



sub_image = zeros(sep_params.sep_z_pt(2)-sep_params.sep_z_pt(1)+1, num_scanline_x, num_scanline_y);

if sys_params.enable_fixed_f_num == false
  for y_idx = 1:sys_params.rx_size_y
    scanline_y_range(1) = max(1, (y_idx - floor(sys_params.sub_size_y /2) + 1)*img_params.lateral_interp_rate);
    scanline_y_range(2) = min(num_scanline_y, (y_idx + floor(sys_params.sub_size_y /2))*img_params.lateral_interp_rate);

    tau_b_idx_sep_1 = squeeze(tau_b{y_idx}); % ADDED
    tau_b_idx_sep = round((tau_b_idx_sep_1)*interp_factor); % ADDED

    for scanline_x = 1:num_scanline_x
      for scanline_y = scanline_y_range(1):scanline_y_range(2)
        elem_y_idx = y_idx - (floor(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y /2)) + 1;

        sub_image(:, scanline_x, scanline_y) = sub_image(:, scanline_x, scanline_y) + apd_win_y(elem_y_idx) * ...
          interp_partial_beamsum(tau_b_idx_sep(:, scanline_x, scanline_y), scanline_x, y_idx);
      end
    end
  end
else
%   focus_z_comp = sep_focus_z(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2));
%   for y_idx = 1:sys_params.rx_size_y
%     scanline_y_range(1) = max(1,  (y_idx - sys_params.sub_size_y /2 + 1)*img_params.lateral_interp_rate);
%     scanline_y_range(2) = min(num_scanline_y,  (y_idx + sys_params.sub_size_y /2)*img_params.lateral_interp_rate);
%     tau_b_idx = iter_sep_delay_calc(coef_tau_b_struct.coef_struct, y_idx);
%     tau_b_idx = round(tau_b_idx*interp_factor);
%     for scanline_x = 1:num_scanline_x
%       for scanline_y = scanline_y_range(1):scanline_y_range(2)
%         ap_size = 2*(sys_params.rx_height+sys_params.rx_kerf)*abs(y_idx - round(scanline_y/img_params.lateral_interp_rate));
%         start_dist = ap_size*sys_params.fixed_f_num;
%         start_idx = max(find(abs(focus_z_comp) >= start_dist, 1, 'first'), 1);
%
%         elem_y_idx = y_idx - (floor(scanline_y/img_params.lateral_interp_rate) - sys_params.sub_size_y /2) + 1;
%         sub_image(start_idx:end, scanline_x, scanline_y) = sub_image(start_idx:end, scanline_x, scanline_y) + apd_win_y(elem_y_idx) * ...
%           interp_partial_beamsum(tau_b_idx(start_idx:end, scanline_x, scanline_y), scanline_x, y_idx);
%       end
%     end
%   end
end

save(sprintf('%s/img_data_firing_%d.mat', paths.image, firing_id), 'sub_image');
