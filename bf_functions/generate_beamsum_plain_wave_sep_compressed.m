function generate_beamsum_plain_wave_sep_compressed(firing_id)

%%%%%%%%%% Initial Setup  %%%%%%%%%%
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'phys_params');

fc = phys_params.fc;
Tc = phys_params.Tc;
fs = phys_params.fs;
Ts = phys_params.Ts;
interp_factor = phys_params.interp_factor;

lambda = phys_params.lambda;
c = phys_params.c;

B_6db = phys_params.B_6db;
Ts_A2D = interp_factor*Ts;

clear phys_params;

load(sprintf('%s/raw_data_firing_%d.mat', paths.echo, firing_id), 'rx_signal', 'rx', 'tx');

%%%%%%%%%% Create Response %%%%%%%%%%

sgm_sq = 3/5/((pi*B_6db)^2*log10(exp(1))); % impulse response
T_sig = 2*ceil(sqrt(6*sgm_sq/log10(exp(1)))*fc)/fc;
t = 0:Ts:T_sig;
h_g = 1e3*exp(-(t-T_sig/2).^2/(2*sgm_sq)); % baseband response

carrier = cos(2*pi*fc*t);
impulse_response = h_g .* carrier;

filter_delay = round(length(impulse_response)*3/2);

%%%%%%%%%% Interpolation  %%%%%%%%%%

load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params', 'sep_params');

% Time gain control added
if img_params.enable_time_gain == true && img_params.atten_flag == true
  ref_depth = 0.02; % Unit [m], Reference depth, where the time gain is 0 dB
  n_ref = round(ref_depth*2/(Ts_A2D*c));
  n_idx = (1:size(rx_signal, 1)).';
  step_gain = img_params.atten_value*100/1e6 * Ts*interp_factor * c * fc;
  TGC_vec = 10.^((n_idx-n_ref)*step_gain/20);
  rx_signal = rx_signal .* repmat(TGC_vec, 1, size(rx_signal, 2));
end

if isfield(img_params,'enable_freespace_gain') && img_params.enable_freespace_gain == true
  FSG_vec = n_idx / n_ref;
  rx_signal = rx_signal .* repmat(FSG_vec, 1, size(rx_signal, 2));
end

if interp_factor ~= 1

  b_INT = [1:interp_factor interp_factor-1:-1:1].'/interp_factor;
  filter_delay = filter_delay + ceil(length(b_INT)/2);

  upsampling = zeros(size(rx_signal).*[interp_factor, 1]);
  upsampling(1:interp_factor:end, :) = rx_signal;
  interp_sig = filter(b_INT, 1, upsampling);

else
  interp_sig = rx_signal;
end

clear rx_signal;

%%%%%%%%%% Apodization  %%%%%%%%%%


switch img_params.win_type
  case 'rectwin'
    win_handle = @rectwin;
  case 'hamming'
    win_handle = @hamming;
  case 'blackman'
    win_handle = @blackman;
  case 'kaiser40'
    win_handle = @(N)kaiser(N, 4.0);
end
apd_win_x = win_handle(sys_params.sub_size_x);
apd_win_y = win_handle(sys_params.sub_size_y);

%%%%%%%%%%  Separable delay %%%%%%%%%%%%%%%%%
sep_focus_z = [sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2)]';
angle_id = mod(firing_id-1, sys_params.num_angles)+1;

% load the delay values
sep_comp_delay_struc = load(sprintf('%s/sep_delay_compressed_tau_firing_%d.mat', paths.delay, angle_id));
switch sep_params.sep_method
  case 'ref'
    % tau_1 = sep_delay_struc.tau_1_baseline(:, :, 1);
    % tau_2 = sep_delay_struc.tau_2_baseline(:, 1, :);
  case 'min_rms'
    tau_1 = sep_comp_delay_struc.tau_1;
    tau_2 = sep_comp_delay_struc.tau_2;
    tau_a_tx1_offsets = sep_comp_delay_struc.tau_a_tx1_offsets;
    tau_b_tx1_offsets = sep_comp_delay_struc.tau_b_tx1_offsets;
  otherwise
    error(['Method ' sep_params.sep_method ' is not supported by current function.!']);
end
clear sep_comp_delay_struc;


stg1_pt = [sep_params.sep_z_pt(1)-sep_params.sep_phase1_beamsum_cusion(1), sep_params.sep_z_pt(2)+sep_params.sep_phase1_beamsum_cusion(2)];
z_grid_stg1 = sep_focus_z(stg1_pt(1):stg1_pt(2), 1);
delay_ref_tau_1 = abs(z_grid_stg1)*2/(c*Ts_A2D);
delay_ref_tau_2 = abs(sep_focus_z(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2)))*2/((c*Ts_A2D)) - delay_ref_tau_1(1);

partial_beamsum = zeros(size(delay_ref_tau_1, 1), sys_params.rx_size_x, sys_params.rx_size_y);

for scanline_x = 1:sys_params.rx_size_x
    for scanline_y = 1:sys_params.rx_size_y
        % set which transducers affect this scanline in x-dim
        tranducer_x_range(1) = max(1, round(scanline_x/img_params.lateral_interp_rate) - floor(sys_params.sub_size_x/2));
        tranducer_x_range(2) = min(sys_params.rx_size_x, round(scanline_x/img_params.lateral_interp_rate) - 1 + floor(sys_params.sub_size_x/2));

        for x_idx = tranducer_x_range(1):tranducer_x_range(2)
            % set the input data index for this transducer
            rx_idx = x_idx + (scanline_y - 1)*sys_params.rx_size_x;

            % set the x-dim index of the beamforming aperture
            elem_x_idx = x_idx - (round(scanline_x/img_params.lateral_interp_rate) - floor(sys_params.sub_size_x/2)) + 1;

            % create the delay vector for this scanline/data channel
            tau_idx = round((tau_1(:, elem_x_idx)+tau_a_tx1_offsets(scanline_x, scanline_y))*interp_factor) + filter_delay;

            % select the relevant data points
            partial_beamsum(:, scanline_x, scanline_y) = partial_beamsum(:, scanline_x, scanline_y) + apd_win_x(elem_x_idx) * ...
                interp_sig(tau_idx, rx_idx);
        end
    end
end

if interp_factor ~= 1

  b_INT = [1:interp_factor interp_factor-1:-1:1].'/interp_factor;

%   d = fdesign.lowpass('Fp,Fst,Ap,Ast', (phys_params.B_6db/2 + fc) / (interp_factor*fs), (fs - fc)/(interp_factor*fs), 0.1, 40);
%   hd = design(d,'kaiserwin');
%   b_INT = hd.Numerator;

  upsampling = zeros(size(partial_beamsum).*[interp_factor, 1, 1]);
  upsampling(1:interp_factor:end, :, :) = partial_beamsum;
  interp_partial_beamsum = filter(b_INT, 1, upsampling);
  stg_2_interp_delay = length(b_INT);

else
  interp_partial_beamsum = partial_beamsum;
  stg_2_interp_delay = 1;
end



sub_image = zeros(size(delay_ref_tau_2, 1), sys_params.rx_size_x, sys_params.rx_size_y);

focus_z_comp = sep_focus_z(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2));

for scanline_x = 1:sys_params.rx_size_x
    for scanline_y = 1:sys_params.rx_size_y
        % set which transducers affect this scanline in y-dim
        tranducer_y_range(1) = max(1, round(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y/2));
        tranducer_y_range(2) = min(sys_params.rx_size_y, round(scanline_y/img_params.lateral_interp_rate) - 1 + floor(sys_params.sub_size_y/2));

        for y_idx = tranducer_y_range(1):tranducer_y_range(2)
            % set the y-dim index of the beamforming aperture
            elem_y_idx = y_idx - (round(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y/2)) + 1;

            % create the delay vector for this scanline/data channel
            tau_idx = round((tau_2(:, elem_y_idx)+tau_b_tx1_offsets(scanline_x, scanline_y, elem_y_idx))*interp_factor);

            % select the relevant data points
            sub_image(:, scanline_x, scanline_y) = sub_image(:, scanline_x, scanline_y) + apd_win_y(elem_y_idx) * ...
                interp_partial_beamsum(tau_idx, scanline_x, y_idx);
        end
    end
end

save(sprintf('%s/img_data_firing_%d.mat', paths.image, firing_id), 'sub_image');
