function [tau, varargout] = iter_sep_delay_calc(coef_struct, load_idx, varargin)

if isempty(varargin)
  fi_cfg.mode = 'sep_double';
else
  fi_cfg = varargin{1};
end


% % [DBG]
% fi_cfg.mode = 'sep_double';

model_type = coef_struct.model_type;
glb_pc_range = coef_struct.glb_pc_range;

pc_range = glb_pc_range;
pc_len = diff(pc_range);
num_pc = length(pc_len);

k = 1;
tau = zeros(pc_range(end), size(coef_struct.a{1}, 2), size(coef_struct.a{1}, 3));
switch model_type
  case 0
    % TBD
  case 1
    % TBD
  case 2
    if strcmpi(fi_cfg.mode, 'sep_double')
      for pc_idx = 1:num_pc
        if length(load_idx) == 1
          c = coef_struct.c{load_idx}(pc_idx, :, :);
          b = coef_struct.b{load_idx}(pc_idx, :, :);
          a = coef_struct.a{load_idx}(pc_idx, :, :);
          init_address = coef_struct.init_address{load_idx}(pc_idx, :, :);
        else
          c = coef_struct.c{load_idx(1),load_idx(2)}(pc_idx, :, :);
          b = coef_struct.b{load_idx(1),load_idx(2)}(pc_idx, :, :);
          a = coef_struct.a{load_idx(1),load_idx(2)}(pc_idx, :, :);
          init_address = coef_struct.init_address{load_idx(1),load_idx(2)}(pc_idx, :, :);
        end


        tau(k, :, :) = init_address;
        k = k + 1;

        aa = 2*a;

        reg_1 = c;
        reg_2 = a+b;
        for n = 1:pc_len(pc_idx)-1
          tau(k, :, :) = tau(k-1, :, :) + reg_1;
          reg_2 = reg_2 - aa;
          reg_1 = reg_1 + reg_2;
          k = k + 1;
        end
      end
    elseif strcmpi(fi_cfg.mode, 'sep_fixed')
      for pc_idx = 1:num_pc

        b_mat = cell2mat(coef_struct.b);
        max_b = max(max(b_mat(pc_idx, :, :)));
        if strcmpi(fi_cfg.b_q.mode, 'fixed')
          shift_b = floor(log2((2^(fi_cfg.b_q.format(1)-fi_cfg.b_q.format(2))- 2^(-fi_cfg.b_q.format(2))) / max_b))-1;
        else
          shift_b = floor(log2((2^(fi_cfg.b_q.format(1)-fi_cfg.b_q.format(2))- 2^(-fi_cfg.b_q.format(2))) / max_b));
        end
        c_mat = cell2mat(coef_struct.c);
        min_c = min(min(c_mat(pc_idx, :, :)));
        max_c = max(max(c_mat(pc_idx, :, :)));
        max_c = max(max_c, 1);
        shift_c = floor(log2((2^(fi_cfg.c_q.format(1)-fi_cfg.c_q.format(2)) - 2^(-fi_cfg.c_q.format(2))) /(max_c-min_c)));
        bias_c = floor(min_c*2^(fi_cfg.c_q.format(2)-(fi_cfg.c_q.format(1)-shift_c)))/...
          2^(fi_cfg.c_q.format(2)-(fi_cfg.c_q.format(1)-shift_c));

        c_q = quantizer(fi_cfg.c_q, 'format', fi_cfg.c_q.format + [0, shift_c]);
        b_q = quantizer(fi_cfg.b_q, 'format', fi_cfg.b_q.format + [0, shift_b]);
        a_q = quantizer(fi_cfg.a_q, 'format', fi_cfg.a_q.format + [0, shift_b]);
        aa_q = quantizer(fi_cfg.a_q, 'format', fi_cfg.aa_q.format + [0, shift_b]);

        reg_2_q = quantizer(fi_cfg.reg_2_q, 'format', fi_cfg.reg_2_q.format + [0, shift_b]);
        reg_1_q = quantizer(fi_cfg.reg_1_q, 'format', fi_cfg.reg_1_q.format + [0, shift_c]);

        if length(load_idx) == 1
          c = quantize(c_q, coef_struct.c{load_idx}(pc_idx, :, :) - bias_c);
          b = quantize(b_q, coef_struct.b{load_idx}(pc_idx, :, :));
          a = quantize(a_q, coef_struct.a{load_idx}(pc_idx, :, :));
          init_address = quantize(fi_cfg.init_address_q, coef_struct.init_address{load_idx}(pc_idx, :, :));
        else
          c = quantize(c_q, coef_struct.c{load_idx(1),load_idx(2)}(pc_idx, :, :) - bias_c);
          b = quantize(b_q, coef_struct.b{load_idx(1),load_idx(2)}(pc_idx, :, :));
          a = quantize(a_q, coef_struct.a{load_idx(1),load_idx(2)}(pc_idx, :, :));
          init_address = quantize(fi_cfg.init_address_q, coef_struct.init_address{load_idx(1),load_idx(2)}(pc_idx, :, :));
        end
        tau(k, :, :) = init_address;
        k = k + 1;

        aa = quantize(aa_q, 2*a);

        reg_1 = quantize(reg_1_q, c);
        reg_2 = quantize(reg_2_q, a+b);
        for n = 1:pc_len(pc_idx)-1
          tau(k, :, :) = quantize(fi_cfg.tau_q, tau(k-1, :, :) + reg_1 + bias_c);
          reg_2 = quantize(reg_2_q, reg_2 - aa);
          reg_1 = quantize(reg_1_q, reg_1 + reg_2);
          k = k + 1;
        end
      end
    else
      error('Unsupported fi mode');
    end
end

nout = nargout();
if nout > 1
  varargout{1} = fi_cfg;
end
