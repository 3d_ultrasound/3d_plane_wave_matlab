function sv_interp_stage_2(fi_cfg, input_data, firing_id)

paths = set_paths();

%%%%%%%%%% Initial Setup  %%%%%%%%%%
load(sprintf('%s/global_params.mat', paths.params), 'phys_params');

interp_factor = phys_params.interp_factor;

clear phys_params;

%%%%%%%%%% Interpolation  %%%%%%%%%%

input_data2 = fi(input_data, true, 12, fi_cfg.in_scale-2);
input_data2 = reinterpretcast(input_data2, numerictype(true, 12, 0));
input_data = quantize(fi_cfg.bf_stg1_out_q, input_data);

if interp_factor ~= 1
    upsampling = fi(zeros(size(input_data).*[interp_factor, 1, 1]), true, 12, fi_cfg.in_scale-4);
    upsampling = reinterpretcast(upsampling, numerictype(true, 12, 0));
    one_bit = input_data2(1, 1);
    one_bit.int = 1;
    for idx = 2:size(input_data2, 1)
        t = input_data2(idx, :, :);
        t_old = input_data2(idx-1, :, :);
        val1 = bitshift(bitshift(t, -2) + bitshift(t_old+one_bit, -2) + bitshift(t_old, -1), -2);
        val2 = bitshift(bitshift(t, -1) + bitshift(t_old, -1), -2);
        val3 = bitshift(bitshift(t+one_bit, -2) + bitshift(t, -1) +  bitshift(t_old, -2), -2);
        val4 = bitshift(t, -2);

        upsampling(((idx-1)*4)+1, :, :) = val1;
        upsampling(((idx-1)*4)+2, :, :) = val2;
        upsampling(((idx-1)*4)+3, :, :) = val3;
        upsampling(((idx-1)*4)+4, :, :) = val4;
    end
    upsampling = reinterpretcast(upsampling, numerictype(true, 12, fi_cfg.in_scale-4));
    upsampling = double(upsampling);
    upsampling = quantize(fi_cfg.interp_stg2_q, upsampling);
    interp_partial_beamsum = upsampling;
else
    interp_partial_beamsum = input_data;
end

clear input_data;
clear input_data2;

save(sprintf('%s/sv_interp_sig_stg2_firing_%d.mat', paths.echo, firing_id), 'interp_partial_beamsum');

end
