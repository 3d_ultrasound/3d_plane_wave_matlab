function compressed_2d_gen_plane_wave(firing_id)
% function iter_coef_sep_gen_3var(sub_ap_idx, coef_struct)
% Description:
%     Generate iterative delay coefficient for separable beamforming and
%     save in paths.iter_coef_sep_folder
% Input:
%     sub_ap_idx_x: subaperture x index ranges from 1 to 6 (half of the size due to symmety)
%     sub_ap_idx_y: subaperture y index ranges from 1 to 4 (half of the size due to symmety)
%     coef_struct_in.model_type: approximation model, currently only model_type = 2 (
%         quadratic approximation model is supported
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_a, this input has default value.
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_b, this input has default value.
% Output:
%     none

enable_coef_gen = true;

paths = set_paths();

coef_save_folder = paths.delay;

load_params = load(sprintf('%s/global_params.mat', paths.params), 'phys_params', 'img_params', 'sep_params', 'sys_params');
phys_params = load_params.phys_params;
img_params = load_params.img_params;
sep_params = load_params.sep_params;
sys_params = load_params.sys_params;

clear load_params;

Ts = phys_params.Ts;
Ts_A2D = Ts*phys_params.interp_factor;
c = phys_params.c;

rx = generate_receive();
Rx_coord = rx.rx_center;
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);

alpha = sys_params.firing_angle_alpha(firing_id);
beta = sys_params.firing_angle_beta(firing_id);
pin_point_coord = repmat(sys_params.pin_point_coord(firing_id, :), size(sep_focus_z, 1), 1);

% set space for outputs
d_tx1 = zeros(sys_params.rx_size_y,sys_params.rx_size_x);
tau = zeros(size(sep_focus_x,1),sys_params.sub_size_y,sys_params.sub_size_x);

% calculate the distance from the plane to the first focal point in each
% scanline
for scanline_y = 1:sys_params.rx_size_y
    for scanline_x = 1:sys_params.rx_size_x
        % select the scanline
        focus_line_tx_coord = [sep_focus_x(:, scanline_x, scanline_y), sep_focus_y(:, scanline_x, scanline_y), sep_focus_z(:, scanline_x, scanline_y)];

        % calculate the transmit distance to the first focal point
        d_tx1(scanline_y,scanline_x) = (focus_line_tx_coord(1,1)-pin_point_coord(1,1))*sin(alpha)*cos(beta)+(focus_line_tx_coord(1,2)-pin_point_coord(1,2))*sin(alpha)*sin(beta)-pin_point_coord(1,3)*cos(alpha);
    end
end


% select the scanline located at the middle of the beamforming aperture
focus_line_tx_coord = [sep_focus_x(:, (floor(sys_params.sub_size_x/2)+1), (floor(sys_params.sub_size_y/2)+1)), sep_focus_y(:, (floor(sys_params.sub_size_x/2)+1), (floor(sys_params.sub_size_y/2)+1)), sep_focus_z(:, (floor(sys_params.sub_size_x/2)+1), (floor(sys_params.sub_size_y/2)+1))];


for y_idx = 1:sys_params.sub_size_y
    for x_idx = 1:sys_params.sub_size_x
        % set index into Rx_coord to retreive transducer indices
        rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;

        % calculate the transmit distance from the first focal point to all
        % others
        d_tx2 = focus_line_tx_coord(:,3)*cos(alpha);

        % calculate the reflected distance from the first focal point to
        % the current transducer
        d_rx = sqrt((Rx_coord(rx_idx,1)-focus_line_tx_coord(:,1)).^2 + (Rx_coord(rx_idx,2)-focus_line_tx_coord(:,2)).^2 + focus_line_tx_coord(:,3).^2);

        % combine the distances and calculate delay
%         tau(:,y_idx,x_idx) = 1/(c*Ts_A2D)*(d_rx + d_tx2 - 2*abs(sep_focus_z(:, 1, 1)));
        tau(:,y_idx,x_idx) = 1/(c*Ts_A2D)*(d_rx + d_tx2); % absolute round trip time
    end
end

tau_tx1 = d_tx1/(c*Ts_A2D);

save(sprintf('%s/2d_delay_comp_tau_firing_%d.mat', paths.delay, firing_id), 'tau', 'tau_tx1');
