function iter_coef_sep_gen_plane_wave_piecewise(coef_struct_in, firing_id)
% function iter_coef_sep_gen_3var(sub_ap_idx, coef_struct)
% Description:
%     Generate iterative delay coefficient for separable beamforming and
%     save in paths.iter_coef_sep_folder
% Input:
%     sub_ap_idx_x: subaperture x index ranges from 1 to 6 (half of the size due to symmety)
%     sub_ap_idx_y: subaperture y index ranges from 1 to 4 (half of the size due to symmety)
%     coef_struct_in.model_type: approximation model, currently only model_type = 2 (
%         quadratic approximation model is supported
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_a, this input has default value.
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_b, this input has default value.
% Output:
%     none

enable_err_map = true;
enable_coef_gen = true;

paths = set_paths();

if enable_coef_gen
  coef_save_folder = paths.delay;
end

if enable_err_map
  err_map_folder = paths.err_map;
end

load_params = load(sprintf('%s/global_params.mat', paths.params), 'phys_params', 'img_params', 'sep_params', 'sys_params');
phys_params = load_params.phys_params;
img_params = load_params.img_params;
sep_params = load_params.sep_params;
sys_params = load_params.sys_params;

clear load_params;

fc = phys_params.fc;
Tc = phys_params.Tc;
fs = phys_params.fs;
Ts = phys_params.Ts;
Ts_A2D = Ts*phys_params.interp_factor;
lambda = phys_params.lambda;
c = phys_params.c;

model_type = coef_struct_in.model_type;

if ~isfield(coef_struct_in, 'glb_pc_range_tau_a')
  switch model_type
    case 2
      %       coef_struct_in.glb_pc_range_tau_a = [ 0, 290, 950, 2608];
      %       coef_struct_in.glb_pc_range_tau_b = [ 0, 340, 960, 2198];
      coef_struct_in.glb_pc_range_tau_a = [ 0, 180, 520, 1150, 2608];
      coef_struct_in.glb_pc_range_tau_b = [ 0, 230, 540, 1160, 2198];
    otherwise
      error('Unsupported approximation model type!');
  end
end
glb_pc_range_tau_a = coef_struct_in.glb_pc_range_tau_a;
glb_pc_range_tau_b = coef_struct_in.glb_pc_range_tau_b;
num_pc = length(glb_pc_range_tau_a);

sep_focus_z_line = [sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2)]';
stg1_pt = [sep_params.sep_z_pt(1)-sep_params.sep_phase1_beamsum_cusion(1), sep_params.sep_z_pt(2)+sep_params.sep_phase1_beamsum_cusion(2)];
z_grid_stg1 = sep_focus_z_line(stg1_pt(1):stg1_pt(2));
delay_ref_tau_a = abs(z_grid_stg1)*2/(c*Ts_A2D);

delay_ref_tau_b = abs(sep_focus_z_line(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2)))*2/((c*Ts_A2D))...
  - delay_ref_tau_a(1, 1, 1);

depth_tau_a = abs(z_grid_stg1(glb_pc_range_tau_a(2:end)));
depth_tau_b = abs(sep_focus_z_line(glb_pc_range_tau_b(2:end) + sep_params.sep_z_pt(1)));
depth_combined = max(depth_tau_a, depth_tau_b);
sub_size_pc = ceil(depth_combined./(sys_params.fixed_f_num*(sys_params.rx_width+sys_params.rx_kerf)) / 2)*2;
sub_size_pc = min(sub_size_pc, ones(size(sub_size_pc))*min(sys_params.sub_size_x, sys_params.sub_size_y));
acc_depth_idx_a = [0, glb_pc_range_tau_a(2:end)+stg1_pt(1), length(sep_focus_z_line)];
acc_depth_idx_b = [0, glb_pc_range_tau_b(2:end)+sep_params.sep_z_pt(1), length(sep_focus_z_line)];
disp(acc_depth_idx_b - acc_depth_idx_a)
acc_depth_idx = max(acc_depth_idx_a, acc_depth_idx_b);

rx = generate_receive();
Rx_coord = rx.rx_center;
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);
num_scanline_x = size(sep_focus_x, 2);
num_scanline_y = size(sep_focus_y, 3);

alpha = sys_params.firing_angle_alpha(firing_id);
beta = sys_params.firing_angle_beta(firing_id);
norm_vec = [sin(alpha)*cos(beta); sin(alpha)*sin(beta); cos(alpha)];
pin_point_coord = repmat(sys_params.pin_point_coord(firing_id, :), size(sep_focus_z, 1), 1);

tau_a = cell(sys_params.rx_size_x, sys_params.rx_size_y);
tau_b = cell(sys_params.rx_size_y, 1);

for pc_idx = 1:num_pc-1
  rx_size_xy = sub_size_pc(pc_idx);
  z_idx_line = (acc_depth_idx(pc_idx)+1:acc_depth_idx(pc_idx+1)).';
  num_z_idx = acc_depth_idx(pc_idx+1)-acc_depth_idx(pc_idx);

  for y_idx = sys_params.rx_size_y:-1:1
    for scanline_x = num_scanline_x:-1:1
      tranducer_x_range(1) = max(1, round(scanline_x/img_params.lateral_interp_rate) - rx_size_xy/2);
      tranducer_x_range(2) = min(sys_params.rx_size_x, round(scanline_x/img_params.lateral_interp_rate) - 1 + rx_size_xy/2);
      tau_a_sum = zeros(num_z_idx, sys_params.rx_size_x);
      tau_b_sum = zeros(num_z_idx, num_scanline_y);

      scanline_y_range(1) = max(1,  (y_idx - rx_size_xy/2 + 1)*img_params.lateral_interp_rate);
      scanline_y_range(2) = min(num_scanline_y,  (y_idx + rx_size_xy/2)*img_params.lateral_interp_rate);
      for scanline_y = scanline_y_range(2):-1:scanline_y_range(1)

        %       tranducer_y_range(1) = max(1,  round(scanline_y/img_params.lateral_interp_rate) - sys_params.sub_size_y /2);
        %       tranducer_y_range(2) = min(sys_params.rx_size_y,  round(scanline_y/img_params.lateral_interp_rate)-1 + sys_params.sub_size_y /2);
        %       if y_idx < tranducer_y_range(1) || y_idx > tranducer_y_range(2)
        %         continue;
        %       end
        focus_line_tx_coord = [sep_focus_x(z_idx_line, scanline_x, scanline_y), sep_focus_y(z_idx_line, scanline_x, scanline_y), sep_focus_z(z_idx_line, scanline_x, scanline_y)];
        dist_tx = (focus_line_tx_coord - pin_point_coord(z_idx_line, :, :))*norm_vec;

        for x_idx = tranducer_x_range(2):-1:tranducer_x_range(1)
          rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;

          dist_rx = (- Rx_coord(rx_idx,1) + sep_focus_x(z_idx_line, scanline_x, scanline_y)).^2;
          dist_rx = dist_rx + (- Rx_coord(rx_idx,2) + sep_focus_y(z_idx_line, scanline_x, scanline_y)).^2;
          dist_rx = dist_rx + (- Rx_coord(rx_idx,3) + sep_focus_z(z_idx_line, scanline_x, scanline_y)).^2;
          dist_rx = sqrt(dist_rx);

          tau_idx = 1/(c*Ts_A2D)*(dist_rx + dist_tx - 2*abs(sep_focus_z(z_idx_line, 1, 1)));
          tau_a_sum(:, x_idx) = tau_a_sum(:, x_idx) + tau_idx;
          tau_b_sum(:, scanline_y) = tau_b_sum(:, scanline_y) + tau_idx;
        end
      end
      tau_total_sum = sum(tau_a_sum, 2);
      actual_x_size = tranducer_x_range(2)-tranducer_x_range(1)+1;
      num_related_scanline_y = scanline_y_range(2)-scanline_y_range(1)+1;
      tau_total_sum = 0.5 * tau_total_sum / (actual_x_size*num_related_scanline_y);

      for x_idx = tranducer_x_range(2):-1:tranducer_x_range(1)
        tau_a{x_idx, y_idx}(z_idx_line, scanline_x) = tau_a_sum(:, x_idx)/num_related_scanline_y - tau_total_sum;
      end
      for scanline_y = scanline_y_range(2):-1:scanline_y_range(1)
        tau_b{y_idx}(z_idx_line, scanline_x, scanline_y) = tau_b_sum(:, scanline_y)/actual_x_size - tau_total_sum;
      end
    end
  end
end


if enable_coef_gen == true;

  coef_struct = coef_struct_in;
  coef_struct.glb_pc_range = glb_pc_range_tau_a;
  coef_struct = rmfield(coef_struct, {'glb_pc_range_tau_a','glb_pc_range_tau_b'});

  for x_idx = sys_params.rx_size_x:-1:1
    scanline_x_range(1) = max(1,  (x_idx - sys_params.sub_size_x /2 + 1)*img_params.lateral_interp_rate);
    scanline_x_range(2) = min(num_scanline_x,  (x_idx + sys_params.sub_size_x /2)*img_params.lateral_interp_rate);
    num_related_scanline_x = scanline_x_range(2)-scanline_x_range(1)+1;
    for y_idx = sys_params.rx_size_y:-1:1

      tau_a_idx = tau_a{x_idx, y_idx}(stg1_pt(1):stg1_pt(2), scanline_x_range(1):scanline_x_range(2)) +...
        repmat(delay_ref_tau_a, [1,  num_related_scanline_x]);
      % fitting tau_a
      c_tau_a = zeros(length(glb_pc_range_tau_b)-1, num_scanline_x);
      b_tau_a = zeros(length(glb_pc_range_tau_b)-1, num_scanline_x);
      a_tau_a = zeros(length(glb_pc_range_tau_b)-1, num_scanline_x);

      pc_range = glb_pc_range_tau_a;
      pc_range(end) = pc_range(end) -1;
      pt_a = pc_range(1:end-1) + 1;
      pt_b = pc_range(2:end);


      switch model_type
        case 2
          Sn = zeros(5, 1);
          target_mat = diff(tau_a_idx, 1, 1);
          for pec_idx = 1:length(glb_pc_range_tau_b)-1
            y_corr = zeros(3, num_related_scanline_x);
            r_data = target_mat(pt_a(pec_idx):pt_b(pec_idx),:,:);
            n_idx = (pt_a(pec_idx):pt_b(pec_idx)).'- pt_a(pec_idx);
            for l = 1:5
              Sn(l) = sum(n_idx.^(l-1));
            end
            Sn_mat = [Sn(5), Sn(4), Sn(3); Sn(4), Sn(3), Sn(2); Sn(3), Sn(2), Sn(1)];
            y_corr(1, :) = (n_idx.^2)'*r_data;
            y_corr(2, :) = (n_idx.^1)'*r_data;
            y_corr(3, :) = sum(r_data, 1);
            coef_vec = Sn_mat\y_corr;
            c_tau_a(pec_idx, scanline_x_range(1):scanline_x_range(2)) = reshape(coef_vec(3,:), 1, num_related_scanline_x);
            b_tau_a(pec_idx, scanline_x_range(1):scanline_x_range(2)) = reshape(coef_vec(2,:), 1, num_related_scanline_x);
            a_tau_a(pec_idx, scanline_x_range(1):scanline_x_range(2)) = reshape(-coef_vec(1,:), 1, num_related_scanline_x);
          end
      end
      init_address_tau_a(:, scanline_x_range(1):scanline_x_range(2))  = tau_a_idx(glb_pc_range_tau_a(1:end-1)+1, :, :);
      coef_struct.c{x_idx, y_idx} = c_tau_a;
      coef_struct.b{x_idx, y_idx} = b_tau_a;
      coef_struct.a{x_idx, y_idx} = a_tau_a;
      coef_struct.init_address{x_idx, y_idx} = init_address_tau_a;
    end
  end
  save(sprintf('%s/tau_a_coef_firing_%d.mat', coef_save_folder, firing_id), 'coef_struct');

  coef_struct = coef_struct_in;
  coef_struct.glb_pc_range = glb_pc_range_tau_b;
  coef_struct = rmfield(coef_struct, {'glb_pc_range_tau_a','glb_pc_range_tau_b'});

  for y_idx = sys_params.rx_size_y:-1:1
    scanline_y_range(1) = max(1,  (y_idx - sys_params.sub_size_y /2 + 1)*img_params.lateral_interp_rate);
    scanline_y_range(2) = min(num_scanline_y,  (y_idx + sys_params.sub_size_y /2)*img_params.lateral_interp_rate);
    num_related_scanline_y = scanline_y_range(2)-scanline_y_range(1)+1;

    tau_b_idx = tau_b{y_idx}(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2), :, scanline_y_range(1):scanline_y_range(2)) +...
      repmat(delay_ref_tau_b, [1, num_scanline_x, num_related_scanline_y]);

    % fitting tau_b
    c_tau_b = zeros(length(glb_pc_range_tau_b)-1, num_scanline_x, num_scanline_y);
    b_tau_b = zeros(length(glb_pc_range_tau_b)-1, num_scanline_x, num_scanline_y);
    a_tau_b = zeros(length(glb_pc_range_tau_b)-1, num_scanline_x, num_scanline_y);

    pc_range = glb_pc_range_tau_b;
    pc_range(end) = pc_range(end) -1;
    pt_a = pc_range(1:end-1) + 1;
    pt_b = pc_range(2:end);

    switch model_type
      case 2
        Sn = zeros(5, 1);
        target_mat = diff(tau_b_idx, 1, 1);
        for pec_idx = 1:length(glb_pc_range_tau_b)-1
          y_corr = zeros(3, num_scanline_x*num_related_scanline_y);
          r_data = reshape(target_mat(pt_a(pec_idx):pt_b(pec_idx), :, :),...
            [pt_b(pec_idx)-pt_a(pec_idx)+1, num_scanline_x*num_related_scanline_y]);
          n_idx = (pt_a(pec_idx):pt_b(pec_idx)).'- pt_a(pec_idx);
          for l = 1:5
            Sn(l) = sum(n_idx.^(l-1));
          end
          Sn_mat = [Sn(5), Sn(4), Sn(3); Sn(4), Sn(3), Sn(2); Sn(3), Sn(2), Sn(1)];
          y_corr(1, :) = (n_idx.^2)'*r_data;
          y_corr(2, :) = (n_idx.^1)'*r_data;
          y_corr(3, :) = sum(r_data, 1);
          coef_vec = Sn_mat\y_corr;
          c_tau_b(pec_idx, :, scanline_y_range(1):scanline_y_range(2)) = reshape(coef_vec(3,:), 1, num_scanline_x, num_related_scanline_y);
          b_tau_b(pec_idx, :, scanline_y_range(1):scanline_y_range(2)) = reshape(coef_vec(2,:), 1, num_scanline_x, num_related_scanline_y);
          a_tau_b(pec_idx, :, scanline_y_range(1):scanline_y_range(2)) = reshape(-coef_vec(1,:), 1, num_scanline_x, num_related_scanline_y);
        end
    end

    init_address_tau_b(:, :, scanline_y_range(1):scanline_y_range(2)) = tau_b_idx(glb_pc_range_tau_b(1:end-1)+1, :, :);
    coef_struct.c{y_idx} = c_tau_b;
    coef_struct.b{y_idx} = b_tau_b;
    coef_struct.a{y_idx} = a_tau_b;
    coef_struct.init_address{y_idx} = init_address_tau_b;
  end
  save(sprintf('%s/tau_b_coef_firing_%d.mat', coef_save_folder, firing_id), 'coef_struct');
end
clear coef_struct y_corr r_data;

enable_baseline = false;
if enable_err_map == true && enable_baseline == true;
  sub_coord_z = sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2);
  [sub_coord_x, sub_coord_y] = get_sub_recieve(img_params, sys_params);
  [sep_focus_z_grid, sub_coord_x_grid, sub_coord_y_grid] = ndgrid(sub_coord_z, sub_coord_x, sub_coord_y);
  tau_a_baseline = (sqrt(sub_coord_x_grid.^2 + sep_focus_z_grid.^2) - abs(sep_focus_z_grid))/ (c*Ts_A2D);
  tau_b_baseline = (sqrt(sub_coord_y_grid.^2 + sep_focus_z_grid.^2) - abs(sep_focus_z_grid))/ (c*Ts_A2D);
  tau_baseline = (tau_a_baseline + tau_b_baseline);
end

if enable_err_map == true
  for pc_idx = num_pc-1:-1:1
    z_idx_line = (acc_depth_idx(pc_idx)+1:acc_depth_idx(pc_idx+1)).';
    num_z_idx = acc_depth_idx(pc_idx+1)-acc_depth_idx(pc_idx);

    err_acc = zeros(num_z_idx, num_scanline_x, num_scanline_y);
    acc_count = zeros(1, num_scanline_x, num_scanline_y);
    for y_idx = sys_params.rx_size_y:-1:1
      for scanline_x = num_scanline_x:-1:1
        tranducer_x_range(1) = max(1, round(scanline_x/img_params.lateral_interp_rate) - sys_params.sub_size_x /2);
        tranducer_x_range(2) = min(sys_params.rx_size_x, round(scanline_x/img_params.lateral_interp_rate) - 1 + sys_params.sub_size_x /2);

        scanline_y_range(1) = max(1,  (y_idx - rx_size_xy/2 + 1)*img_params.lateral_interp_rate);
        scanline_y_range(2) = min(num_scanline_y,  (y_idx + rx_size_xy/2)*img_params.lateral_interp_rate);
        for scanline_y = scanline_y_range(2):-1:scanline_y_range(1)

          focus_line_tx_coord = [sep_focus_x(z_idx_line, scanline_x, scanline_y), sep_focus_y(z_idx_line, scanline_x, scanline_y), sep_focus_z(z_idx_line, scanline_x, scanline_y)];
          dist_tx = (focus_line_tx_coord - pin_point_coord(z_idx_line, :, :))*norm_vec;

          for x_idx = tranducer_x_range(2):-1:tranducer_x_range(1)
            rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;

            dist_rx = (- Rx_coord(rx_idx,1) + sep_focus_x(z_idx_line, scanline_x, scanline_y)).^2;
            dist_rx = dist_rx + (- Rx_coord(rx_idx,2) + sep_focus_y(z_idx_line, scanline_x, scanline_y)).^2;
            dist_rx = dist_rx + (- Rx_coord(rx_idx,3) + sep_focus_z(z_idx_line, scanline_x, scanline_y)).^2;
            dist_rx = sqrt(dist_rx);

            tau_idx = 1/(c*Ts_A2D)*(dist_rx + dist_tx - 2*abs(sep_focus_z(z_idx_line, 1, 1)));
            tau_recover =  tau_a{x_idx, y_idx}(z_idx_line, scanline_x) + tau_b{y_idx}(z_idx_line, scanline_x, scanline_y);

            err_acc(:, scanline_x, scanline_y) = err_acc(:, scanline_x, scanline_y) + (tau_recover - tau_idx).^2;
            acc_count(1, scanline_x, scanline_y) =  acc_count(1, scanline_x, scanline_y) + 1;
          end
        end
      end
    end
    err_scanline(z_idx_line, :, :) = err_acc ./ repmat(acc_count, [size(err_acc, 1), 1, 1]);

    if  enable_baseline == true;
      save(sprintf('%s/err_diff_subsize_%dx%d_firing_%d.mat', err_map_folder, sys_params.sub_size_x, sys_params.sub_size_y, firing_id),...
        'err_scanline', 'err_scanline_baseline');
    else
      save(sprintf('%s/err_diff_subsize_%dx%d_firing_%d.mat', err_map_folder, sys_params.sub_size_x, sys_params.sub_size_y, firing_id),...
        'err_scanline');
    end
  end
end
% tau_a_baseline = (sqrt(sub_coord_x_grid.^2 + sep_focus_z_grid.^2) - abs(sep_focus_z_grid))/ (c*Ts_A2D);
% tau_b_baseline = (sqrt(sub_coord_y_grid.^2 + sep_focus_z_grid.^2) - abs(sep_focus_z_grid))/ (c*Ts_A2D);
%
% if enable_err_map
%   tau_recover = repmat(tau_a, [1, 1, sys_params.sub_size_y]) + ...
%     repmat(tau_b, [1, sys_params.sub_size_x, 1]);
%   err_diff_our = tau_idx - tau_recover;
%
%   tau_baseline = (tau_a_baseline + tau_b_baseline);
%   err_diff_baseline = tau_idx - tau_baseline;
%
%   save(sprintf('%s/err_diff_subsize_%dx%d.mat', err_map_folder, sys_params.sub_size_x, sys_params.sub_size_y),...
%     'err_diff_our', 'err_diff_baseline');
%
%   rms_line_our = sqrt(sum(sum(err_diff_our.^2,3),2)/(sys_params.sub_size_x*sys_params.sub_size_y));
%   rms_line_baseline = sqrt(sum(sum(err_diff_baseline.^2,3),2)/(sys_params.sub_size_x*sys_params.sub_size_y));
%
%   plot(rms_line_baseline*360/10, 'b');
%   hold on;
%   plot(rms_line_our*360/10, 'r');
%   hold off;
% end

% if enable_coef_gen
%   save(sprintf('%s/sep_delay_tau_firing_%d.mat', coef_save_folder, firing_id), 'tau_a', 'tau_b');
% end
