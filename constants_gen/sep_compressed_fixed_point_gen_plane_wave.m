function sep_compressed_fixed_point_gen_plane_wave(firing_id)
% function iter_coef_sep_gen_3var(sub_ap_idx, coef_struct)
% Description:
%     Generate iterative delay coefficient for separable beamforming and
%     save in paths.iter_coef_sep_folder
% Input:
%     sub_ap_idx_x: subaperture x index ranges from 1 to 6 (half of the size due to symmety)
%     sub_ap_idx_y: subaperture y index ranges from 1 to 4 (half of the size due to symmety)
%     coef_struct_in.model_type: approximation model, currently only model_type = 2 (
%         quadratic approximation model is supported
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_a, this input has default value.
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_b, this input has default value.
% Output:
%     none

paths = set_paths();

load_params = load(sprintf('%s/global_params.mat', paths.params), 'phys_params', 'img_params', 'sep_params', 'sys_params');
phys_params = load_params.phys_params;
img_params = load_params.img_params;
sep_params = load_params.sep_params;
sys_params = load_params.sys_params;

clear load_params;

Ts = phys_params.Ts;
Ts_A2D = Ts*phys_params.interp_factor;
c = phys_params.c;

rx = generate_receive();
Rx_coord = rx.rx_center;
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);

alpha = sys_params.firing_angle_alpha(firing_id);
beta = sys_params.firing_angle_beta(firing_id);
pin_point_coord = repmat(sys_params.pin_point_coord(firing_id, :), size(sep_focus_z, 1), 1);

d_tx1 = zeros(sys_params.rx_size_x,sys_params.rx_size_x);

tau_1_2 = zeros(size(sep_focus_x,1), sys_params.sub_size_x, sys_params.sub_size_y);

% calculate the distance from the plane to the first focal point in each
% scanline
for scanline_x = 1:sys_params.rx_size_x
    for scanline_y = 1:sys_params.rx_size_y
        % select the scanline
        focus_line_tx_coord = [sep_focus_x(:, scanline_x, scanline_y), sep_focus_y(:, scanline_x, scanline_y), sep_focus_z(:, scanline_x, scanline_y)];

        % calculate the transmit distance to the first focal point
        d_tx1(scanline_x, scanline_y) = (focus_line_tx_coord(1,1)-pin_point_coord(1,1))*sin(alpha)*cos(beta)+(focus_line_tx_coord(1,2)-pin_point_coord(1,2))*sin(alpha)*sin(beta)-pin_point_coord(1,3)*cos(alpha);
    end
end

tau_tx1 = d_tx1/(c*Ts_A2D);
tau_a_tx1_offsets = zeros(sys_params.rx_size_x, sys_params.rx_size_y, sys_params.rx_size_x); % rx_size_x offsets per scanline due to sharing - ***THIS IS SO THE HARDWARE WILL WORK THE SAME WAY IN BOTH STAGES***
tau_b_tx1_offsets = zeros(sys_params.rx_size_x, sys_params.rx_size_y, sys_params.rx_size_x); % rx_size_x offsets per scanline due to sharing

% calculate the averaged d_tx1 delays for the first stage
for scanline_x = 1:sys_params.rx_size_x
    for scanline_y = 1:sys_params.rx_size_y
        % set which transducers affect this scanline in x-dim
        tranducer_x_range(1) = max(1, round(scanline_x/img_params.lateral_interp_rate) - floor(sys_params.sub_size_x /2));
        tranducer_x_range(2) = min(sys_params.rx_size_x, round(scanline_x/img_params.lateral_interp_rate) - 1 + floor(sys_params.sub_size_x /2));

        scanline_y_range(1) = max(1, (scanline_y - floor(sys_params.sub_size_y /2) + 1)*img_params.lateral_interp_rate);
        scanline_y_range(2) = min(sys_params.rx_size_y, (scanline_y + floor(sys_params.sub_size_y /2))*img_params.lateral_interp_rate);

        % iterate through each transducer that affects this scanline
        for x_idx = tranducer_x_range(1):tranducer_x_range(2)
            % average the delays to match the scanlines
            % TODO: IMPROVE THIS (remove edge cases and see if it helps..)
            actual_x_size = tranducer_x_range(2)-tranducer_x_range(1)+1;
            num_related_scanline_y = scanline_y_range(2)-scanline_y_range(1)+1;
            test0 = tau_tx1(scanline_x, scanline_y_range(1):scanline_y_range(2));
            testX = sum(test0);
            test1 = testX * actual_x_size; % multiplied because this is the sum of one dim
            test2 = 0.5 * test1 / (actual_x_size*num_related_scanline_y);

            % set the x-dim index of the beamforming aperture
            tau_a_tx1_offsets(scanline_x, scanline_y, :) = test2;
        end
    end
end

% calculate the averaged d_tx1 delays for the second stage
for scanline_x = 1:sys_params.rx_size_x
    for scanline_y = 1:sys_params.rx_size_y
        % set which transducers affect this scanline in y-dim
        tranducer_y_range(1) = max(1, round(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y /2));
        tranducer_y_range(2) = min(sys_params.rx_size_y, round(scanline_y/img_params.lateral_interp_rate)-1 + floor(sys_params.sub_size_y /2));

        % iterate through each transducer that affects this scanline
        for y_idx = tranducer_y_range(1):tranducer_y_range(2)
            % average the delays to match the scanlines; stage 2 delays are
            % offset by all stage 1 delays (from contributing channels)
%             tau_b_tx1_offsets(scanline_x, scanline_y, y_idx) = tau_tx1(scanline_x, scanline_y)-tau_a_tx1_offsets(scanline_x, y_idx);

            % set the y-dim index of the beamforming aperture
            elem_y_idx = y_idx - (round(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y /2)) + 1;

            tau_b_tx1_offsets(scanline_x, scanline_y, elem_y_idx) = tau_tx1(scanline_x, scanline_y)-tau_a_tx1_offsets(scanline_x, y_idx, elem_y_idx);
        end
    end
end
% average the second stage delays so there is only one per
% scanline...downside is that this causes somewhat significant clarity loss
% tau_b_tx1_offsets2 = zeros(sys_params.rx_size_x, sys_params.rx_size_y);
% for scanline_x = 1:sys_params.rx_size_x
%     for scanline_y = 1:sys_params.rx_size_y
%         % set which transducers affect this scanline in y-dim
%         tranducer_y_range(1) = max(1, round(scanline_y/img_params.lateral_interp_rate) - floor(sys_params.sub_size_y /2));
%         tranducer_y_range(2) = min(sys_params.rx_size_y, round(scanline_y/img_params.lateral_interp_rate)-1 + floor(sys_params.sub_size_y /2));
%
%         num_transducers = tranducer_y_range(2) - tranducer_y_range(1) + 1;
%
%         tau_b_tx1_offsets2(scanline_x, scanline_y) = sum(tau_b_tx1_offsets(scanline_x, scanline_y, :)) / num_transducers;
%     end
% end
% tau_b_tx1_offsets = tau_b_tx1_offsets2;


% select the scanline located at the middle of the beamforming aperture
focus_line_tx_coord = [sep_focus_x(:, (floor(sys_params.sub_size_x/2)+1), (floor(sys_params.sub_size_y/2)+1)), sep_focus_y(:, (floor(sys_params.sub_size_x/2)+1), (floor(sys_params.sub_size_y/2)+1)), sep_focus_z(:, (floor(sys_params.sub_size_x/2)+1), (floor(sys_params.sub_size_y/2)+1))];


for x_idx = 1:sys_params.sub_size_x
    for y_idx = 1:sys_params.sub_size_y
        % set index into Rx_coord to retreive transducer indices
        rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;

        % calculate the transmit distance from the first focal point to all
        % others
        d_tx2 = focus_line_tx_coord(:,3)*cos(alpha);

        % calculate the reflected distance from the first focal point to
        % the current transducer
        d_rx = sqrt((Rx_coord(rx_idx,1)-focus_line_tx_coord(:,1)).^2 + (Rx_coord(rx_idx,2)-focus_line_tx_coord(:,2)).^2 + focus_line_tx_coord(:,3).^2);

        % combine the distances and calculate delay
        tau_1_2(:, x_idx, y_idx) = (d_rx + d_tx2 - 2*abs(sep_focus_z(:, 1, 1)))/(c*Ts_A2D);
    end
end

% collapse the array along x and y; should result in Mz values
p_m_z = sum(sum(tau_1_2,2),3)/(2*sys_params.sub_size_x*sys_params.sub_size_y);

% tau_1 - collapse along y; should result in Mz*Nx delays
tau_1 = (squeeze(sum(tau_1_2,3))/sys_params.sub_size_y) - p_m_z(:);

% tau_2 - collapse along x; should result in Mz*Ny delays
tau_2 = (squeeze(sum(tau_1_2,2))/sys_params.sub_size_x) - p_m_z(:);

% calculate delay offsets - Ming did this, don't know why
sep_focus_z_line = [sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2)]';
stg1_pt = [sep_params.sep_z_pt(1)-sep_params.sep_phase1_beamsum_cusion(1), sep_params.sep_z_pt(2)+sep_params.sep_phase1_beamsum_cusion(2)];
z_grid_stg1 = sep_focus_z_line(stg1_pt(1):stg1_pt(2));
delay_ref_tau_a = abs(z_grid_stg1)*2/(c*Ts_A2D);
delay_ref_tau_b = abs(sep_focus_z_line(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2)))*2/((c*Ts_A2D)) - delay_ref_tau_a(1, 1, 1);

% add the delay offsets for focusing and remove unnecessary delays
tau_1 = tau_1(stg1_pt(1):stg1_pt(2), :)+delay_ref_tau_a;
tau_2 = tau_2(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2), :)+delay_ref_tau_b;

save(sprintf('%s/sep_delay_compressed_fixed_point_tau_firing_%d.mat', paths.delay, firing_id), 'tau_1', 'tau_2', 'tau_a_tx1_offsets', 'tau_b_tx1_offsets');
