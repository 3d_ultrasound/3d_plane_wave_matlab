function sep_gen_plane_wave(firing_id)
% function iter_coef_sep_gen_3var(sub_ap_idx, coef_struct)
% Description:
%     Generate iterative delay coefficient for separable beamforming and
%     save in paths.iter_coef_sep_folder
% Input:
%     sub_ap_idx_x: subaperture x index ranges from 1 to 6 (half of the size due to symmety)
%     sub_ap_idx_y: subaperture y index ranges from 1 to 4 (half of the size due to symmety)
%     coef_struct_in.model_type: approximation model, currently only model_type = 2 (
%         quadratic approximation model is supported
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_a, this input has default value.
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_b, this input has default value.
% Output:
%     none

enable_err_map = false;
enable_coef_gen = true;

paths = set_paths();

if enable_coef_gen
  coef_save_folder = paths.delay;
end

if enable_err_map
  err_map_folder = paths.err_map;
end

load_params = load(sprintf('%s/global_params.mat', paths.params), 'phys_params', 'img_params', 'sep_params', 'sys_params');
phys_params = load_params.phys_params;
img_params = load_params.img_params;
sep_params = load_params.sep_params;
sys_params = load_params.sys_params;

clear load_params;

fc = phys_params.fc;
Tc = phys_params.Tc;
fs = phys_params.fs;
Ts = phys_params.Ts;
Ts_A2D = Ts*phys_params.interp_factor;
lambda = phys_params.lambda;
c = phys_params.c;

rx = generate_receive();
Rx_coord = rx.rx_center;
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);
num_scanline_x = size(sep_focus_x, 2);
num_scanline_y = size(sep_focus_y, 3);

alpha = sys_params.firing_angle_alpha(firing_id);
beta = sys_params.firing_angle_beta(firing_id);
norm_vec = [sin(alpha)*cos(beta); sin(alpha)*sin(beta); cos(alpha)];
pin_point_coord = repmat(sys_params.pin_point_coord(firing_id, :), size(sep_focus_z, 1), 1);

tau_a = cell(sys_params.rx_size_x, sys_params.rx_size_y);
tau_b = cell(sys_params.rx_size_y, 1);

for y_idx = sys_params.rx_size_y:-1:1
  for scanline_x = num_scanline_x:-1:1
    tranducer_x_range(1) = max(1, round(scanline_x/img_params.lateral_interp_rate) - floor(sys_params.sub_size_x /2));
    tranducer_x_range(2) = min(sys_params.rx_size_x, round(scanline_x/img_params.lateral_interp_rate) - 1 + floor(sys_params.sub_size_x /2));
    tau_a_sum = zeros(size(sep_focus_x, 1), sys_params.rx_size_x);
    tau_b_sum = zeros(size(sep_focus_x, 1), num_scanline_y);

    scanline_y_range(1) = max(1,  (y_idx - floor(sys_params.sub_size_y /2) + 1)*img_params.lateral_interp_rate);
    scanline_y_range(2) = min(num_scanline_y,  (y_idx + floor(sys_params.sub_size_y /2))*img_params.lateral_interp_rate);
    for scanline_y = scanline_y_range(2):-1:scanline_y_range(1)
      focus_line_tx_coord = [sep_focus_x(:, scanline_x, scanline_y), sep_focus_y(:, scanline_x, scanline_y), sep_focus_z(:, scanline_x, scanline_y)];
      dist_tx = (focus_line_tx_coord - pin_point_coord)*norm_vec;

      for x_idx = tranducer_x_range(2):-1:tranducer_x_range(1)
        rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;

        dist_rx = sqrt((focus_line_tx_coord(:,1)-Rx_coord(rx_idx,1)).^2 + (focus_line_tx_coord(:,2)-Rx_coord(rx_idx,2)).^2 + (focus_line_tx_coord(:,3)-Rx_coord(rx_idx,3)).^2);

        tau_idx = 1/(c*Ts_A2D)*(dist_rx + dist_tx - 2*abs(sep_focus_z(:, 1, 1)));
        tau_a_sum(:, x_idx) = tau_a_sum(:, x_idx) + tau_idx;
        tau_b_sum(:, scanline_y) = tau_b_sum(:, scanline_y) + tau_idx;
      end
    end
    tau_total_sum = sum(tau_a_sum, 2);
    actual_x_size = tranducer_x_range(2)-tranducer_x_range(1)+1;
    num_related_scanline_y = scanline_y_range(2)-scanline_y_range(1)+1;
    tau_total_sum = 0.5 * tau_total_sum / (actual_x_size*num_related_scanline_y);

    for x_idx = tranducer_x_range(2):-1:tranducer_x_range(1)
      tau_a{x_idx, y_idx}(:, scanline_x) = tau_a_sum(:, x_idx)/num_related_scanline_y - tau_total_sum;
    end
    for scanline_y = scanline_y_range(2):-1:scanline_y_range(1)
      tau_b{y_idx}(:, scanline_x, scanline_y) = tau_b_sum(:, scanline_y)/actual_x_size - tau_total_sum;
    end
  end
end


sep_focus_z_line = [sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2)]';
stg1_pt = [sep_params.sep_z_pt(1)-sep_params.sep_phase1_beamsum_cusion(1), sep_params.sep_z_pt(2)+sep_params.sep_phase1_beamsum_cusion(2)];
z_grid_stg1 = sep_focus_z_line(stg1_pt(1):stg1_pt(2));
delay_ref_tau_a = abs(z_grid_stg1)*2/(c*Ts_A2D);

delay_ref_tau_b = abs(sep_focus_z_line(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2)))*2/((c*Ts_A2D))...
- delay_ref_tau_a(1, 1, 1);

for y_idx = 1:num_scanline_y
  for x_idx = 1:num_scanline_x
      tau_a{x_idx,y_idx} = tau_a{x_idx,y_idx}(stg1_pt(1):stg1_pt(2), :, :)+delay_ref_tau_a;
  end
end
for y_idx = 1:num_scanline_y
  tau_b{y_idx} = tau_b{y_idx}(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2), :, :)+delay_ref_tau_b;
end

% tau_a_baseline = (sqrt(sub_coord_x_grid.^2 + sep_focus_z_grid.^2) - abs(sep_focus_z_grid))/ (c*Ts_A2D);
% tau_b_baseline = (sqrt(sub_coord_y_grid.^2 + sep_focus_z_grid.^2) - abs(sep_focus_z_grid))/ (c*Ts_A2D);
%
% if enable_err_map
%   tau_recover = repmat(tau_a, [1, 1, sys_params.sub_size_y]) + ...
%     repmat(tau_b, [1, sys_params.sub_size_x, 1]);
%   err_diff_our = tau_idx - tau_recover;
%
%   tau_baseline = (tau_a_baseline + tau_b_baseline);
%   err_diff_baseline = tau_idx - tau_baseline;
%
%   save(sprintf('%s/err_diff_subsize_%dx%d.mat', err_map_folder, sys_params.sub_size_x, sys_params.sub_size_y),...
%     'err_diff_our', 'err_diff_baseline');
%
%   rms_line_our = sqrt(sum(sum(err_diff_our.^2,3),2)/(sys_params.sub_size_x*sys_params.sub_size_y));
%   rms_line_baseline = sqrt(sum(sum(err_diff_baseline.^2,3),2)/(sys_params.sub_size_x*sys_params.sub_size_y));
%
%   plot(rms_line_baseline*360/10, 'b');
%   hold on;
%   plot(rms_line_our*360/10, 'r');
%   hold off;
% end

% focus_z = focus_line_tx_coord(:,3)*cos(alpha);

% save(sprintf('%s/sep_delay_tau_firing_%d.mat', coef_save_folder, firing_id), 'tau_a', 'tau_b', 'focus_z');
save(sprintf('%s/sep_delay_tau_firing_%d.mat', coef_save_folder, firing_id), 'tau_a', 'tau_b');
