function sep_gen_plane_wave_TEST(firing_id)
% function iter_coef_sep_gen_3var(sub_ap_idx, coef_struct)
% Description:
%     Generate iterative delay coefficient for separable beamforming and
%     save in paths.iter_coef_sep_folder
% Input:
%     sub_ap_idx_x: subaperture x index ranges from 1 to 6 (half of the size due to symmety)
%     sub_ap_idx_y: subaperture y index ranges from 1 to 4 (half of the size due to symmety)
%     coef_struct_in.model_type: approximation model, currently only model_type = 2 (
%         quadratic approximation model is supported
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_a, this input has default value.
%     coef_struct_in.glb_pc_range_tau_a: the cut-off point of delay section
%         for tau_b, this input has default value.
% Output:
%     none

enable_err_map = false;
enable_coef_gen = true;

paths = set_paths();

if enable_coef_gen
    coef_save_folder = paths.delay;
end

if enable_err_map
    err_map_folder = paths.err_map;
end

load_params = load(sprintf('%s/global_params.mat', paths.params), 'phys_params', 'img_params', 'sep_params', 'sys_params');
phys_params = load_params.phys_params;
img_params = load_params.img_params;
sep_params = load_params.sep_params;
sys_params = load_params.sys_params;

clear load_params;

fc = phys_params.fc;
Tc = phys_params.Tc;
fs = phys_params.fs;
Ts = phys_params.Ts;
Ts_A2D = Ts*phys_params.interp_factor;
lambda = phys_params.lambda;
c = phys_params.c;

rx = generate_receive();
Rx_coord = rx.rx_center;
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);
num_scanline_x = size(sep_focus_x, 2);
num_scanline_y = size(sep_focus_y, 3);

alpha = sys_params.firing_angle_alpha(firing_id);
beta = sys_params.firing_angle_beta(firing_id);
norm_vec = [sin(alpha)*cos(beta); sin(alpha)*sin(beta); cos(alpha)];
pin_point_coord = repmat(sys_params.pin_point_coord(firing_id, :), size(sep_focus_z, 1), 1);

tau_a2 = cell(sys_params.rx_size_x, sys_params.rx_size_y);
tau_b2 = cell(sys_params.rx_size_y, 1);


d_tx1 = zeros(sys_params.rx_size_y,sys_params.rx_size_x);

for y_idx = sys_params.rx_size_y:-1:1
    for scanline_x = num_scanline_x:-1:1
        tranducer_x_range(1) = max(1, round(scanline_x/img_params.lateral_interp_rate) - sys_params.sub_size_x /2);
        tranducer_x_range(2) = min(sys_params.rx_size_x, round(scanline_x/img_params.lateral_interp_rate) - 1 + sys_params.sub_size_x /2);
        tau_a_sum = zeros(size(sep_focus_x, 1), sys_params.rx_size_x);
        tau_b_sum = zeros(size(sep_focus_x, 1), num_scanline_y);
        tau_a_sum2 = zeros(size(sep_focus_x, 1), sys_params.rx_size_x);
        tau_b_sum2 = zeros(size(sep_focus_x, 1), num_scanline_y);

        scanline_y_range(1) = max(1,  (y_idx - sys_params.sub_size_y /2 + 1)*img_params.lateral_interp_rate);
        scanline_y_range(2) = min(num_scanline_y,  (y_idx + sys_params.sub_size_y /2)*img_params.lateral_interp_rate);

        for scanline_y = scanline_y_range(2):-1:scanline_y_range(1)
            focus_line_tx_coord = [sep_focus_x(:, scanline_x, scanline_y), sep_focus_y(:, scanline_x, scanline_y), sep_focus_z(:, scanline_x, scanline_y)];

            d_tx1(scanline_x, scanline_y) = (focus_line_tx_coord(1,1)-pin_point_coord(1,1))*sin(alpha)*cos(beta)+(focus_line_tx_coord(1,2)-pin_point_coord(1,2))*sin(alpha)*sin(beta)-pin_point_coord(1,3)*cos(alpha);
            d_tx2 = focus_line_tx_coord(:,3)*cos(alpha);

            dist_tx = d_tx1(scanline_x, scanline_y) + d_tx2;
            dist_tx2 = d_tx2;

            for x_idx = tranducer_x_range(2):-1:tranducer_x_range(1)
                rx_idx = x_idx + (y_idx-1)*sys_params.rx_size_x;

                dist_rx = sqrt((focus_line_tx_coord(:,1)-Rx_coord(rx_idx,1)).^2 + (focus_line_tx_coord(:,2)-Rx_coord(rx_idx,2)).^2 + (focus_line_tx_coord(:,3)-Rx_coord(rx_idx,3)).^2);

                tau_idx = 1/(c*Ts_A2D)*(dist_rx + dist_tx - 2*abs(sep_focus_z(:, 1, 1)));
                tau_a_sum(:, x_idx) = tau_a_sum(:, x_idx) + tau_idx;
                tau_b_sum(:, scanline_y) = tau_b_sum(:, scanline_y) + tau_idx;

                tau_idx2 = 1/(c*Ts_A2D)*(dist_rx + dist_tx2 - 2*abs(sep_focus_z(:, 1, 1)));
                tau_a_sum2(:, x_idx) = tau_a_sum2(:, x_idx) + tau_idx2;
                tau_b_sum2(:, scanline_y) = tau_b_sum2(:, scanline_y) + tau_idx2;
            end
        end

        tau_total_sum = sum(tau_a_sum, 2);
        actual_x_size = tranducer_x_range(2)-tranducer_x_range(1)+1;
        num_related_scanline_y = scanline_y_range(2)-scanline_y_range(1)+1;
        tau_total_sum = 0.5 * tau_total_sum / (actual_x_size*num_related_scanline_y);

        for x_idx = tranducer_x_range(2):-1:tranducer_x_range(1)
            tau_a2{x_idx, y_idx}(:, scanline_x) = tau_a_sum2(:, x_idx)/num_related_scanline_y - tau_total_sum;
        end
        for scanline_y = scanline_y_range(2):-1:scanline_y_range(1)
            tau_b2{y_idx}(:, scanline_x, scanline_y) = tau_b_sum2(:, scanline_y)/actual_x_size - tau_total_sum;

            if(scanline_x == 11 && scanline_y == 11)
            end
        end
    end
end

sep_focus_z_line = [sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2)]';
stg1_pt = [sep_params.sep_z_pt(1)-sep_params.sep_phase1_beamsum_cusion(1), sep_params.sep_z_pt(2)+sep_params.sep_phase1_beamsum_cusion(2)];
z_grid_stg1 = sep_focus_z_line(stg1_pt(1):stg1_pt(2));
delay_ref_tau_a = abs(z_grid_stg1)*2/(c*Ts_A2D);

delay_ref_tau_b = abs(sep_focus_z_line(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2)))*2/((c*Ts_A2D))...
- delay_ref_tau_a(1, 1, 1);

for y_idx = 1:num_scanline_y
    for x_idx = 1:num_scanline_x
        tau_a2{x_idx,y_idx} = tau_a2{x_idx,y_idx}(stg1_pt(1):stg1_pt(2), :, :)+delay_ref_tau_a;
    end
end
for y_idx = 1:num_scanline_y
    tau_b2{y_idx} = tau_b2{y_idx}(sep_params.sep_z_pt(1):sep_params.sep_z_pt(2), :, :)+delay_ref_tau_b;
end

tau_tx1 = d_tx1/(c*Ts_A2D);

save(sprintf('%s/sep_TEST_delay_tau_firing_%d.mat', coef_save_folder, firing_id), 'tau_a2', 'tau_b2', 'tau_tx1');
