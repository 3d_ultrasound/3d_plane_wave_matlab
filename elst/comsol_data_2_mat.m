function comsol_data_2_mat(core, num_firings)
% Description:
%   Read COMSOL data in .csv format and restore in Matlab data file (.mat
%   format)
% Input:
%   core:           process id
%   num_firings:    the number of firings, if left as empty matrix the
%                   function will figure it out by itself by stored parameters 

paths = set_paths();
addpath(genpath(paths.elst));
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'elst_params', 'img_params', 'elst_params');

if isempty(num_firings)
  num_firings = floor(elst_params.sim_duration/elst_params.firing_time_interval);
end
total_cores = 2;

jobs = mod(0:num_firings-1, total_cores)+1;
firings = 1:num_firings;
my_jobs = firings(jobs == core);

load_cfg.file_name = sprintf('%s/3d_shear_wave_v3.csv', paths.comsol_data);
load_cfg.grid_size = elst_params.comsol_grid_size;
load_cfg.num_dim = length(load_cfg.grid_size);

for firing_id = my_jobs
  fprintf('Working on firing # %d\n', firing_id);
  disp = comsol_shear_wave_load(load_cfg, 'data', firing_id);
  save(sprintf('%s/shear_wave_disp_fire_%d.mat', paths.comsol_data, firing_id), 'disp');
end

if core == 1
  load_cfg.file_name = sprintf('%s/3d_shear_wave_v3.csv', paths.comsol_data);
  load_cfg.grid_size = elst_params.comsol_grid_size;
  load_cfg.num_dim = length(load_cfg.grid_size);
  
  grid = comsol_shear_wave_load(load_cfg, 'grid_coord', []);
  save(sprintf('%s/coord_grid.mat', paths.comsol_data), 'grid');
end