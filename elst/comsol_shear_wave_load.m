function output_struct = comsol_shear_wave_load(cfg_struct, read_type, frame_idx)
% Description:
%    read comsol data in .csv file into Matlab
% Input:
%   cfg_struct.grid_size  3-by-1 vector, size of grid in x, y, z
%   cfg_struct.num_dim    scalar, number of dimensions
%   cfg_struct.file_name  string, file name with complete directory
%   read_type             string, 'grid_coord' for grid coordinates
%                         'data' for data trunck
%   frame_idx             Indicates the frame read in this function call
% Output when reading coordinates:
%   output_struct.grid_x  x coordinate 
%   output_struct.grid_y  y coordinate 
%   output_struct.grid_z  z coordinate 
% Output when reading data:
%   output_struct.disp_x  displacement in x direction 
%   output_struct.disp_y  displacement in y direction 
%   output_struct.disp_z  displacement in z direction 

file_name = cfg_struct.file_name;
cfg_section = dlmread(file_name, ',', [3 1 5 1]);
grid_starting_row = 9;
data_starting_row = 14;

num_dim = cfg_section(1);
num_node = cfg_section(2);
num_expr = cfg_section(3);
if cfg_struct.num_dim ~= num_dim
  error('Dimensions mismatch.');
end

if prod(cfg_struct.grid_size) ~= num_node
  error('Grid size mismathes.');
else
  grid_size = cfg_struct.grid_size;
end

switch lower(read_type)
  case 'grid_coord'
    x_coord = dlmread(file_name, ',', [grid_starting_row, 0,... 
      grid_starting_row, grid_size(1)-1]);
    y_coord = dlmread(file_name, ',', [grid_starting_row+1, 0,... 
      grid_starting_row+1, grid_size(2)-1]);
    z_coord = dlmread(file_name, ',', [grid_starting_row+2, 0,... 
      grid_starting_row+2, grid_size(3)-1]);
    [grid_x, grid_y, grid_z] = ndgrid(x_coord, y_coord, z_coord);
    output_struct.grid_x = grid_x;
    output_struct.grid_y = grid_y;
    output_struct.grid_z = grid_z;
  case 'data'
    data_width = grid_size(2);
    data_length = grid_size(1)*grid_size(3);
    dim_idx = 1;
    data_block = dlmread(file_name, ',', ...
      [data_starting_row+(frame_idx-1)*(data_length+2)*num_dim+(dim_idx-1)*(data_length+2), 0, ...
       data_starting_row+(frame_idx-1)*(data_length+2)*num_dim + dim_idx*(data_length+2)-3, data_width-1]);
    output_struct.disp_x = reshape(data_block.', grid_size);
    
    dim_idx = 2;
    data_block = dlmread(file_name, ',', ...
      [data_starting_row+(frame_idx-1)*(data_length+2)*num_dim+(dim_idx-1)*(data_length+2), 0, ...
       data_starting_row+(frame_idx-1)*(data_length+2)*num_dim + dim_idx*(data_length+2)-3, data_width-1]);   
    output_struct.disp_y = reshape(data_block.', grid_size);
    
    dim_idx = 3;
    data_block = dlmread(file_name, ',', ...
      [data_starting_row+(frame_idx-1)*(data_length+2)*num_dim+(dim_idx-1)*(data_length+2), 0, ...
       data_starting_row+(frame_idx-1)*(data_length+2)*num_dim + dim_idx*(data_length+2)-3, data_width-1]);   
    output_struct.disp_z = reshape(data_block.', grid_size);

    fid = fopen(file_name);
    try
      time_str = textscan(fid,'%s', 1, 'delimiter', ',',...
        'headerlines', data_starting_row + (frame_idx-1)*(data_length+2)*num_dim -1,...
        'returnonerror',0,'emptyvalue',0, 'CollectOutput', true);
    catch exception
      fclose(fid);
      throw(exception);
    end
    fclose(fid);
    t_loc = strfind(time_str{1}, '@');
    output_struct.t = str2double(time_str{1}{1}(t_loc{1}+4:end));
  otherwise
    error('Unknown read_type.');
end
