function est_out = elst_estimator_baseline(cfg, uz_mat)
% Description:
%   Elsasticity estimation based on displacement in z dimension (uz_mat)
% Input:
%   uz_mat    uz_mat(z,x,y,f) is a four-dimensional array that represents
%             the desplacement in z, f means the fourth dimenstion is in
%             temperal frequency domain
%   cfg.time_interval     The time interval in two consecutive frames
%   cfg.num_dim           The number of spatial dimension
%   cfg.spatial_interval  The spatial resolution in each spatial dimension
% Output:
%   est_out   est_out(z,x,y,f) is the estimation output, f is the frequency index 

origin_size = size(uz_mat);
fft_size = 32;
if fft_size < origin_size(4)
  error('Larger FFT size required.');
end

Fu_mat_full = fft(uz_mat, fft_size, 4);
clear uz_mat;
Fu_mat = Fu_mat_full(:, :, :, 2:fft_size/2); % DC is removed in this new version
clear Fu_mat_full;

freq = zeros([ones(1, length(origin_size)-1), fft_size/2-1]);

% % (j*omega)^2 for partial derivative with respect of t
% for k = 1:fft_size/2-1
%     freq(1, 1, 1, k) = 2*pi*k/(cfg.time_interval*fft_size);
% end
% Fu_par_t = Fu_mat.*repmat(-freq.^2, [origin_size(1:3), 1]);

% 2*(cos(omega)-1) for discretized partial derivative (difference) with respect of t
for k = 1:fft_size/2-1
    freq(1, 1, 1, k) = 2*pi*k/fft_size;
end
Fu_par_t = Fu_mat.*repmat(2*(cos(freq)-1)*(1/cfg.time_interval^2), [origin_size(1:3), 1]);
Fu_par_x = diff(Fu_mat, 2, 2)*(1/cfg.spatial_interval(2)^2);
curl_Uz = Fu_par_x(2:end-1, :, 2:end-1, :);
clear Fu_par_x;
Fu_par_z = diff(Fu_mat, 2, 1)*(1/cfg.spatial_interval(1)^2);
curl_Uz = curl_Uz + Fu_par_z(:, 2:end-1, 2:end-1, :);
clear Fu_par_z;
if cfg.num_dim == 3
  Fu_par_y = diff(Fu_mat, 2, 3)*(1/cfg.spatial_interval(3)^2);
  curl_Uz = curl_Uz + Fu_par_y(2:end-1, 2:end-1, :, :);
  clear Fu_par_y;
end
est_out = Fu_par_t(2:end-1, 2:end-1, 2:end-1, :) ./ curl_Uz;



