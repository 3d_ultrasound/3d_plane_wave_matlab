function est_out = elst_estimator_new_flt_before(cfg, uz_mat)
% Description:
%   Elsasticity estimation based on displacement in z dimension (uz_mat)
% Input:
%   uz_mat    uz_mat(z,x,y,f) is a four-dimensional array that represents
%             the desplacement in z, f means the fourth dimenstion is in
%             temperal frequency domain
%   cfg.time_interval     The time interval in two consecutive frames
%   cfg.num_dim           The number of spatial dimension
%   cfg.spatial_interval  The spatial resolution in each spatial dimension
% Output:
%   est_out   est_out(z,x,y,f) is the estimation output, f is the frequency index 

filter_order = 2;
filter_freq = [0, 3/16, 4/16, 1];
filter_response = [1, 1, 0, 0];
[filter_coef_b, filter_coef_a] = yulewalk(filter_order, filter_freq, filter_response);
[size_z, size_x, size_y, num_frame] = size(uz_mat);
uz_mat = filter(filter_coef_b,filter_coef_a,uz_mat,[],4);


uz_par_t = diff(uz_mat, 2, 4)*(1/cfg.time_interval^2);
uz_par_z = diff(uz_mat, 2, 1)*(1/cfg.spatial_interval(1)^2);
curl_uz = uz_par_z(:, 2:end-1, 2:end-1, :);
clear uz_par_z;
uz_par_x = diff(uz_mat, 2, 2)*(1/cfg.spatial_interval(2)^2);
curl_uz = curl_uz + uz_par_x(2:end-1, :, 2:end-1, :);
clear uz_par_x;

if cfg.num_dim == 3
  uz_par_y = diff(uz_mat, 2, 3)*(1/cfg.spatial_interval(3)^2);
  curl_uz = curl_uz + uz_par_y(2:end-1, 2:end-1, :, :);
  clear Fu_par_y;
end

sel_frame_t = 1:num_frame-2;
sum_uz_par_t = sum(abs(uz_par_t(2:end-1,2:end-1,2:end-1,sel_frame_t)), 4)/length(sel_frame_t);
clear uz_par_t;
sel_frame_curl = 1:num_frame;
sum_curl_uz = sum(abs(curl_uz(:, :, :,sel_frame_curl)), 4)/length(sel_frame_curl);
clear curl_uz;

threshold = 1.8;
med_curl_uz = med_filter_3d(sum_curl_uz, [5, 5, 5]);
bad_flag = (sum_curl_uz >= threshold*med_curl_uz) | (sum_curl_uz <= (1/threshold)*med_curl_uz);

patch_size = [5, 5, 5];
weight_mat = 0.3/16 * ones(patch_size);
weight_mat(2:4,2:4,2:4) = 0.7/8;
weight_mat(3,3,3) = 0;

for x_idx = 1:size_x-2
  for y_idx = 1:size_y-2
    for z_idx = 1:size_z-2
      if bad_flag(z_idx, x_idx, y_idx) ==  true
        x_set = max(1, x_idx - floor(patch_size(2)/2)) : min(size_x-2, x_idx + ceil(patch_size(2)/2)-1);
        y_set = max(1, y_idx - floor(patch_size(3)/2)) : min(size_y-2, y_idx + ceil(patch_size(3)/2)-1);
        z_set = max(1, z_idx - floor(patch_size(1)/2)) : min(size_z-2, z_idx + ceil(patch_size(1)/2)-1);
        patch_curl_uz = sum_curl_uz(z_set, x_set, y_set);
        good_flag = ~bad_flag(z_set, x_set, y_set);
        weight_patch = weight_mat(good_flag)./sum(weight_mat(good_flag));
        sum_curl_uz(z_idx, x_idx, y_idx) = sum(patch_curl_uz(good_flag).*weight_patch);
      end
    end
  end
end

est_out = sum_uz_par_t ./ sum_curl_uz;



