function B = gaussian_sep_filter_3d(A, sigma_vec, threshold)
% Description:
%   3-D Guassian filter
% Input
%   A   input data
%   sigma_vec  the sigma vaules of Guassian function in multiple dimension
%   threshold  the threshold that cut-off the Guassian window
% Output
%   B   output data
size_A = size(A);

half_N_1 = round(sqrt(-2*sigma_vec(1)^2*log(threshold)));
n_idx_1 = (-half_N_1:half_N_1).';
fir_tap_1 = exp(-(n_idx_1).^2/(2*sigma_vec(1)^2));
fir_tap_1 = fir_tap_1./sum(fir_tap_1);
first_filt_data = zeros(size_A(1)+2*half_N_1, size_A(2), size_A(3));
first_filt_data(1:end-2*half_N_1, :, :) = A;
first_filt_data = filter(fir_tap_1, 1, first_filt_data, [], 1);

half_N_2 = round(sqrt(-2*sigma_vec(2)^2*log(threshold)));
n_idx_2 = (-half_N_2:half_N_2).';
fir_tap_2 = exp(-(n_idx_2).^2/(2*sigma_vec(2)^2));
fir_tap_2 = fir_tap_2./sum(fir_tap_2);
second_filt_data = zeros(size_A(1)+2*half_N_1, size_A(2)+2*half_N_2, size_A(3));
second_filt_data(:, 1:end-2*half_N_2, :) = first_filt_data;
second_filt_data = filter(fir_tap_2, 1, second_filt_data, [], 2);

half_N_3 = round(sqrt(-2*sigma_vec(2)^2*log(threshold)));
n_idx_3 = (-half_N_3:half_N_3).';
fir_tap_3 = exp(-(n_idx_3).^2/(2*sigma_vec(3)^2));
fir_tap_3 = fir_tap_3./sum(fir_tap_3);
third_filt_data = zeros(size_A(1)+2*half_N_1, size_A(2)+2*half_N_2, size_A(3)+2*half_N_3);
third_filt_data(:, :, 1:end-2*half_N_3) = second_filt_data;
third_filt_data = filter(fir_tap_3, 1, third_filt_data, [], 3);

B = third_filt_data(half_N_1+1:end-half_N_1, half_N_2+1:end-half_N_2, half_N_3+1:end-half_N_3);