function lag_est_mat = group_1d_correlation(cfg, ref_frame, target_frame)
% Description:
%   1-D cross-correlation with parabolic fitting to improve the accuracy. 
%   This function is not recommended. It's just reserved for future study
dim = size(ref_frame);

if cfg.num_dim == 2
  dim(3) = 1;
elseif cfg.num_dim == 1
  dim(2) = 1;
  dim(3) = 1;
end
lag_est_mat = zeros(dim);

% x_mat = [1, -1, 1; 0, 0, 1; 1, 1, 1];
% x_mat_inv = inv(x_mat);
x_mat_inv = [0.5, -1, 0.5; 
            -0.5,  0, 0.5; 
               0,  1,   0];
             
for est_idx = 1:dim(1)
  ref_start_idx = max(1, est_idx - floor(cfg.corr_len/2));
  ref_end_idx = min(dim(1), est_idx + floor(cfg.corr_len/2));
  target_start_idx = max(1, est_idx - floor((cfg.search_win_size+cfg.corr_len)/2));
  target_end_idx = min(dim(1), est_idx + floor((cfg.search_win_size+cfg.corr_len)/2));
  ref_len = ref_end_idx - ref_start_idx + 1;
  target_len = target_end_idx - target_start_idx + 1;
  cnt_offset = ceil((ref_end_idx + ref_start_idx)/2) - ceil((target_end_idx + target_start_idx)/2);
  for y_idx = 1:dim(3)
    for x_idx = 1:dim(2)
      ref_frag = ref_frame(ref_start_idx:ref_end_idx, x_idx, y_idx);
      cc = xcorr(target_frame(target_start_idx:target_end_idx, x_idx, y_idx), ref_frag);
      [mv, m_idx] = max(cc);
      if m_idx == 1 || m_idx == 2*target_len-1
        idx_adjust = 0;
      else
        y = cc(m_idx-1:m_idx+1);
        abc = x_mat_inv*y;
        idx_adjust = -abc(2)/(2*abc(1));
      end
      idx_adjust = 0;
      lag_est_mat(est_idx, x_idx, y_idx) = m_idx + idx_adjust - ...
        (target_len + floor(target_len/2) - floor(ref_len/2)) - cnt_offset;
    end
  end
end
