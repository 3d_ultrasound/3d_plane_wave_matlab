function lag_est_mat = group_1d_correlation_phase(cfg, ref_frame, target_frame)
% Description:
%   1-D cross correlation along the first dimension (z dimension) using phase 
%   information to refine the estimation 
% Input:
%   ref_frame     ref_frame(z,x,y) is a three dimensional reference frame
%   target_frame  target_frame(z,x,y) is a three dimensional target frame
%   cfg.num_dim           the number of dimensions, can be 2 or 3
%   cfg.corr_len          the value indicates the correlation length
%   cfg.search_win_size   the value indicates the search window size
% Output:
%   lag_est_mat   the lag estimation output in number of samples


dim = size(ref_frame);
DBG_FLAG = false;

if cfg.num_dim == 2
  dim(3) = 1;
elseif cfg.num_dim == 1
  dim(2) = 1;
  dim(3) = 1;
end
lag_est_mat = zeros(dim);

win_func = ones(cfg.corr_len, 1);
% win_func = hamming(cfg.corr_len);

max_lag = floor(cfg.search_win_size/2);
cc = zeros(2*max_lag+1, 1);
for y_idx = 1:dim(3)
  for x_idx = 1:dim(2)
    for est_idx = 1+max_lag+floor(cfg.corr_len/2):dim(1)-max_lag-floor(cfg.corr_len/2)
      ref_start_idx = est_idx - floor(cfg.corr_len/2);
      ref_end_idx = est_idx + floor(cfg.corr_len/2);
      ref_frag = ref_frame(ref_start_idx:ref_end_idx, x_idx, y_idx).*win_func;
      ref_frag = ref_frag - mean(ref_frag);
      
      lag = -max_lag:max_lag;
      for lag_idx = lag;
        
        target_start_idx = est_idx - floor(cfg.corr_len/2) + lag_idx;
        target_end_idx = est_idx + floor(cfg.corr_len/2) + lag_idx;
        target_frag = target_frame(target_start_idx:target_end_idx, x_idx, y_idx);
        target_frag = target_frag - mean(target_frag);
        norm_factor = sqrt(target_frag'*target_frag);
        
        cc(lag_idx+max_lag+1) = ref_frag'*target_frag/norm_factor;
      end
      [mv, max_idx] = max(abs(cc));
      if max_idx > 1 && max_idx < 2*max_lag+1
        peak_samples = cc(max_idx-1:max_idx+1);
        phase_y = angle(peak_samples);
        idx_adjust = 2*sum(phase_y)/(3*phase_y(1)-phase_y(3));
        if idx_adjust < -1 || idx_adjust > 1
          idx_adjust = 0;
        end
      else
        idx_adjust = 0;
      end
      lag_est_mat(est_idx, x_idx, y_idx) = lag(max_idx) + idx_adjust;
    end
  end
end
