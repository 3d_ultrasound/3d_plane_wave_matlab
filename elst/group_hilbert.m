function x = group_hilbert(xr, n)
% Description:
%   1-D group hilbert transform along the first dimension
% Input:
%   xr    the input data
%   n     the number of points of FFT
% Output:
%   x     the output data

if nargin<2, n=[]; end
if ~isreal(xr)
  warning(message('signal:hilbert:Ignore'))
  xr = real(xr);
end

if isempty(n)
  n = size(xr,1);
end
x = fft(xr,n,1); % n-point FFT over columns.
h  = zeros(n,~isempty(x)); % nx1 for nonempty. 0x0 for empty.
if n > 0 && 2*fix(n/2) == n
  % even and nonempty
  h([1 n/2+1]) = 1;
  h(2:n/2) = 2;
elseif n>0
  % odd and nonempty
  h(1) = 1;
  h(2:(n+1)/2) = 2;
end
size_x = size(x);
x = ifft(x.*repmat(h, [1, size_x(2:end)]));
