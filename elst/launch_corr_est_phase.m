function launch_corr_est_phase(core, num_cpd_frame)
% Description:
%   Correlation that uses phase information
% Input:
%    core:            process id
%    num_cpd_frame:   the number of correlated frames you want to process

total_cores = 4;
paths = set_paths();
addpath(genpath(paths.elst));
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'elst_params');

corr_cfg.corr_len = 61;
corr_cfg.search_win_size = 31;
corr_cfg.num_dim = 3;

load(sprintf('%s/cpd_image_%d.mat', paths.image, 1), 'cpd_frame');

ref_frame = group_hilbert(cpd_frame);

jobs = mod(0:num_cpd_frame-1, total_cores)+1;
all_frame = 1:num_cpd_frame;
my_jobs = all_frame(jobs == core);

for cpd_frame_idx = my_jobs
  fprintf('Working on frame # %d\n', cpd_frame_idx);
  load(sprintf('%s/cpd_image_%d.mat', paths.image, cpd_frame_idx), 'cpd_frame');
  target_frame = group_hilbert(cpd_frame);
  lag_est_mat = group_1d_correlation_phase(corr_cfg, ref_frame, target_frame);
  save(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
end

