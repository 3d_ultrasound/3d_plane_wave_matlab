function launch_elst_est(start_frame_idx, end_frame_idx, elst_type)
% Description:
%   Elasticity estimation on cross correaltion data
% Input:
%   start_frame_idx  start the estimation from this frame
%   end_frame_idx:   end the estimation from this frame

if nargin < 3
  elst_type = 'flt_before';
end

paths = set_paths();
addpath(genpath(paths.elst));
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'elst_params', 'img_params', 'elst_params');


load(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, start_frame_idx), 'lag_est_mat');
Uz_mat = zeros([size(lag_est_mat), end_frame_idx-start_frame_idx+1]);
Uz_mat(:, :, :, 1) = lag_est_mat*img_params.range_resolution;
for cpd_frame_idx = start_frame_idx+1:end_frame_idx
  load(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
  Uz_mat(:, :, :, cpd_frame_idx-start_frame_idx+1) = lag_est_mat*img_params.range_resolution;
end

elst_cfg.time_interval = elst_params.firing_time_interval*sys_params.num_angles*elst_params.frame_skip;
elst_cfg.spatial_interval = [img_params.range_resolution;
                        (sys_params.rx_width + sys_params.rx_kerf)/img_params.lateral_interp_rate;
                        (sys_params.rx_height + sys_params.rx_kerf)/img_params.lateral_interp_rate];
elst_cfg.num_dim = 3;

switch lower(elst_type)
  case 'baseline'
    est_out = elst_estimator_baseline(elst_cfg, Uz_mat);
  case 'flt_after'
    est_out = elst_estimator_new_flt_after(elst_cfg, Uz_mat);
  case 'flt_before'
    est_out = elst_estimator_new_flt_before(elst_cfg, Uz_mat);
  otherwise
    error('Unrecognized estimator type.');
end

save(sprintf('%s/est_out.mat', paths.elst_data), 'est_out');