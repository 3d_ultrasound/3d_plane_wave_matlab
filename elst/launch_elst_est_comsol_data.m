function launch_elst_est_comsol_data(start_frame_idx, end_frame_idx, elst_type)
% Description:
%   Elasticity estimation on COMSOL data
% Input:
%   start_frame_idx  start the estimation from this frame
%   end_frame_idx:   end the estimation from this frame

if nargin < 3
  elst_type = 'flt_before';
end

paths = set_paths();
addpath(genpath(paths.elst));
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'elst_params', 'img_params', 'elst_params');

Uz_mat = zeros([elst_params.comsol_grid_size(3), elst_params.comsol_grid_size(1), ...
  elst_params.comsol_grid_size(2), end_frame_idx-start_frame_idx+1]);

frame_interval = 5;
for cpd_frame_idx = start_frame_idx:end_frame_idx
  firing_id = (cpd_frame_idx-1)*frame_interval + 1;
  load(sprintf('%s/shear_wave_disp_fire_%d.mat', paths.comsol_data, firing_id), 'disp');
  Uz_mat(:, :, :, cpd_frame_idx-start_frame_idx+1) = shiftdim(disp.disp_z, 2);
end

load(sprintf('%s/coord_grid.mat', paths.comsol_data), 'grid');

elst_cfg.time_interval = elst_params.firing_time_interval*frame_interval;
elst_cfg.spatial_interval = [grid.grid_z(1, 1, 2) - grid.grid_z(1, 1, 1);
                             grid.grid_x(2, 1, 1) - grid.grid_x(1, 1, 1);
                             grid.grid_y(1, 2, 1) - grid.grid_y(1, 1, 1)];
elst_cfg.num_dim = 3;

switch lower(elst_type)
  case 'baseline'
    est_out = elst_estimator_baseline(elst_cfg, Uz_mat);
  case 'flt_after'
    est_out = elst_estimator_new_flt_after(elst_cfg, Uz_mat);
  case 'flt_before'
    est_out = elst_estimator_new_flt_before(elst_cfg, Uz_mat);
  otherwise
    error('Unrecognized estimator type.');
end

save(sprintf('%s/est_comsol_out.mat', paths.elst_data), 'est_out');
