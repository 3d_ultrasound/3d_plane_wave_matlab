function launch_elst_preprocess(core, num_cpd_frame)
% Description:
%   This function pre processes the correlation data for elasticity estimator
%   The pre-process includes median filtering, and 3-D Guassian filtering
% Input:
%    core:            process id
%    num_cpd_frame:   the number of correlated frames you want to process
total_cores = 2;

paths = set_paths();
addpath(genpath(paths.elst));
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'elst_params');

jobs = mod(0:num_cpd_frame-1, total_cores)+1;
all_frame = 1:num_cpd_frame;
my_jobs = all_frame(jobs == core);

for cpd_frame_idx = my_jobs
  fprintf('Working on frame # %d\n', cpd_frame_idx);
  load(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
  inter_mat = med_filter_3d(lag_est_mat, [3, 3, 3]);
  lag_est_mat = gaussian_sep_filter_3d(inter_mat, [3, 1, 1], 0.1);
  save(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
end
