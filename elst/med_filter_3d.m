function B = med_filter_3d(A, winsize)
% Description:
%   3-D median filter
% Input:
%   A         input 3-D array
%   winsize   filter window sizes in three dimensions
% Output:
%   B         output filtered 3-D array

A_size = size(A);
B = zeros(size(A));
z_size_left = floor(winsize(2)/2);
z_size_right = ceil(winsize(1)/2)-1;
for y_idx = 1:A_size(3)
  y_set = max(1, y_idx - floor(winsize(3)/2)):min(A_size(3), y_idx + ceil(winsize(3)/2) -1); 
  for x_idx = 1:A_size(2)
    x_set = max(1, x_idx - floor(winsize(2)/2)):min(A_size(3), x_idx + ceil(winsize(2)/2)-1);
    for z_idx = 1:A_size(1)
      z_set = max(1, z_idx - z_size_left):min(A_size(1), z_idx + z_size_right); 
      vec = reshape(A(z_set, x_set, y_set), 1, []);
      B(z_idx, x_idx, y_idx) = median(vec);
    end
  end
end

