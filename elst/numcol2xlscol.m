function xlscol = numcol2xlscol(numcol)
% Description:
%   Translate column index into alphabetic column index used in excel form

base = 26;
string_mapping_table = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', ...
  'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',];
if numcol == 1
  r = 0;
  num_char = 1;
else
  q = numcol-1;
  idx = 1;
  r = [];
  while q > 0
    next_q = floor(q/base);
    r(idx) = q - next_q*base;
    q = next_q;
    idx = idx + 1;
  end
  num_char = idx -1;
  xlscol = '';
end
for idx = 1:num_char
  xlscol(idx) = string_mapping_table(r(num_char-idx+1)+1);
end
