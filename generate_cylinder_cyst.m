%  Create cylinder a cyst phantom
%
%  Calling: [positions, amp] = cylinder_pht (N);
%
%  Parameters:  N - Number of scatterers in the phantom
%
%  Output:      positions  - Positions of the scatterers.
%               amp        - amplitude of the scatterers.
%

function [positions, amp] = generate_cylinder_cyst (cyst_params)

% set random seed
rnd_seed = RandStream('mt19937ar','seed', 101);
N = cyst_params.scatters;

x_size = cyst_params.x_size;   %  Width of phantom [mm]
y_size = cyst_params.y_size;   %  Transverse width of phantom [mm]
z_size = cyst_params.z_size;   %  Height of phantom [mm]
z_start = cyst_params.z_start;  %  Start of phantom surface [mm];

%  Create the general scatterers

x = (rand(rnd_seed, N,1)-0.5)*x_size;
y = (rand(rnd_seed, N,1)-0.5)*y_size;
z = rand(rnd_seed, N,1)*z_size + z_start;

%  Generate the amplitudes with a Gaussian distribution

amp=randn(rnd_seed, N,1);

%  Make the cyst and set the amplitudes to zero inside

%  10 mm cylinder
r=  cyst_params.cyst_r;      %  Radius of cyst [mm]
xc= cyst_params.cyst_xc;     %  Place of cyst [mm]
zc= cyst_params.cyst_zc;
positions=[x y z];

inside = ( ((x-xc).^2 + (z-zc).^2) < r^2);

enable_dbg_plot = false;
if enable_dbg_plot
  figure;
  plot3(x(~inside),y(~inside),z(~inside), 'b.', 'MarkerSize', 0.2);
  hold on;
  plot3(x(inside),y(inside),z(inside), 'r*', 'MarkerSize', 4);
  hold off;
  axis equal;
end

positions(inside, :) = [];
amp(inside) = [];

