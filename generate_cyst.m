%  Create a computer model of a cyst phantom. The phantom contains
%  fiven point targets and 6, 5, 4, 3, 2 mm diameter waterfilled cysts,
%  and 6, 5, 4, 3, 2 mm diameter high scattering regions. All scatterers
%  are situated in a box of (x,y,z)=(50,10,60) mm and the box starts
%  30 mm from the transducer surface.
%
%  Calling: [positions, amp] = cyst_phantom (N);
%
%  Parameters:  N - Number of scatterers in the phantom
%
%  Output:      positions  - Positions of the scatterers.
%               amp        - amplitude of the scatterers.
%
%  Version 2.2, April 2, 1998 by Joergen Arendt Jensen

function [positions, amp] = generate_cyst (param)

% set random seed
rnd_seed = RandStream('mt19937ar','seed', 100);

N = param.scatters;
%  Create the general scatterers

x = (rand(rnd_seed, N,1)-0.5)*param.cyst_x_size;
y = (rand(rnd_seed, N,1)-0.5)*param.cyst_y_size;
z = rand(rnd_seed, N,1)*param.cyst_z_size + param.cyst_z_start;

%  Generate the amplitudes with a Gaussian distribution

amp=randn(rnd_seed, N,1);

%  Make the cyst and set the amplitudes to zero inside

inside = false(N, 1);
for cyst = 1:param.cyst_num
	inside = inside | ( ((x-param.cyst_xc(cyst)).^2 + (y-param.cyst_yc(cyst)).^2 + (z-param.cyst_zc(cyst)).^2) < param.cyst_r(cyst)^2 );
end

enable_dbg_plot = false;
if enable_dbg_plot
  figure;
  plot3(x(~inside),y(~inside),z(~inside), 'b.', 'MarkerSize', 0.2);
  hold on;
  plot3(x(inside),y(inside),z(inside), 'r*', 'MarkerSize', 4);
  hold off;
  axis equal;
end



% Remove the scatters inside the cysts
x(inside) = [];
y(inside) = [];
z(inside) = [];
amp(inside) = [];


phi = asin(y./sqrt(z.^2 + y.^2));
r = sqrt(x.^2 + z.^2 + y.^2);
theta = asin(x./r);
theta_rot = param.theta_rotate;
phi_rot = param.phi_rotate;

x_rot = r.*sin(theta + theta_rot);
y_rot = r.*cos(theta + theta_rot).*sin(phi + phi_rot);
z_rot = r.*cos(theta + theta_rot).*cos(phi + phi_rot);

positions=[x_rot y_rot z_rot];

