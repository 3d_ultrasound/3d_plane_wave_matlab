%  Create cylinder a cyst phantom
%
%  Calling: [positions, amp] = cylinder_pht (N);
%
%  Parameters:  N - Number of scatterers in the phantom
%
%  Output:      positions  - Positions of the scatterers.
%               amp        - amplitude of the scatterers.
%

function [positions, amp] = generate_elst_cyst(cyst_params, elst_params, firing_id)
disp_adjust_factor = 16;

paths = set_paths();
addpath(genpath(paths.elst));

% set random seed
rnd_seed = RandStream('mt19937ar','seed', 101);
N = cyst_params.scatters;

x_size = cyst_params.x_size;   %  Width of phantom [mm]
y_size = cyst_params.y_size;   %  Transverse width of phantom [mm]
z_size = cyst_params.z_size;   %  Height of phantom [mm]
z_start = cyst_params.z_start;  %  Start of phantom surface [mm];

%  Create the general scatterers

x = (rand(rnd_seed, N,1)-0.5)*x_size;
y = (rand(rnd_seed, N,1)-0.5)*y_size;
z = rand(rnd_seed, N,1)*z_size + z_start;
amp=randn(rnd_seed, N,1);

%  Generate the amplitudes with a Gaussian distribution
load(sprintf('%s/coord_grid.mat', paths.comsol_data), 'grid');
load(sprintf('%s/shear_wave_disp_fire_%d.mat', paths.comsol_data, firing_id), 'disp');

dx = interpn(grid.grid_x, grid.grid_y, grid.grid_z, disp.disp_x, x, y, z);
dy = interpn(grid.grid_x, grid.grid_y, grid.grid_z, disp.disp_y, x, y, z);
dz = interpn(grid.grid_x, grid.grid_y, grid.grid_z, disp.disp_z, x, y, z);

dx(isnan(dx)) = 0;
dy(isnan(dy)) = 0;
dz(isnan(dz)) = 0;

positions = [x+ disp_adjust_factor*dx, y+ disp_adjust_factor*dy, z+ disp_adjust_factor*dz];



