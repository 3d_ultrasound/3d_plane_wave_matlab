function rx_transducers = generate_receive()

%%%%%%%%%% Initial Setup  %%%%%%%%%%

paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params');

rx_transducers.rx_layout = zeros(sys_params.rx_size_x*sys_params.rx_size_y, 19);
rx_transducers.rx_center = zeros(sys_params.rx_size_x*sys_params.rx_size_y, 3);

%%%%%%%%%% Determine Shift  %%%%%%%%%%

delta_x = sys_params.rx_width + sys_params.rx_kerf;
delta_y = sys_params.rx_height + sys_params.rx_kerf;

% Starts in bottom left
center_adjust_x = -(sys_params.tx_kerf + sys_params.tx_width)/2;
center_adjust_y = -(sys_params.tx_kerf + sys_params.tx_height)/2;

first_x = -(sys_params.rx_size_x / 2 * delta_x - sys_params.rx_kerf/2) + center_adjust_x;
first_y = -(sys_params.rx_size_y / 2 * delta_y - sys_params.rx_kerf/2) + center_adjust_y;


%%%%%%%%%% Generate Position  %%%%%%%%%%

for y = 1:sys_params.rx_size_y
  for x = 1:sys_params.rx_size_x
    x_start = first_x + (x-1) * delta_x;
    y_start = first_y + (y-1) * delta_y;

    t = x + (y-1) * sys_params.rx_size_x;

    rx_transducers.rx_layout(t,1) = t;
    rx_transducers.rx_layout(t,2) = x_start;
    rx_transducers.rx_layout(t,3) = y_start;
    rx_transducers.rx_layout(t,4) = 0;
    rx_transducers.rx_layout(t,5) = x_start;
    rx_transducers.rx_layout(t,6) = y_start + sys_params.rx_height;
    rx_transducers.rx_layout(t,7) = 0;
    rx_transducers.rx_layout(t,8) = x_start + sys_params.rx_width;
    rx_transducers.rx_layout(t,9) = y_start + sys_params.rx_height;
    rx_transducers.rx_layout(t,10) = 0;
    rx_transducers.rx_layout(t,11) = x_start + sys_params.rx_width;
    rx_transducers.rx_layout(t,12) = y_start;
    rx_transducers.rx_layout(t,13) = 0;
    rx_transducers.rx_layout(t,14) = 1;
    rx_transducers.rx_layout(t,15) = sys_params.rx_width;
    rx_transducers.rx_layout(t,16) = sys_params.rx_height;
    rx_transducers.rx_layout(t,17) = x_start + sys_params.rx_width/2;
    rx_transducers.rx_layout(t,18) = y_start + sys_params.rx_height/2;
    rx_transducers.rx_layout(t,19) = 0;

    rx_transducers.rx_center(t,1) = rx_transducers.rx_layout(t,17);
    rx_transducers.rx_center(t,2) = rx_transducers.rx_layout(t,18);
    rx_transducers.rx_center(t,3) = rx_transducers.rx_layout(t,19);

  end
end


