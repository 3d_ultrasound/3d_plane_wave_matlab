function generate_scatters(firing_id)

m = 1;
cm = 1/100;
mm = 1/1000;
MHz = 1e6;

%%%%%%%%%% Initial Setup  %%%%%%%%%%
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'phys_params');

addpath(genpath(paths.field));
field_init(0);

fc = phys_params.fc;
Tc = phys_params.Tc;
fs = phys_params.fs;
Ts = phys_params.Ts;

c = phys_params.c;

B_6db = phys_params.B_6db;
interp_factor = phys_params.interp_factor;

clear phys_params;

%%%%%%%%%% Set Field Values %%%%%%%%%%

load(sprintf('%s/global_params.mat', paths.params), 'img_params', 'sys_params');

set_field('fs', fs);
set_field('c', c);

if img_params.atten_flag == true
    att = img_params.atten_value;  % dB / [MHz cm]
    set_field('att',att*fc/MHz/cm);
    set_field('Freq_att',att/MHz/cm);
    set_field('att_f0',fc);
    set_field('use_att',1);
else
    set_field('use_att', 0);
end


%%%%%%%%%% Create Response %%%%%%%%%%

sgm_sq = 3/5/((pi*B_6db)^2*log10(exp(1))); % impulse response
T_sig = 2*ceil(sqrt(6*sgm_sq/log10(exp(1)))*fc)/fc;
t = 0:Ts:T_sig;
h_g = 1e3*exp(-(t-T_sig/2).^2/(2*sgm_sq)); % baseband response

carrier = cos(2*pi*fc*t);
impulse_response = h_g .* carrier;


%%%%%%%%%% Generate Apertures %%%%%%%%%%

rx = generate_receive();
tx = generate_transmit();

receive_aperture = xdc_rectangles(rx.rx_layout, rx.rx_center, img_params.focus);
transmit_aperture = xdc_rectangles(tx.tx_layout, tx.tx_center, img_params.focus);

xdc_impulse(receive_aperture, impulse_response);
xdc_impulse(transmit_aperture, impulse_response);

temp = xdc_get(receive_aperture, 'rect');
rx.Rx_coord = temp(8:10,:)';

temp = xdc_get(transmit_aperture, 'rect');
tx.Tx_coord = temp(8:10,:)';

%%%%%%%%%% Generate phantom %%%%%%%%%%

load(sprintf('%s/global_params.mat', paths.params), 'cyst_params', 'elst_params');

switch lower(cyst_params.type)
  case 'point'
    phantom_positions = cyst_params.phantom_positions;
    phantom_amplitudes = 1 * ones(1, size(phantom_positions, 1)).';
  case 'sphere'
    [phantom_positions, phantom_amplitudes] = generate_cyst(cyst_params);
  case 'cylinder'
    [phantom_positions, phantom_amplitudes] = generate_cylinder_cyst(cyst_params);
  case 'elst'
    [phantom_positions, phantom_amplitudes] = ...
      generate_elst_cyst(cyst_params, elst_params, (firing_id-1)*elst_params.frame_skip + 1);
end

clear cyst_params;

%%%%%%%%%% Create Image Space  %%%%%%%%%%

z_range = img_params.z_range;

zero_padding = 0;
max_len = floor(z_range(2)*2 / c / (Ts*interp_factor)) + zero_padding;
max_len = floor(max_len/interp_factor)*interp_factor+1;

excitation = sin(2*pi*fc*(0:(1/fs):1/fc));
excitation_waveform = conv(impulse_response, excitation);

xdc_excitation(transmit_aperture, excitation_waveform);
angle_id = mod(firing_id-1, sys_params.num_angles)+1;
alpha = sys_params.firing_angle_alpha(angle_id);
beta = sys_params.firing_angle_beta(angle_id);
norm_vec = [sin(alpha)*cos(beta); sin(alpha)*sin(beta); cos(alpha)];
pin_point_coord = repmat(sys_params.pin_point_coord(angle_id, :), size(tx.Tx_coord, 1), 1);
element_delay = (tx.Tx_coord - pin_point_coord)*norm_vec/c;
ele_delay(transmit_aperture, (1:size(tx.Tx_coord, 1)).', element_delay);

filter_delay = length(impulse_response)*3/2;

%excitation = sin(2*pi*fc*(0:(1/fs):2/fc));
%excitation = excitation.*hamming(length(excitation)).';
%xdc_excitation (transmit_aperture, excitation);

%filter_delay = round(length(impulse_response)+length(excitation)/2);
%filter_delay = filter_delay - round(abs(tx.vrt_src_pos(3))/(c*Ts));

if img_params.noise_flag == true
  noise = 10^(-420/20)*randn(max_len, sys_params.rx_size_x*sys_params.rx_size_y);
  rx_signal = noise;

else
  % Use calc_scat_multi() to replace calc_scat_all() to increase the processing speed
  [scat, time_ref] = calc_scat_multi(transmit_aperture, receive_aperture, ...
                                   phantom_positions, phantom_amplitudes);

  time_ref_pt = round(time_ref / (Ts*interp_factor));

  rx_signal = zeros(max_len, sys_params.rx_size_x*sys_params.rx_size_y);

  rx_signal(time_ref_pt : time_ref_pt + ceil(size(scat, 1)/interp_factor) -1, :) = scat(1:interp_factor:end, :);

end

xdc_free (transmit_aperture)
xdc_free (receive_aperture)

%%%%%%%%%% Write Data  %%%%%%%%%%

save(sprintf('%s/raw_data_firing_%d.mat', paths.echo, firing_id), 'rx_signal', 'rx', 'tx');

