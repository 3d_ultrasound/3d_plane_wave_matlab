function tx_transducers = generate_transmit()

%%%%%%%%%% Initial Setup  %%%%%%%%%%

paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params');

tx_transducers.tx_layout = zeros(sys_params.tx_size_x*sys_params.tx_size_y, 19);
tx_transducers.tx_center = zeros(sys_params.tx_size_x*sys_params.tx_size_y, 3);

%%%%%%%%%% Determine Shift  %%%%%%%%%%

delta_x = sys_params.tx_width + sys_params.tx_kerf;
delta_y = sys_params.tx_height + sys_params.tx_kerf;

% Starts in bottom left

first_x = -(sys_params.tx_size_x / 2 * delta_x - sys_params.tx_kerf/2);
first_y = -(sys_params.tx_size_y / 2 * delta_y - sys_params.tx_kerf/2);


%%%%%%%%%% Generate Position  %%%%%%%%%%

for y = 1:sys_params.tx_size_y
  for x = 1:sys_params.tx_size_x
    x_start = first_x + (x-1) * delta_x;
    y_start = first_y + (y-1) * delta_y;

    t = x + (y-1) * sys_params.tx_size_x;

    tx_transducers.tx_layout(t,1) = t;
    tx_transducers.tx_layout(t,2) = x_start;
    tx_transducers.tx_layout(t,3) = y_start;
    tx_transducers.tx_layout(t,4) = 0;
    tx_transducers.tx_layout(t,5) = x_start;
    tx_transducers.tx_layout(t,6) = y_start + sys_params.tx_height;
    tx_transducers.tx_layout(t,7) = 0;
    tx_transducers.tx_layout(t,8) = x_start + sys_params.tx_width;
    tx_transducers.tx_layout(t,9) = y_start + sys_params.tx_height;
    tx_transducers.tx_layout(t,10) = 0;
    tx_transducers.tx_layout(t,11) = x_start + sys_params.tx_width;
    tx_transducers.tx_layout(t,12) = y_start;
    tx_transducers.tx_layout(t,13) = 0;
    tx_transducers.tx_layout(t,14) = 1;
    tx_transducers.tx_layout(t,15) = sys_params.tx_width;
    tx_transducers.tx_layout(t,16) = sys_params.tx_height;
    tx_transducers.tx_layout(t,17) = x_start + sys_params.tx_width/2;
    tx_transducers.tx_layout(t,18) = y_start + sys_params.tx_height/2;
    tx_transducers.tx_layout(t,19) = 0;

    tx_transducers.tx_center(t,1) = tx_transducers.tx_layout(t,17);
    tx_transducers.tx_center(t,2) = tx_transducers.tx_layout(t,18);
    tx_transducers.tx_center(t,3) = tx_transducers.tx_layout(t,19);

  end
end



