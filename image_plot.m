paths = set_paths();
addpath(genpath(paths.misc));
load(sprintf('%s/img_complete.mat', paths.image), 'total_frame');

% %%%%%%%%%% Demodulation  %%%%%%%%%%
% load(sprintf('%s/global_params.mat', paths.params), 'phys_params');
% undemod_total_frame = total_frame;
% c = phys_params.c;
% fc = phys_params.fc;
% range_resolution = phys_params.lambda/(2*phys_params.fs/phys_params.interp_factor/phys_params.fc);
% Ts_line = range_resolution*2/c;
% fs_line = 1 / Ts_line;
%
%
% demodulation_carrier =  repmat(exp(2j*pi*fc*(0:Ts_line:(size(undemod_total_frame,1)-1)*Ts_line)).', ...
%     [1, size(undemod_total_frame,2), size(undemod_total_frame,3)]);
% % Filter design
% % d = fdesign.lowpass('Fp,Fst,Ap,Ast', phys_params.B_6db/2, fc*2-phys_params.B_6db/2, 1, 40, fs_line);
% d = fdesign.lowpass('Fp,Fst,Ap,Ast', phys_params.B_6db/2, fc, 1, 40, fs_line);
% hd = design(d,'kaiserwin');
% b_LPF = hd.Numerator;
%
% % Multiply with carrier
% demodulated = undemod_total_frame.*demodulation_carrier;
%
% % Low pass filtering
% total_frame = filter(b_LPF, 1, demodulated);
%
% total_frame = abs(total_frame);
load(sprintf('%s/global_params.mat', paths.params));

log_total_frame = 20*log10(total_frame);
dynamic_range = 40;
log_total_frame = log_total_frame - max(log_total_frame(:)) + dynamic_range;
log_total_frame(log_total_frame<0) = 0;

% index = 17;
% center_line_z1 = squeeze(log_total_frame(:,index,index));
% center_line_z = max(squeeze(max(log_total_frame,[],2)),[],2);
%
index_z = linspace(img_params.z_range(1),img_params.z_range(2),max(size(log_total_frame)));
% delta_z = (img_params.z_range(2)-img_params.z_range(1))/(max(size(log_total_frame))-1);
% plot(index_z*1000,center_line_z)
%
% [max_val, max_idx] = max(center_line_z);
% z_pos = index_z(max_idx);
% xlabel('depth(mm)')
% ylabel('magnitude(dB)')
% grid on
% xlim([0,60])
% addpath('C:\Siyuan_Matlab\export_fig')
% fig_auto_adjustment(1.2)
% % export_fig('./cell_resolution_z.pdf')
% close
% [max_temp, max_index_z] = max(center_line_z);
% half_max = max(center_line_z) + 20*log10(1/2);
% [min_temp, left_half_max] = min(abs(half_max - center_line_z(1:max_index_z)));
% [min_temp_2, right_half_max] = min(abs(half_max - center_line_z(1+max_index_z:end)));
% right_half_max = right_half_max + max_index_z;
% FWHM_z = (right_half_max - left_half_max)*delta_z*1000; %unit:mm
%
% xy_plane = squeeze(max(log_total_frame,[],1));
% imagesc(xy_plane);
% colormap gray
% xy_plane_interp = interp1(1:32,xy_plane,1:0.25:32, 'spline');
% xy_plane_interp = interp1(1:32,xy_plane_interp',1:0.25:32, 'spline');
% imagesc(xy_plane_interp);
% colormap gray
% center_line_x = squeeze(max(xy_plane_interp,[],1));
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);
index_x = linspace(min(sep_focus_x(:)), max(sep_focus_y(:)), size(log_total_frame,2));
% delta_x = (max(sep_focus_x(:))-min(sep_focus_x(:)))/(size(log_total_frame,2)-1);
% % figure;
% % plot(index_x*1000,center_line_x)
% % xlabel('lateral-x(mm)')
% % ylabel('magnitude(dB)')
% % grid on
% %
% % fig_auto_adjustment(1.8)
% % export_fig('./cell_resolution_x.pdf')
% % close
% [max_temp, max_index_x] = max(center_line_x);
% half_max = max(center_line_x) + 20*log10(1/2);
% [min_temp, left_half_max] = min(abs(half_max - center_line_x(1:max_index_x)));
% [min_temp_2, right_half_max] = min(abs(half_max - center_line_x(1+max_index_x:end)));
% right_half_max = right_half_max + max_index_x;
% FWHM_x = (right_half_max - left_half_max)*delta_x/4*1000; %unit:mm
%
% center_line_y = squeeze(log_total_frame(max_index_z,index,:));
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);
index_y = linspace(min(sep_focus_y(:)), max(sep_focus_y(:)), size(log_total_frame,3));
% delta_y = (max(sep_focus_y(:))-min(sep_focus_y(:)))/(size(log_total_frame,3)-1);
% figure;
% plot(index_y*1000,center_line_y)
% xlabel('lateral-y(mm)')
% ylabel('magnitude(dB)')
% grid on
% fig_auto_adjustment(1.8)
% % export_fig('./cell_resolution_y.pdf')
% close
% [max_temp, max_index_y] = max(center_line_y);
% half_max = max(center_line_y) + 20*log10(1/2);
% [min_temp, left_half_max] = min(abs(half_max - center_line_y(1:max_index_y)));
% [min_temp_2, right_half_max] = min(abs(half_max - center_line_y(1+max_index_y:end)));
% right_half_max = right_half_max + max_index_y;
% FWHM_y = (right_half_max - left_half_max)*delta_y*1000; %unit:mm

center_slice_xy = squeeze(max(log_total_frame,[],1));
figure;
imagesc(index_x'*1000, index_y'*1000, center_slice_xy)
% imagesc(index_x*1000, index_y*1000, xy_plane)
colormap  gray
colorbar
xlabel('lateral-x(mm)')
ylabel('lateral-y(mm)')
fig_auto_adjustment(1.8)
% export_fig('./cell_resolution_slice_xy.pdf')
% close

% center_slice_xz = squeeze(max(log_total_frame,[],2));
% figure;
% imagesc(index_x*1000, index_z*1000, center_slice_xz)
% % imagesc(index_x*1000, index_y*1000, xy_plane)
% colormap  gray
% colorbar
% xlabel('lateral-x(mm)')
% ylabel('lateral-y(mm)')
% fig_auto_adjustment(1.8)
% % scatter_density = 10/(FWHM_x*FWHM_y*FWHM_z);