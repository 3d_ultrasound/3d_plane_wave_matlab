function launch_beamsum(core, bf_type)
% File to control launching of scatter generation
paths = set_paths();
addpath(genpath(paths.bf_functions));
addpath(genpath(paths.param_friends));

paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'elst_params');

if elst_params.is_elst_mode == true
  num_firings = floor(elst_params.sim_duration / (elst_params.firing_time_interval*elst_params.frame_skip));
else
  num_firings = sys_params.num_angles;
end

total_cores = 1;

jobs = mod(0:num_firings-1, total_cores)+1;
firings = 1:num_firings;
my_jobs = firings(jobs == core);
parfor firing_id = my_jobs
  fprintf('Working on firing # %d\n', firing_id);
%   tic
  switch lower(bf_type)
    case '2d'
      generate_beamsum_plain_wave_2d(firing_id);
    case '2d_compressed'
      generate_beamsum_plain_wave_compressed_2d(firing_id);
    case 'nonsep'
      generate_beamsum_plain_wave(firing_id);
    case 'nonsep_compressed'
      generate_beamsum_plain_wave_nonsep_compressed(firing_id);
    case 'sep'
      generate_beamsum_plain_wave_sep(firing_id);
    case 'sep_compressed'
      generate_beamsum_plain_wave_sep_compressed(firing_id);
    case 'sep_compressed_fixed_point'
      generate_beamsum_plain_wave_sep_compressed_fixed_point(firing_id);
    case 'sep_iter'
      generate_beamsum_plain_wave_sep_iter(firing_id);
    case 'test'
      generate_beamsum_plain_wave_sep_compressed_TEST(firing_id);
    otherwise
      error('Unknown constant type.');
  end
%   toc
end

