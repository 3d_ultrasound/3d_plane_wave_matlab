function launch_constants(core, constant_type)
% Description
%     Generate iterative delay for plane wave separable beamforming and
%     save in paths.delay folder
% Input:
%     constant_type: 'sep' for separable delay or 'nonsep' non-separable delay
% Output:
%     none
paths = set_paths;
addpath(genpath(paths.constant_gen));
addpath(genpath(paths.param_friends));

paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params');

num_firings = sys_params.num_angles;
total_cores = 1;

jobs = mod(0:num_firings-1, total_cores)+1;
firings = 1:num_firings;
my_jobs = firings(jobs == core);

parfor firing_id = my_jobs
  fprintf('Working on firing # %d\n', firing_id);
%   tic
  switch lower(constant_type)
    case '2d_compressed'
      compressed_2d_gen_plane_wave(firing_id);
    case 'nonsep_compressed'
      nonsep_compressed_gen_plane_wave(firing_id);
    case 'sep'
      sep_gen_plane_wave(firing_id);
    case 'sep_compressed'
      sep_compressed_gen_plane_wave(firing_id);
    case 'sep_compressed_fixed_point'
      sep_compressed_fixed_point_gen_plane_wave(firing_id);
    case 'sep_iter'
      iter_coef_sep_gen_plane_wave(firing_id);
    case 'test'
      sep_compressed_gen_plane_wave_TEST(firing_id);
    case 'nonsep'
      disp('Non-separable delay calculation is performed on-the-fly in the beamsum.');
    otherwise
      error('Unknown constant type.');
  end
%   toc
end





