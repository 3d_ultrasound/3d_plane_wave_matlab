function launch_scatters(varargin)
% File to control launching of scatter generation

paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'elst_params');

if elst_params.is_elst_mode == true
  num_firings = floor(elst_params.sim_duration / (elst_params.firing_time_interval*elst_params.frame_skip));
else
  num_firings = sys_params.num_angles;
end

if(isempty(varargin))
    first_firing = 1;
    last_firing = num_firings;
elseif(length(varargin) ~= 2)
    fprintf('Error: launch_scatters() takes either zero or two arguments.\n');
    fprintf('Usage (for computing all firings on this machine): launch_scatters()\n');
    fprintf('Usage (for computing only some firings on this machine): launch_scatters(first_firing, last_firing)\n');
    return;
else
    first_firing = varargin{1};
    last_firing = varargin{2};
end

if(first_firing < 1 || first_firing > last_firing)
    fprintf('Error: first_firing argument must be greater than 0 and less than or equal to last_firing\n');
    return;
elseif(last_firing < first_firing || last_firing > num_firings)
    fprintf('Error: last_firing argument must be greater than or equal to first_firing and less than or equal to num_firings\n');
    return;
end

parfor firing_id = first_firing:last_firing
  fprintf('Working on firing # %d\n', firing_id);
  generate_scatters(firing_id);
end

