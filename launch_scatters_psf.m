function launch_scatters_psf(core)

total_cores = 1;
if core > total_cores
    display('Exceed total cores.')
    return;
end
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params');

num_firings = sys_params.num_angles;

firings_core = num_firings / total_cores;

firing_id_start = (core - 1) * firings_core + 1;
firing_id_end = (core) * firings_core;

for firing_id = firing_id_start : firing_id_end
    generate_scatters(firing_id);
    firing_id
end

if core == total_cores
%     rest_firing_ids = num_firings - total_cores * firings_core;
    
    for firing_id = (total_cores * firings_core + 1) : num_firings
        generate_scatters(firing_id);
        firing_id
    end
end
    