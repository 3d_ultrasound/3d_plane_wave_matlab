clear;
Q = 1024;
P = 2198;
K_int = 5;
r = 3;
btw = 12;
L_base = 10;
fs = 40e6;
Ts = 1/fs;

b1 = 3;
b2 = 1;

% b1 = 2 + 3/btw;
% b2 = 2 + 1/btw;


LL = 0.1:0.1:140;
L_base = 10;
m = 1:100;
for idx = 1:length(LL)
    K = LL(idx)*L_base;
    n = 2.^(ceil(log2(K+(P+K)./m-1)));
    cmplx = (m).*(b1*n.*log2(n) + b2*n);
    [min_cmplx(idx) pc_m(idx)] = min(cmplx);
end
plot(LL, min_cmplx);

