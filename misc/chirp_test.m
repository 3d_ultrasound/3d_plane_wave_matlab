fc = 4e6;
fs = 30*fc;
Ts = 1/fs;
deci_rate = 6;
c = 1540;
lamda = c/fc;

B_n6db = 4e6;
B = 2e6;
sgm_sq = 3/5/((pi*B_n6db)^2*log10(exp(1)));
T_sig = 2*ceil(sqrt(6*sgm_sq/log10(exp(1)))*fc)/fc;
t = 0:Ts:T_sig-Ts;
h_g = 1e3*exp(-(t-T_sig/2).^2/(2*sgm_sq));

T_chirp = 8*T_sig;
t = [0:Ts:T_chirp].';
half_freq_offset = 1e6;
chirp_sig_1 = cos(2*pi*((fc-B/2-half_freq_offset)*t + 1/2 * (B/T_chirp) * t.^2)+pi/2);
tx_w = tukeywin(length(chirp_sig_1), 0.8);
tx_signal_1 = chirp_sig_1.*tx_w;

chirp_sig_2 = cos(2*pi*((fc-B/2+half_freq_offset)*t + 1/2 * (B/T_chirp) * t.^2)+pi/2);
tx_signal_2 = chirp_sig_2(end:-1:1).*tx_w;


R_x = conv(tx_signal_1, tx_signal_2(end:-1:1));
R_s1 = conv(tx_signal_1, tx_signal_1(end:-1:1));
R_s2 = conv(tx_signal_2, tx_signal_2(end:-1:1));

plot(R_x, 'r'); hold on;
plot(R_s1, 'b'); hold on;
plot(R_s2, 'g'); hold off;

