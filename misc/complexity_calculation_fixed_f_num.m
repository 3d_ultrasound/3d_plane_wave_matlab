f_num = 1.4;
c = 1540;
d_range = [0.008, 0.05];
f_A2D = 40e6;
fc = 4e6;
lambda = c/fc;
range_resolution = lambda/(2*f_A2D/fc);


depth = (d_range(1):range_resolution:d_range(2))';
max_N_x = 64;
max_N_y = 64;
M_y = 128;
M_x = 128;
pitch = lambda;
N_x_set = round(depth/(f_num*lambda)/2)*2;
N_x_set(N_x_set > max_N_x) = max_N_x;
N_y_set = N_x_set;

das_count_nonsep = 0;
for scanline_y = 1:M_y
  tranducer_y_range_1 = max(1,  scanline_y - N_y_set /2);
  tranducer_y_range_2 = min(max_N_y,  scanline_y-1 + N_y_set /2);
  actual_y_size = tranducer_y_range_2 - tranducer_y_range_1 + 1;
  for scanline_x = 1:M_x
    tranducer_x_range_1 = max(1,  scanline_x - N_x_set /2);
    tranducer_x_range_2 = min(N_x_set,  scanline_x-1 + N_x_set /2);
    actual_x_size = tranducer_x_range_2 - tranducer_x_range_1 + 1;
    das_count_nonsep = das_count_nonsep + actual_y_size'*actual_x_size;
  end
end

das_count_sep = 0;
for scanline_x = 1:M_x
  tranducer_x_range_1 = max(1,  scanline_x - N_x_set /2);
  tranducer_x_range_2 = min(N_x_set,  scanline_x-1 + N_x_set /2);
  actual_x_size = tranducer_x_range_2 - tranducer_x_range_1 + 1;
  das_count_sep = das_count_sep + sum(actual_x_size)*max_N_y;
end



for scanline_y = 1:M_y
  tranducer_y_range_1 = max(1,  scanline_y - N_y_set /2);
  tranducer_y_range_2 = min(max_N_y,  scanline_y-1 + N_y_set /2);
  actual_y_size = tranducer_y_range_2 - tranducer_y_range_1 + 1;
  das_count_sep = das_count_sep + sum(actual_y_size)*M_x;
end

das_count_nonsep
das_count_sep
das_count_nonsep / das_count_sep


