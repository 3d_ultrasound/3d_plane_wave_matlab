f_num = 2;
lambda = 1540/4e6;
d_range = [0.008, 0.05];
depth = linspace(d_range(1), d_range(2), 100);
max_N_x = 32;
M_y = 32;
pitch = lambda;
N_x_set = depth/(f_num*lambda);
N_x_set(N_x_set > max_N_x) = max_N_x;

rdct_set = N_x_set.*M_y./(N_x_set + M_y);
avg_rdct = sum(N_x_set.*M_y)/sum(N_x_set + M_y)

