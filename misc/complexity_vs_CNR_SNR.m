close all;

rdct_factor = 11;
base_frame_rate = 9000;
node_set = [1, 5, 9, 13];
CNR = [ 1.6233
    1.9200
    2.1867
    2.3867];
SNR = [7.44, 3.48, 2.99;
       14.21, 10.76, 8.96;
       16.60, 12.92, 12.01;
       18.18, 14.90, 12.65];
base_SNR = [7.64, 3.68, 3.32];
SNR = mean(SNR, 2);
SNR = SNR - mean(base_SNR);

figure(1);
[ax, h1, h2] = plotyy(node_set, CNR, node_set, SNR);
set(h1, 'Marker', 'o', 'linewidth', 1.5, 'markersize', 8, 'color', 'b');
set(h2, 'Marker', '^', 'linewidth', 1.5, 'markersize', 8, 'color', 'r');
axis(ax(1), [0, 15, 1.5, 2.5]);
axis(ax(2), [0, 15, -1, 12]);
set(ax(1), 'XTick', node_set);
set(ax(1), 'YColor', 'b');
set(ax(2), 'XTick', [], 'XTickLabel', {});
set(ax(2), 'YTick', [0, 5, 10]);
set(ax(2), 'YColor', 'r');
xlabel(ax(1), 'Number of firing angles');
ylabel(ax(1), 'CNR');
ylabel(ax(2), 'SNR gain [dB]');
lg_str = {'CNR', 'SNR'};
hlg = legend(lg_str, 'location', 'Northwest');
% set(hlg, 'position', 'Northeast');

font_expand_factor = 1.2;
set(gcf, 'Color',[1 1 1]);
axis_font_size = font_expand_factor * get(ax(1), 'FontSize');
set(ax(1), 'FontSize', axis_font_size);
label_font_size = font_expand_factor * get(get(ax(1), 'XLabel'), 'FontSize');
set(get(ax(1), 'XLabel'), 'FontSize', label_font_size);
set(get(ax(1), 'YLabel'), 'FontSize', label_font_size);
set(get(ax(1), 'ZLabel'), 'FontSize', label_font_size);

set(ax(2), 'FontSize', axis_font_size);
set(get(ax(2), 'XLabel'), 'FontSize', label_font_size);
set(get(ax(2), 'YLabel'), 'FontSize', label_font_size);
set(get(ax(2), 'ZLabel'), 'FontSize', label_font_size);

export_fig('./figures/angles_vs_benefits', '-png', '-pdf');

figure(2);
[ax, h1, h2] = plotyy(node_set, base_frame_rate./node_set, node_set, node_set/rdct_factor);
set(h1, 'Marker', 's', 'linewidth', 1.5, 'markersize', 8, 'color', [0.478, .063, .894]);
set(h2, 'Marker', 'd', 'linewidth', 1.5, 'markersize', 8, 'color', [.169, .506, .337]);
axis(ax(2), [0, 15, 0, 1.5]);
axis(ax(1), [0, 15, 0, 10000]);
set(ax(2), 'XTick', node_set);
set(ax(2), 'YTick', [0, 0.5, 1]);
set(ax(2), 'YColor', [.169, .506, .337]);
set(ax(1), 'XTick', [], 'XTickLabel', {});
set(ax(1), 'YTick', [0, 2000, 4000, 6000, 8000, 10000],...
  'YTickLabel', {'0', '2k', '4k', '6k', '8k', '10k'});
set(ax(1), 'YColor', [0.478, .063, .894]);
xlabel(ax(2), 'Number of firing angles');
ylabel(ax(2), 'Complexity compared to baseline system');
ylabel(ax(1), 'Volume acquisition rate');
lg_str = {'Volume rate', 'Complexity'};
hlg = legend(lg_str, 'location', 'Northeast');
% set(hlg, 'position', 'Northeast');

font_expand_factor = 1.2;
set(gcf, 'Color',[1 1 1]);
axis_font_size = font_expand_factor * get(ax(1), 'FontSize');
set(ax(1), 'FontSize', axis_font_size);
label_font_size = font_expand_factor * get(get(ax(1), 'XLabel'), 'FontSize');
set(get(ax(1), 'XLabel'), 'FontSize', label_font_size);
set(get(ax(1), 'YLabel'), 'FontSize', label_font_size);
set(get(ax(1), 'ZLabel'), 'FontSize', label_font_size);

set(ax(2), 'FontSize', axis_font_size);
set(get(ax(2), 'XLabel'), 'FontSize', label_font_size);
set(get(ax(2), 'YLabel'), 'FontSize', label_font_size);
set(get(ax(2), 'ZLabel'), 'FontSize', label_font_size);

export_fig('./figures/angles_vs_costs', '-png', '-pdf');
