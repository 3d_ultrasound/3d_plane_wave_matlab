M_z = (0.05-0.008)*2/1540*40e6;
num_sections = 3;
coef_per_sl = 4*num_sections + 1;
M_x = 32;
M_y = 32;
N_x = 32;
N_y = 32;

num_coef_tau_1 =  coef_per_sl * N_x * N_y *M_x 
num_coef_tau_2 =  coef_per_sl * N_y * M_y *M_x 

num_coef_tau = num_coef_tau_1 + num_coef_tau_2

MB = num_coef_tau * 1.5/1024^2