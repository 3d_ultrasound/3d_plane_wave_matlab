close all;
err_folder = 'c:\Ming_data\v7_err_map';
err_file_1 = [err_folder, '\err_diff_subsize_30x30_firing_1.mat'];
err_file_2 = [err_folder, '\err_diff_subsize_20x20_firing_1.mat'];
err_file_3 = [err_folder, '\err_diff_subsize_10x10_firing_1.mat'];

load_params = load(sprintf('%s/global_params.mat', err_folder), 'phys_params', 'img_params', 'sep_params', 'sys_params');
phys_params = load_params.phys_params;
img_params = load_params.img_params;
sep_params = load_params.sep_params;
sys_params = load_params.sys_params;

sep_focus_z = sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2);
[sub_coord_x, sub_coord_y] = get_sub_recieve(img_params, sys_params);

ap_size = phys_params.lambda * [10, 20, 30];

plot_interval = 40;
depth = sep_focus_z*1000;

figure(1);
load(err_file_1, 'err_scanline', 'err_scanline_baseline');
rms_line_our = sqrt(sum(sum(err_scanline,3),2)/(sys_params.rx_size_x*sys_params.rx_size_y));
rms_line_baseline = sqrt(sum(sum(err_scanline_baseline,3),2)/(sys_params.rx_size_x*sys_params.rx_size_y));
% rms_line_our = sqrt(err_scanline(:, 16, 16));
% rms_line_baseline = sqrt(err_scanline_baseline(:, 16, 16));
plot(depth(1:plot_interval:end) , rms_line_baseline(1:plot_interval:end)*360*phys_params.Ts*phys_params.interp_factor*phys_params.fc, '--+', 'linewidth', 1.5, 'color', 'blue','markersize', 10);
hold on;
plot(depth(1:plot_interval:end) , rms_line_our(1:plot_interval:end)*360*phys_params.Ts*phys_params.interp_factor*phys_params.fc, '-o', 'linewidth', 1.5, 'color', 'red','markersize', 10);

load(err_file_2, 'err_scanline', 'err_scanline_baseline');
rms_line_our = sqrt(sum(sum(err_scanline,3),2)/(sys_params.rx_size_x*sys_params.rx_size_y));
rms_line_baseline = sqrt(sum(sum(err_scanline_baseline,3),2)/(sys_params.rx_size_x*sys_params.rx_size_y));
% rms_line_our = sqrt(err_scanline(:, 16, 16));
% rms_line_baseline = sqrt(err_scanline_baseline(:, 16, 16));
plot(depth(1:plot_interval:end) , rms_line_baseline(1:plot_interval:end)*360*phys_params.Ts*phys_params.interp_factor*phys_params.fc, '--*', 'linewidth', 1.5, 'color', 'blue','markersize', 10);
plot(depth(1:plot_interval:end) , rms_line_our(1:plot_interval:end)*360*phys_params.Ts*phys_params.interp_factor*phys_params.fc, '-s', 'linewidth', 1.5, 'color', 'red','markersize', 10);

load(err_file_3, 'err_scanline', 'err_scanline_baseline');
rms_line_our = sqrt(sum(sum(err_scanline,3),2)/(sys_params.rx_size_x*sys_params.rx_size_y));
rms_line_baseline = sqrt(sum(sum(err_scanline_baseline,3),2)/(sys_params.rx_size_x*sys_params.rx_size_y));
% rms_line_our = sqrt(err_scanline(:, 16, 16));
% rms_line_baseline = sqrt(err_scanline_baseline(:, 16, 16));
plot(depth(1:plot_interval:end) , rms_line_baseline(1:plot_interval:end)*360*phys_params.Ts*phys_params.interp_factor*phys_params.fc, '--x', 'linewidth', 1.5, 'color', 'blue','markersize', 10);
plot(depth(1:plot_interval:end) , rms_line_our(1:plot_interval:end)*360*phys_params.Ts*phys_params.interp_factor*phys_params.fc, '-d', 'linewidth', 1.5, 'color', 'red','markersize', 10);

hold off;
grid on;

lg_str = {'Ref.   30x30', 'Prop. 30x30', 'Ref.   20x20', 'Prop. 20x20', 'Ref.   10x10', 'Prop. 10x10'};
legend(lg_str, 'location', 'northeast');
axis([0 50 0 40]);
xlabel('Depth [mm]', 'fontsize', 12);
ylabel('RMS Phase Error [Degrees]', 'fontsize', 12);
fig_auto_adjustment;
export_fig('./figures/rms_err_vs_depth_plane_wave', '-png', '-pdf');

% 
% figure(2);
% % avg_start_point = floor(0.01*2/phys_params.c*phys_params.fs/phys_params.interp_factor);
% avg_start_point = 1;
% err_mean_angle = sqrt(squeeze(sum(err_mean_vol(avg_start_point:end, :, :), 1))/(size(err_mean_vol, 1)-avg_start_point));
% 
% surf(theta_net*180/pi, phi_net*180/pi, err_mean_angle*360*phys_params.Ts*phys_params.interp_factor*phys_params.fc);
% axis([-23 23 -23 23 0 20]);
% caxis([0 20]);
% view([-45 30]);
% xlabel('\theta [Degree]', 'fontsize', 12);
% ylabel('\phi [Degree]', 'fontsize', 12);
% zlabel('RMS Phase Error [Degree]', 'fontsize', 12);
% fig_auto_adjustment;
% export_fig('./figures/rms_err_vs_angle_iter', '-png', '-pdf');


% err_folder = 'c:\Ming_data\separable_delay_err_rec_v2';
% load_struct = load([err_folder '\parameter_file.mat'], 'param');
% param = load_struct.param;
% 
% sep_focus_x = param.sep_focus_x;
% sep_focus_y = param.sep_focus_y;
% sep_focus_z = param.sep_focus_z;
% sep_r_grid = param.sep_r_grid;
% r_depth = sep_r_grid(:, 1, 1);
% 
% load(sprintf('%s/err_mean_vol.mat', err_folder), 'r_depth', 'err_mean_line', 'err_mean_vol');
% figure(1);
% plot(r_depth*100, err_mean_line*360*param.Ts*param.interp_factor*param.fc, 'r');
% hold on;
% figure(3);
% err_mean_angle = sqrt(squeeze(sum(err_mean_vol(avg_start_point:end, :, :), 1))/(size(err_mean_vol, 1)-avg_start_point));
% 
% surf(theta_net*180/pi, phi_net*180/pi, err_mean_angle*360*param.Ts*param.interp_factor*param.fc);
% view([-43 42]);
% xlabel('\theta');
% ylabel('\phi');
% 
% 
% err_folder = 'c:\Ming_data\separable_delay_err_rec_v3';
% load_struct = load([err_folder '\parameter_file.mat'], 'param');
% param = load_struct.param;
% 
% sep_focus_x = param.sep_focus_x;
% sep_focus_y = param.sep_focus_y;
% sep_focus_z = param.sep_focus_z;
% sep_r_grid = param.sep_r_grid;
% r_depth = sep_r_grid(:, 1, 1);
% 
% load(sprintf('%s/err_mean_vol.mat', err_folder), 'r_depth', 'err_mean_line', 'err_mean_vol');
% figure(4);
% err_mean_angle = sqrt(squeeze(sum(err_mean_vol(avg_start_point:end, :, :), 1))/(size(err_mean_vol, 1)-avg_start_point));
% 
% surf(theta_net*180/pi, phi_net*180/pi, err_mean_angle*360*param.Ts*param.interp_factor*param.fc);
% view([-43 42]);
% xlabel('\theta');
% ylabel('\phi');
% 
% figure(1);
% % plot(r_depth*100, err_mean_line*360*param.Ts*param.interp_factor*param.fc, 'g');
% % hold off;
% % 
% xlabel('Depth [cm]');
% ylabel('RMS Phase Error [Degree]');
% grid on;
% lg = {'Sphere','Two Cones', 'Two Planes'};
% legend(lg, 'location', 'Northeast');
