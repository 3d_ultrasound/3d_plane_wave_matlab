file_name = 'C:\Ming_data\v7_elst_data_bak\lag_est_mat_8.mat';
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params', 'elst_params');
[focus_z, focus_x, focus_y] = get_focus(img_params, sys_params);
load(file_name, 'lag_est_mat');
y_sel = 32;
% h = surf(lag_est_mat(:, :, y_sel));
resolution = img_params.range_resolution;
h = pcolor(focus_x(:, :, y_sel)*1000, focus_z(:, :, y_sel)*1000, lag_est_mat(:, :, y_sel)*resolution*1000);
xlabel('x [mm]');
ylabel('z [mm]');
set(h, 'edgecolor', 'none');
axis([-5.5 5.5 12 24]);
axis ij;
axis square;
% caxis([-10 10]);
colorbar;
fig_auto_adjustment();


