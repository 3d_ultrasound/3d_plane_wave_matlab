%% Load Data
load('C:\Ming_data\v7_elst_data\est_out.mat','est_out');
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params', 'elst_params');
[focus_z, focus_x, focus_y] = get_focus(img_params, sys_params);

%% Post Processing
freq_sel_set =1:4; % DC is removed in this new version
rho = 1079;
est_combined = rho*3*sum(abs(est_out(:, :, :, freq_sel_set)), 4)/length(freq_sel_set);
est_filtered = med_filter_3d(est_combined, [3, 3, 3]);
est_filtered = gaussian_sep_filter_3d(est_filtered, [1, 0.7, 0.7], 0.1);

%% Plot Result
y_sel = 31;
clf;
h = pcolor(focus_x(2:end-1, 2:end-1, y_sel)*1000, focus_z(2:end-1, 2:end-1, y_sel)*1000, abs(squeeze(est_filtered(:, :, y_sel))));
set(h, 'edgecolor', 'none');
axis ij;
axis square;
axis([-5.5 5.5 12 24]);
caxis([0 10e3]);
colorbar;
xlabel('x [mm]');
ylabel('z [mm]');
fig_auto_adjustment();
% export_fig('./figures/elst_bf_baseline', '-png');
