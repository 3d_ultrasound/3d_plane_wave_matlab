load('C:\Ming_data\v7_elst_data\est_comsol_out.mat','est_out');
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params', 'elst_params');
% [focus_z, focus_x, focus_y] = get_focus(img_params, sys_params);
freq_sel_set = 1; % DC is removed in this new version
y_sel = 31;
rho = 1079;
est_combined = rho*3*sum(abs(est_out(:, :, :, freq_sel_set)), 4)/length(freq_sel_set);
load(sprintf('%s/coord_grid.mat', paths.comsol_data), 'grid');

focus_x = shiftdim(grid.grid_x, 2);
focus_y = shiftdim(grid.grid_y, 2);
focus_z = shiftdim(grid.grid_z, 2);

clf;
h = pcolor(1000*squeeze(focus_x(2:end-1, 2:end-1, y_sel)), 1000*squeeze(focus_z(2:end-1, 2:end-1, y_sel)), real(squeeze(est_combined(:, :, y_sel))));
set(h, 'edgecolor', 'none');
axis ij;
axis([-5.5 5.5 12 24]);
axis square;
caxis([0 20e3]);
colorbar;
xlabel('x [mm]');
ylabel('z [mm]');
fig_auto_adjustment();
% export_fig('./figures/comsol_new_est', '-png');
