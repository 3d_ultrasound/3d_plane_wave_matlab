function fig_auto_adjustment(varargin)
if isempty(varargin)
  font_expand_factor = 1.5;
else
  font_expand_factor = varargin{1};
end
set(gcf, 'Color',[1 1 1]);
axis_font_size = font_expand_factor * get(gca, 'FontSize');
set(gca, 'FontSize', axis_font_size);
label_font_size = font_expand_factor * get(get(gca, 'XLabel'), 'FontSize');
set(get(gca, 'XLabel'), 'FontSize', label_font_size);
set(get(gca, 'YLabel'), 'FontSize', label_font_size);
set(get(gca, 'ZLabel'), 'FontSize', label_font_size);
