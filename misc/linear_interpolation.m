[s1, s2, s3]=size(total_frame);
N1 = 2;
N2 = 6;
N3 = 6;
d1 = 1:s1;
d2 = 1:s2;
d3 = 1:s3;

i1 = 1:1/N1:s1;
i2 = 1:1/N2:s2;
i3 = 1:1/N3:s3;
total_frame_1 = zeros((s1-1)*N1+1, s2, s3);
for jj = 1:s2
    for kk = 1:s3
total_frame_1(:,jj,kk) = interp1(d1, total_frame(:,jj,kk), i1);
    end
end

total_frame_2 = zeros((s1-1)*N1+1, (s2-1)*N2+1, s3);
for ii = 1:(s1-1)*N1+1
    for kk = 1:s3
total_frame_2(ii,:,kk) = interp1(d2, total_frame_1(ii,:,kk), i2);
    end
end

total_frame_3 = zeros((s1-1)*N1+1, (s2-1)*N2+1, (s3-1)*N3+1);
for ii = 1:(s1-1)*N1+1
    for jj = 1:(s2-1)*N2+1
total_frame_3(ii,jj,:) = interp1(d3, squeeze(total_frame_2(ii,jj,:)), i3);
    end
end

[S1, S2, S3] = size(total_frame_3);
x_index = linspace(min(sct_params.focus_x(:)), max(sct_params.focus_x(:)), s2);
X_index = linspace(min(sct_params.focus_x(:)), max(sct_params.focus_x(:)), S2);
y_index = x_index; % x and y index are the same in this case
Y_index = X_index;
z_index = linspace(img_params.z_range(1), img_params.z_range(2), s1);
Z_index = linspace(img_params.z_range(1), img_params.z_range(2), S1);

cyst_pos = floor((cyst_params.cyst_zc - img_params.z_range(1))/(img_params.z_range(2)-img_params.z_range(1))*s1);
cyst_POS = floor((cyst_params.cyst_zc - img_params.z_range(1))/(img_params.z_range(2)-img_params.z_range(1))*S1);
imagesc(y_index, x_index, squeeze(total_frame(cyst_pos(2), :, :)))
colormap gray
figure;
imagesc(Y_index, X_index, squeeze(total_frame_3(cyst_POS(2), :, :)))
colormap gray

figure;
imagesc(x_index, z_index, squeeze(total_frame(:,floor(s2/2), :)))
colormap gray
axis equal
figure;
% [S1, S2, S3] = size(total_frame_3);
imagesc(Z_index, X_index, squeeze(total_frame_3(:, floor(S2/2), :)))
colormap gray
axis equal

figure;
imagesc(z_index, y_index, squeeze(total_frame(:,:,floor(s3/2))))
colormap gray
axis equal
figure;
% [S1, S2, S3] = size(total_frame_3);
imagesc(Z_index, Y_index, squeeze(total_frame_3(:,:, floor(S3/2))))
colormap gray
axis equal