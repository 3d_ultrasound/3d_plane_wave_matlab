function  psf_pw_plot(contrast, filename, point_depth_set, noise_level, point_sel)
%  rd_pattern_set_plot_plane_view(contrast, filename, point_depth_set, f_number, point_sel)
load_struct = load([filename '/img_complete.mat']);
total_frame = load_struct.total_frame;
load(sprintf('%s/global_params.mat', filename), 'img_params', 'sys_params');

num_psf = length(point_depth_set);
mid_depth = (point_depth_set(1:end-1) + point_depth_set(2:end))/2;
z_range = img_params.z_range;
range_pt = floor(([z_range(1), mid_depth, z_range(2)]-z_range(1))/img_params.range_resolution)+1;

[focus_z, focus_x, focus_y] = get_focus(img_params, sys_params);
x_range = [min(focus_x(:)), max(focus_x(:))]*1000;
y_range = [min(focus_y(:)), max(focus_y(:))]*1000;
min_x_range = min(abs(x_range));
min_y_range = min(abs(y_range));
x_grid = squeeze(focus_x(1,:, :))*1000;
y_grid = squeeze(focus_y(1,:, :))*1000;


for psf_idx = point_sel
  temp_image = abs(total_frame(range_pt(psf_idx):range_pt(psf_idx+1), :, :));
  max_value = max(temp_image(:));
  SNR(psf_idx) = 20*log10(max_value) - noise_level;
  disp(['PSF #', num2str(psf_idx), ' SNR = ', num2str(SNR(psf_idx))]);
  temp_image = temp_image/max_value;
  log_image = 20*log10(temp_image);
  log_temp = max(log_image);
  log_temp = squeeze(log_temp);
  
  fine_x = linspace(x_range(1),x_range(2),200);
  fine_y = linspace(y_range(1),y_range(2),200);
  [fine_x_grid fine_y_grid] = meshgrid(fine_x, fine_y);
  
  fine_log_map = interp2(x_grid.', y_grid.', log_temp.', fine_x_grid, fine_y_grid,'spline');
  fine_log_map = fine_log_map - max(fine_log_map(:));
  fine_log_map(fine_log_map < -contrast) = -contrast;
  
  figure(psf_idx);
  h = pcolor(fine_x_grid, fine_y_grid, fine_log_map);
  
  axis([-min_x_range, min_x_range, -min_y_range, min_y_range]);
  axis square;
  set(h, 'edgecolor', 'none');
  set(gca, 'YTick', -5:2.5:5, 'XTick', -5:2.5:5);
  set(gca, 'color', 'black');
  colormap gray;
  set(gca, 'fontsize', 11);
  
%   title(['f-number = ', num2str(f_number(psf_idx), '%2.1f')], 'fontsize', 14);
  ylabel('y [mm]', 'fontsize', 14);
  xlabel('x [mm]', 'fontsize', 14);
end
SNR
mean_SNR = mean(SNR)