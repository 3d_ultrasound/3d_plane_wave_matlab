close all;
point_depth_set = [13 23 33]/1000;
f_number = [];
% point_sel = [1 2 3];
point_sel = [1];
% filename = 'D:\Richard\ultrasound\Matlab\v8_v3\data\5_nonsep';
% filename = 'D:\Richard\ultrasound\Matlab\v8_v3\data\5_nonsep_compressed';
% filename = 'D:\Richard\ultrasound\Matlab\v8_v3\data\9_nonsep';
% filename = 'D:\Richard\ultrasound\Matlab\v8_v3\data\9_nonsep_compressed';
% filename = 'D:\Richard\ultrasound\Matlab\v8_v3\data\9_sep';
filename = 'D:\Richard\ultrasound\Matlab\v8_v3\data\9_sep_compressed';
contrast = 40; % 60dB
% noise_level = -260; % no compounding
noise_level = -260+10*log10(5); % 9 compounding
psf_pw_plot(contrast, filename, point_depth_set, noise_level, point_sel)

for psf_idx = point_sel
  figure(psf_idx);
%   fig_auto_adjustment(2);
  set(gca, 'Color', 'none');
  export_fig([filename, num2str(psf_idx)], '-png', '-pdf', '-r200', '-transparent');
%   export_fig(['D:\Richard\ultrasound\Matlab\v8_v2\data\5_nonsep_compressed', num2str(psf_idx)], '-png', '-pdf', '-r200', '-transparent');
%   export_fig(['D:\Richard\ultrasound\Matlab\v8_v2\data\9_nonsep', num2str(psf_idx)], '-png', '-pdf', '-r200', '-transparent');
  % export_fig(['D:\Richard\ultrasound\Matlab\v8_v2\data\9_nonsep_compressed', num2str(psf_idx)], '-png', '-pdf', '-r200', '-transparent');
end

