function x_peak = quadratic_peak_est(y_vec, X_mat)
% function x_peak = quadratic_peak_est(y_vec, X_mat)
% Input:
%    y_vec: N-by-1 vector, indicates N observations at N differnt points.
%    X_mat: N-by-3 matrix, indicates the coordinates of N observation points
%           in terms of [x1, y1, z1; x2, y2, z2; ...].
% Output:
%    x_peak: 3-by-1 vector indicates the estimated peak coordinates in terms of 
%            [x_p, y_p, z_p].'.
% Author:    Ming Yang
% Institute: Arizona State University
% Date:      Feb. 28, 2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DBG_ENABLE = false;

if length(y_vec) ~= size(X_mat, 1)
  error('Data length does not match!');
end
num_sample = length(y_vec);
num_dim = size(X_mat, 2)+1;
right_mat_acc = zeros(num_dim, num_dim);
left_mat_acc = zeros(num_dim*num_dim, num_dim*num_dim);
for sample_idx = 1:num_sample
  x_vec = [X_mat(sample_idx, :).'; 1];
  x_cross = x_vec*x_vec';
  left_mat_acc = left_mat_acc + kron(x_cross, x_cross);
  right_mat_acc = right_mat_acc + y_vec(sample_idx)*x_cross;
  if DBG_ENABLE
    if sample_idx == 1
      non_zeros_count = 0;
    end
    for dim_idx = 1:num_dim
      non_zeros_count = non_zeros_count + sum(x_cross(1:dim_idx, dim_idx)~=0);
    end
  end
end
upper_tri_idx = [];
lower_tri_idx = [];
diag_idx = [];
for m = 1:num_dim
   diag_idx = [diag_idx; (m-1)*num_dim+m];
   upper_tri_idx = [upper_tri_idx; ((m-1)*num_dim+(1:m-1)).'];
   lower_tri_idx = [lower_tri_idx; (m+(0:m-2)*num_dim).'];
end
left_mat_upper = 2*left_mat_acc([diag_idx; upper_tri_idx], [diag_idx; upper_tri_idx]);
left_mat_upper(:, 1:num_dim) = 0.5*left_mat_upper(:, 1:num_dim);
right_mat_upper = right_mat_acc([diag_idx; upper_tri_idx]);

R_vech = left_mat_upper\right_mat_upper;
R_mat = zeros(num_dim,num_dim);
R_mat([diag_idx; upper_tri_idx]) = R_vech;
R_mat([diag_idx; lower_tri_idx]) = R_vech;
R_0 = R_mat(1:num_dim-1, 1:num_dim-1);
R_b = R_mat(1:num_dim-1, num_dim);

threshold = 0;
if DBG_ENABLE
  R_0
  cR0 = cond(R_0)
end
score = diag(R_0'*R_0);
bad_tf_flag = score/sum(score) < threshold;
R_0_shrink = R_0(~bad_tf_flag, ~bad_tf_flag);
R_b_shrink = R_b(~bad_tf_flag);
x_peak_shrink = -R_0_shrink\R_b_shrink;
x_peak = zeros(num_dim-1, 1);
x_peak(~bad_tf_flag) = x_peak_shrink;
