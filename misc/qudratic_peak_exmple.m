x = [-1:1:1];
y = [-1:1:1];
z = [-1:1:1];
[X,Y,Z] = ndgrid(x, y, z);
X_mat = [X(:), Y(:), Z(:)];
x_vec_0 = [0.2, 0.2, 0.2].';
num_samples = size(X_mat, 1);
y_vec = zeros(num_samples, 1);
% R = [3 0 0; 0 0.3 0; 0 0 1];
% rand_mat = randn(3,3);
% R = rand_mat'*rand_mat
% cR = cond(R)
for sample_idx = 1:num_samples
  x_vec_tic = X_mat(sample_idx, :).' - x_vec_0;
  y_vec(sample_idx) = sinc(sqrt(x_vec_tic'*x_vec_tic)/4);
%   y_vec(sample_idx) = (x_vec_tic'*R*x_vec_tic) + 0.3*randn(1);
%   y_vec(sample_idx) = (x_vec_tic'*x_vec_tic)/4;
end
x_0_est = quadratic_peak_est(y_vec, X_mat)

