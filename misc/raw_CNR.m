paths = set_paths();
addpath(genpath(paths.param_friends));
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params', 'cyst_params');
load(sprintf('%s/img_complete.mat', paths.image), 'total_frame');
[focus_z, focus_x, focus_y] = get_focus(img_params, sys_params);

dynamic_range = 70; % in dB
log_img = 20*log10(abs(total_frame));
log_img = log_img - max(log_img(:));
log_img(log_img < -dynamic_range) = -dynamic_range;

inside_ratio = 0.6;
outside_ratio = [1.1, 1.3];
pos_adj = [0 0 0];
cyst_r = cyst_params.cyst_r;

for cyst_idx = 1:cyst_params.cyst_num
  target_r = cyst_r(cyst_idx) * inside_ratio;
  back_r = cyst_r(cyst_idx)*outside_ratio;
  
  dist_mat = sqrt((focus_x - cyst_params.cyst_xc(cyst_idx) - pos_adj(1)).^2 + ...
    (focus_y - cyst_params.cyst_yc(cyst_idx) - pos_adj(2)).^2 + ...
    (focus_z - cyst_params.cyst_zc(cyst_idx) - pos_adj(3)).^2);
  target_flag = dist_mat < target_r;
  back_flag = dist_mat > back_r(1) & dist_mat < back_r(2);
  target_samples = log_img(target_flag);
  back_samples = log_img(back_flag);
  cyst_CNR(cyst_idx) = abs(mean(target_samples(:)) - mean(back_samples(:)))/sqrt(var(target_samples) + var(back_samples));
end

cyst_CNR

clear cyst_CNR;