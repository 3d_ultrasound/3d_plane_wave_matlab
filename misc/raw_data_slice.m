paths = set_paths();
addpath(genpath(paths.param_friends));
image_dir = 'C:\Ming_data\v7_image';
load([image_dir, '\img_complete.mat']);
load([paths.params, '\global_params.mat'], 'img_params', 'sys_params');
[focus_z, focus_x, focus_y] = get_focus(img_params, sys_params);

dynamic_range = 40; % in dB
log_img = 20*log10(abs(total_frame));
log_img = log_img - max(log_img(:));
log_img(log_img < -dynamic_range) = -dynamic_range;

y_pt = 16;
slice_xz = squeeze(log_img(:, :, y_pt));
mesh_x = squeeze(focus_x(:, :, y_pt))*1000;
mesh_z = squeeze(focus_z(:, :, y_pt))*1000;

figure();
h = pcolor(mesh_x, mesh_z, slice_xz);
set(h, 'edgecolor', 'none');
colormap gray;
axis equal ij;
axis([min(mesh_x(:)), max(mesh_x(:)), min(mesh_z(:)), 50]);
xlabel('x [mm]');
ylabel('z [mm]');
fig_auto_adjustment;
export_fig('./figures/slice_xz', '-png');

z_pt = 780;
slice_xy = squeeze(log_img(z_pt, :, :));
mesh_x = squeeze(focus_x(z_pt, :, :))*1000;
mesh_y = squeeze(focus_y(z_pt, :, :))*1000;

figure();
h = pcolor(mesh_x, mesh_y, slice_xy);
set(h, 'edgecolor', 'none');
colormap gray;
axis equal ij;
axis([min(mesh_x(:)), max(mesh_x(:)), min(mesh_y(:)), max(mesh_y(:))]);
xlabel('x [mm]');
ylabel('y [mm]');
fig_auto_adjustment;
% export_fig('./figures/slice_xy', '-png');


