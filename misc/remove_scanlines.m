num_cpd_frame = 36;
remove_interval = 2;

paths = set_paths();
addpath(genpath(paths.elst));

for cpd_frame_idx = 1:num_cpd_frame
  fprintf('Working on frame # %d\n', cpd_frame_idx);
  ld_str = load(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
  lag_est_mat = ld_str.lag_est_mat(:, 1:remove_interval:end, 1:remove_interval:end);
  save(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
end