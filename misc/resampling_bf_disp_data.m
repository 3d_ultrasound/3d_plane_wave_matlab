function resampling_bf_disp_data(start_firing_idx, end_firing_idx)
skip_clrl.skip_z = 10;
skip_clrl.skip_x = 2;
skip_clrl.skip_y = 2;
paths = set_paths();

for cpd_frame_idx = start_firing_idx:end_firing_idx
  ld_struct = load(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
  lag_est_mat = ld_struct.lag_est_mat(1:skip_clrl.skip_z:end, 1:skip_clrl.skip_x:end, 1:skip_clrl.skip_y:end);
  save(sprintf('%s/lag_est_mat_%d.mat', paths.elst_data, cpd_frame_idx), 'lag_est_mat');
end
