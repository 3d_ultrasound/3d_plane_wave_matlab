function resampling_comsol_data(start_firing_idx, end_firing_idx, firing_skip)
paths = set_paths();
skip_x = 2;
skip_y = 2;
skip_z = 2;

ld_struct = load(sprintf('%s/coord_grid.mat', paths.comsol_data), 'grid');
grid.grid_x = ld_struct.grid.grid_x(1:skip_x:end, 1:skip_y:end, 1:skip_z:end);
grid.grid_y = ld_struct.grid.grid_y(1:skip_x:end, 1:skip_y:end, 1:skip_z:end);
grid.grid_z = ld_struct.grid.grid_z(1:skip_x:end, 1:skip_y:end, 1:skip_z:end);
save(sprintf('%s/coord_grid.mat', paths.comsol_data), 'grid');

for fr_idx = start_firing_idx:firing_skip:end_firing_idx
  ld_struct = load(sprintf('%s/shear_wave_disp_fire_%d.mat', paths.comsol_data, fr_idx), 'disp');
  disp.disp_x = ld_struct.disp.disp_x(1:skip_x:end, 1:skip_y:end, 1:skip_z:end);
  disp.disp_y = ld_struct.disp.disp_y(1:skip_x:end, 1:skip_y:end, 1:skip_z:end);
  disp.disp_z = ld_struct.disp.disp_z(1:skip_x:end, 1:skip_y:end, 1:skip_z:end);
  save(sprintf('%s/shear_wave_disp_fire_%d.mat', paths.comsol_data, fr_idx), 'disp');
end



