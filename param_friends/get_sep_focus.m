function [sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params)
% [focus_z, focus_x, focus_y] = get_focus(img_params, sys_params)
% NOTICE: z is in the first dimension

delta_x = (sys_params.rx_width + sys_params.rx_kerf)/img_params.lateral_interp_rate;
delta_y = (sys_params.rx_height + sys_params.rx_kerf)/img_params.lateral_interp_rate;


center_adjust_x = -(sys_params.tx_kerf + sys_params.tx_width)/2;
center_adjust_y = -(sys_params.tx_kerf + sys_params.tx_height)/2;

first_x = -(sys_params.rx_size_x*img_params.lateral_interp_rate / 2 * delta_x) + center_adjust_x;
first_y = -(sys_params.rx_size_y*img_params.lateral_interp_rate / 2 * delta_y) + center_adjust_y;

x_point = first_x + (0:sys_params.rx_size_x*img_params.lateral_interp_rate-1)*delta_x;
y_point = first_y + (0:sys_params.rx_size_y*img_params.lateral_interp_rate-1)*delta_y;
z_point = sep_params.sep_z_range(1):img_params.range_resolution:sep_params.sep_z_range(2);

[sep_focus_z, sep_focus_x, sep_focus_y] = ndgrid(z_point, x_point, y_point);

