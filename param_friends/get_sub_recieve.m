function [sub_rx_x, sub_rx_y] = get_sub_recieve(img_params, sys_params)
% [sub_rx_x, sub_rx_y] = get_sub_recieve(img_params, sys_params)
% Get transducer coordinates in a subaperture, assuming the center of 
% the subaperuter is at (0, 0, 0)  

delta_x = (sys_params.rx_width + sys_params.rx_kerf)/img_params.lateral_interp_rate;
delta_y = (sys_params.rx_height + sys_params.rx_kerf)/img_params.lateral_interp_rate;


first_x = -(sys_params.sub_size_x / 2 * delta_x - delta_x/2);
first_y = -(sys_params.sub_size_y / 2 * delta_y - delta_y/2);

sub_rx_x = first_x + (0:sys_params.sub_size_x*img_params.lateral_interp_rate-1).'*delta_x;
sub_rx_y = first_y + (0:sys_params.sub_size_y*img_params.lateral_interp_rate-1).'*delta_y;
