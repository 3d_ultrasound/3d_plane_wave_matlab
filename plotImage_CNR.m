%%%% Plot the image
clear all;
plot_fenceng = 0;
if ~exist('bf_type')
    bf_type = 'sep';
end

if ~exist('compression', 'var')
    compression = true;
end

% set_params;
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params');

paths.image = sprintf('%s/allInOne/', paths.image);

%%% Load the system parameters
sub_aps_x = sys_params.sub_aps_x;
sub_aps_y = sys_params.sub_aps_y;

sub_x = sys_params.sub_size_x;
sub_y = sys_params.sub_size_y;

sub_shift = sys_params.sub_shift;
sub_shift_x = sys_params.sub_shift;
sub_shift_y = sys_params.sub_shift;

sub_aps = sub_aps_x * sub_aps_y;

pitch_x = sys_params.pitch;
pitch_y = sys_params.pitch;

f_number = sys_params.f_number;
times = sys_params.times;

clear sys_params;

%%% load the imaging parameters
r_range = img_params.r_range(1) : img_params.range_resolution : (img_params.r_range(2));
range_size = max(size(r_range));

focus_x = (- (sub_aps_x - 1) * pitch_x * sub_shift_x / 2) : (pitch_x * sub_shift_x) : ((sub_aps_x - 1) * pitch_x * sub_shift_x / 2);
focus_y = (- (sub_aps_y - 1) * pitch_y * sub_shift_y / 2) : (pitch_y * sub_shift_y) : ((sub_aps_y - 1) * pitch_y * sub_shift_y / 2);
focus_z = r_range;

% clear img_params;

%%% load the image file
if ~exist('./data/tempImage_volume2/interpted_CNR_volume2.mat', 'file')
    load(sprintf('./data/image_volume2/allInOne/%s/%dby%dshift%dF%d/img_complete.mat', bf_type, sub_x, sub_y, sub_shift, f_number), 'total_frame');

    framePlot = total_frame;

    if compression
        interp_ratio_base2 = 3;
        interp_ratio = 2^(interp_ratio_base2);

        framePlot_compress = zeros(size(framePlot, 1), interp_ratio*size(framePlot, 3) - interp_ratio + 1, ...
            interp_ratio*size(framePlot, 3) - interp_ratio + 1);

        for depth_idx = 1 : size(framePlot, 1)
            interp_temp = interp2(squeeze(framePlot(depth_idx, :, :)), interp_ratio_base2);
            framePlot_compress(depth_idx, :, :) = interp_temp;
            display(depth_idx);
            pause(0.01);
        end

        focus_x = interp(focus_x, interp_ratio);
        focus_x = focus_x(1 : (30 * interp_ratio - interp_ratio + 1));
        focus_y = interp(focus_y, interp_ratio);
        focus_y = focus_y(1 : (30 * interp_ratio - interp_ratio + 1));

        framePlot = framePlot_compress;
    end

    save('./data/tempImage_volume2/interpted_CNR_volume2.mat', 'framePlot', '-v7.3');
else
    interp_ratio_base2 = 3;
    interp_ratio = 2^(interp_ratio_base2);
    focus_x = interp(focus_x, interp_ratio);
    focus_x = focus_x(1 : (30 * interp_ratio - interp_ratio + 1));
    focus_y = interp(focus_y, interp_ratio);
    focus_y = focus_y(1 : (30 * interp_ratio - interp_ratio + 1));
    load('./data/tempImage_volume2/interpted_CNR_volume2.mat', 'framePlot');
end

%% Compress the image to log scale
framePlot_max = max(framePlot(:));
log_env = framePlot/framePlot_max;
% log_env = 1;
log_env = 20 * log10(log_env);

%%% if the dynamic range is not given, set it to be 40
if ~exist('dynamic_range')
    dynamic_range = 40;
end
log_env = log_env + dynamic_range;
log_env( log_env < 0 ) = 0;

% % imagePlot1 = squeeze( max(log_env, [], 2));
% imagePlot1 = squeeze( log_env( :, 116, :));
% % imagePlot1 = squeeze( log_env( foor(size(log_env, 1) / (100) * 30), :, :));
% % imagePlot1(floor(0.7*size(imagePlot1, 1)) : end, :) = 0;
% % imagePlot2 = squeeze( max(log_env, [], 3));
% imagePlot2 = squeeze(log_env(:, :, 116));
% imagePlot2 = squeeze( log_env(floor(size(log_env, 1) / (100) * 30), :, :));
% % imagePlot2(floor(0.7*size(imagePlot2, 1)) : end, :) = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%% Plot the image
% figure(333);
% subplot(1, 2, 1);
% imagesc(focus_x*1000, focus_z*1000, imagePlot1);
% colormap(gray);
% % axis equal;
% xlabel('y [mm]', 'fontname', 'Times New Roman',  'color', [0 0 0]);
% ylabel('z [mm]', 'fontname', 'Times New Roman', 'color', [0 0 0]);
% title('Y-Z Projection', 'fontname', 'Times New Roman');
% axis equal;
% % xlim([-10, 10]);
% % ylim([10, 100]);
% xlim([-5, 5]);
% ylim([0.0485, 0.0585]*1000);
% % axis equal;
% % xlim([0, max(focus_x)*1000]);
% % ylim([30, 40]);
%
% subplot(1, 2, 2);
% imagesc(focus_x*1000, focus_z*1000, imagePlot2);
% colormap(gray);
% xlabel('x [mm]', 'fontname', 'Times New Roman', 'color', [0 0 0]);
% ylabel('z [mm]', 'fontname', 'Times New Roman', 'color', [0 0 0]);
% title('X-Z Projection', 'fontname', 'Times New Roman');
% axis equal;
% xlim([-5, 5]);
% ylim([0.0485, 0.0585]*1000);

% imagePlot = squeeze( max(log_env, [], 3));
% imagePlot = squeeze( (log_env(:, 23, :)));
% imagePlot = log_env;
% InterpPlot = zeros(size(imagePlot, 1), size(imagePlot, 2)*4-3, size(imagePlot, 3)*4-3);
% %%%%% Interpolation %%%%%%
% for depths = 1 : size(imagePlot, 1)
% %     frame_temp = interp2(squeeze(total_frame(depths, :, :)));
%     interp_temp = interp2(squeeze(imagePlot(depths, :, :)), 2);
% %     interp_temp = interp_temp(1 : size(interp_temp, 1)-3, 1 : size(interp_temp, 2)-3);
%     InterpPlot(depths, :, :) = interp_temp;
% end
% imagePlot = squeeze(InterpPlot(:, (1 + size(InterpPlot, 2))/2, :));
% focus_x = interp(focus_x, 4);
% focus_x = focus_x(1 : length(focus_x) - 3 );
% focus_y = interp(focus_y, 4);
% focus_y = focus_y(1 : length(focus_y) - 3 );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% focus_z = focus_z((focus_z < 0.08));
% focus_z = focus_z(focus_z > 0.06);
focus_z_pos = find(focus_z > 0.010 & focus_z < 0.030);
InterpPlot = log_env(focus_z_pos, :, :);
focus_z = focus_z(focus_z_pos);

%%% Plot the image
% hfig = figure(333);
% set(hfig, 'position', [740 159 327 811]);
% imagesc(focus_x, focus_z, imagePlot);
% xlabel('Lateral distance [m]');
% ylabel('Axial distance [m]');
% % title({sprintf('3D Imaging, F = %d, Shift = %d', f_number, sub_shift), sprintf('Dynamic Range = %d, Pitch = %d x lambda', dynamic_range, times/2)});
% title({sprintf('Dynamic Range = %d, F = %d', dynamic_range, f_number), sprintf('Beamforming Type: %s', bf_type)});
% colormap(gray);
% axis equal;
% xlim([min(focus_x), max(focus_x)]);

focus_temp_1 = repmat(focus_x, [length(focus_z), 1, length(focus_y)]);
focus_temp_2 = zeros(1, 1, length(focus_y));
focus_temp_2(1, 1, :) = focus_y;
focus_temp_2 = repmat(focus_temp_2, [length(focus_z), length(focus_x), 1]);
focus_temp_3 = repmat(focus_z', [1, length(focus_x), length(focus_y)]);
focus_temp = sqrt(focus_temp_1.^2 + focus_temp_2.^2 + focus_temp_3.^2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cyst_in_temp_1 = sqrt((focus_temp_1 - 0 *10^(-3)).^2 + focus_temp_2.^2 + ( focus_temp_3 - 1.87 * 10^(-2) ).^2);
cyst_in_mask_1 = (cyst_in_temp_1 <= 0.7 * 3.5 * 10^(-3));
% cyst_in_1 = cyst_in_mask_1 .* InterpPlot;
cyst_in_1 = InterpPlot(find(cyst_in_mask_1 == 1));
mu_cyst_in_1 = mean(cyst_in_1(:));
signam_cyst_in =  sum(sum( (cyst_in_1 - mu_cyst_in_1).^2 )) / numel(cyst_in_1);

cyst_out_mask_1_temp_1 = (cyst_in_temp_1 >= 1 * 3.5 * 10^(-3));
cyst_out_mask_1_temp_2 = (cyst_in_temp_1 <= 1.15 * 3.5 * 10^(-3));
cyst_out_mask_1 = cyst_out_mask_1_temp_1 .* cyst_out_mask_1_temp_2;
cyst_out_1 = InterpPlot(find(cyst_out_mask_1 == 1));
mu_cyst_out_1 = mean(cyst_out_1(:));
signam_cyst_out =  sum(sum( (cyst_out_1 - mu_cyst_out_1).^2 )) / numel(cyst_out_1);

CNR_1 = abs(mu_cyst_in_1 - mu_cyst_out_1) / sqrt(signam_cyst_in + signam_cyst_out);
CR_1 = ( mu_cyst_out_1 - mu_cyst_in_1 ) / ( mu_cyst_out_1 + mu_cyst_in_1 );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cyst_in_temp_2 = sqrt(focus_temp_1.^2 + focus_temp_2.^2 + ( focus_temp_3 - 4.5 * 10^(-2) ).^2);
% cyst_in_mask_2 = (cyst_in_temp_2 <= 0.8 * 2.5 * 10^(-3));
% % cyst_in_1 = cyst_in_mask_1 .* InterpPlot;
% cyst_in_2 = InterpPlot(find(cyst_in_mask_2 == 1));
% mu_cyst_in_2 = mean(mean(mean(cyst_in_2)));
% signam_cyst_in_2 =  sum(sum( (cyst_in_2 - mu_cyst_in_2).^2 )) / numel(cyst_in_2);
%
% cyst_out_mask_2_temp_1 = (cyst_in_temp_2 >= 1 * 2.5 * 10^(-3));
% cyst_out_mask_2_temp_2 = (cyst_in_temp_2 <= 1.15 * 2.5 * 10^(-3));
% cyst_out_mask_2 = cyst_out_mask_2_temp_1 .* cyst_out_mask_2_temp_2;
% cyst_out_2 = InterpPlot(find(cyst_out_mask_2 == 1));
% mu_cyst_out_2 = mean(mean(mean(cyst_out_2)));
% signam_cyst_out_2 =  sum(sum( (cyst_out_2 - mu_cyst_out_2).^2 )) / numel(cyst_out_2);
%
% CNR_2 = abs(mu_cyst_in_2 - mu_cyst_out_2) / sqrt(signam_cyst_in_2 + signam_cyst_out_2);
% CR_2 = ( mu_cyst_out_2 - mu_cyst_in_2 ) / ( mu_cyst_out_2 + mu_cyst_in_2 );
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cyst_in_temp_3 = sqrt(focus_temp_1.^2 + focus_temp_2.^2 + ( focus_temp_3 - 5.5 * 10^(-2) ).^2);
% cyst_in_mask_3 = (cyst_in_temp_3 <= 0.8 * 3.5 * 10^(-3));
% % cyst_in_1 = cyst_in_mask_1 .* InterpPlot;
% cyst_in_3 = InterpPlot(find(cyst_in_mask_3 == 1));
% mu_cyst_in_3 = mean(mean(mean(cyst_in_3)));
% signam_cyst_in_3 =  sum(sum( (cyst_in_3 - mu_cyst_in_3).^2 )) / numel(cyst_in_3);
%
% cyst_out_mask_3_temp_1 = (cyst_in_temp_3 >= 1 * 3.5 * 10^(-3));
% cyst_out_mask_3_temp_2 = (cyst_in_temp_3 <= 1.15 * 3.5 * 10^(-3));
% cyst_out_mask_3 = cyst_out_mask_3_temp_1 .* cyst_out_mask_3_temp_2;
% cyst_out_3 = InterpPlot(find(cyst_out_mask_3 == 1));
% mu_cyst_out_3 = mean(mean(mean(cyst_out_3)));
% signam_cyst_out_3 =  sum(sum( (cyst_out_3 - mu_cyst_out_3).^2 )) / numel(cyst_out_3);
%
% CNR_3 = abs(mu_cyst_in_3 - mu_cyst_out_3) / sqrt(signam_cyst_in_3 + signam_cyst_out_3);
% CR_3 = ( mu_cyst_out_3 - mu_cyst_in_3 ) / ( mu_cyst_out_3 + mu_cyst_in_3 );
%
% CNR_avrage = (CNR_1 + CNR_2 + CNR_3)/3;
% CR_average = (CR_1 + CR_2 + CR_3)/3;

% curr_date = date;
% %%% save the image
% if ~exist(sprintf('./data/tempImage/%s/%s', bf_type, curr_date))
%     mkdir(sprintf('./data/tempImage/%s/%s', bf_type, curr_date));
% end
% saveas(gcf, sprintf('./data/tempImage/%s/%s/imagePlot_F%d_Shift%d_DR%d.fig', bf_type, curr_date, f_number, sub_shift, dynamic_range));
% saveas(gcf, sprintf('./data/tempImage/%s/%s/imagePlot_F%d_Shift%d_DR%d.jpg', bf_type, curr_date, f_number, sub_shift, dynamic_range));

