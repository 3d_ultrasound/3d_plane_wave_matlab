bf_type = 'sep';

if ~exist('compression', 'var')
    interpolation = false;
end

% set_params;
paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params');

% paths.image = sprintf('%s/allInOne/', paths.image);

%%% Load the system parameters
% sub_aps_x = sys_params.sub_aps_x;
% sub_aps_y = sys_params.sub_aps_y;

% sub_x = sys_params.sub_size_x;
% sub_y = sys_params.sub_size_y;

% sub_shift = sys_params.sub_shift;
% sub_shift_x = sys_params.sub_shift;
% sub_shift_y = sys_params.sub_shift;

% sub_aps = sub_aps_x * sub_aps_y;

% pitch_x = sys_params.pitch;
% pitch_y = sys_params.pitch;

% f_number = sys_params.f_number;
% times = sys_params.times;

clear sys_params;

%%% load the imaging parameters
% r_range = img_params.r_range(1) : img_params.range_resolution : (img_params.r_range(2));
% range_size = max(size(r_range));

% focus_x = (- (sub_aps_x - 1) * pitch_x * sub_shift_x / 2) : (pitch_x * sub_shift_x) : ((sub_aps_x - 1) * pitch_x * sub_shift_x / 2);
% focus_y = (- (sub_aps_y - 1) * pitch_y * sub_shift_y / 2) : (pitch_y * sub_shift_y) : ((sub_aps_y - 1) * pitch_y * sub_shift_y / 2);
% focus_z = r_range;
load(sprintf('%s/global_params.mat', paths.params));
[sep_focus_z, sep_focus_x, sep_focus_y] = get_sep_focus(img_params, sys_params, sep_params);

focus_x = linspace(min(sep_focus_x(:)), max(sep_focus_y(:)), size(log_total_frame,2));
focus_y = linspace(min(sep_focus_y(:)), max(sep_focus_y(:)), size(log_total_frame,3));
focus_z = linspace(img_params.z_range(1),img_params.z_range(2),max(size(log_total_frame)));

% clear img_params;

%%% load the image file
% load(sprintf('%s/%s/%dby%dshift%dF%d/img_complete.mat', paths.image, bf_type, sub_x, sub_y, sub_shift, f_number), 'total_frame');
load(sprintf('%s/img_complete.mat', paths.image), 'total_frame');

framePlot = total_frame;

if interpolation
    interp_ratio_base2 = 3;
    interp_ratio = 2^(interp_ratio_base2);

    framePlot_compress = zeros(size(framePlot, 1), interp_ratio*size(framePlot, 3) - interp_ratio + 1, ...
        interp_ratio*size(framePlot, 3) - interp_ratio + 1);

    for depth_idx = 1 : size(framePlot, 1)
        interp_temp = interp2(squeeze(framePlot(depth_idx, :, :)), interp_ratio_base2);
        framePlot_compress(depth_idx, :, :) = interp_temp;
        display(depth_idx);
        pause(0.01);
    end

    focus_x = interp(focus_x, interp_ratio);
    focus_x = focus_x(1 : (30 * interp_ratio - interp_ratio + 1));
    focus_y = interp(focus_y, interp_ratio);
    focus_y = focus_y(1 : (30 * interp_ratio - interp_ratio + 1));

    framePlot = framePlot_compress;
end

%%% Compress the image to log scale
framePlot_max = max(framePlot(:));
log_env = framePlot/framePlot_max;
% log_env = 1;
log_env = 20 * log10(log_env);

%%% if the dynamic range is not given, set it to be 40
if ~exist('dynamic_range')
    dynamic_range = 40;
end
log_env = log_env + dynamic_range;
log_env( log_env < 0 ) = 0;

% imagePlot1 = squeeze( max(log_env, [], 2));
imagePlot1 = squeeze( log_env( :, 16, :));
% imagePlot1 = squeeze( log_env( foor(size(log_env, 1) / (100) * 30), :, :));
% imagePlot1(floor(0.7*size(imagePlot1, 1)) : end, :) = 0;
% imagePlot2 = squeeze( max(log_env, [], 3));
imagePlot2 = squeeze(log_env(:, :, 16));
% imagePlot2 = squeeze(log_env);
% % imagePlot2 = squeeze( log_env(floor(size(log_env, 1) / (100) * 30), :, :));
% % imagePlot2(floor(0.7*size(imagePlot2, 1)) : end, :) = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Plot the image
figure(333);
% subplot(1, 2, 1);
% imagesc(focus_x*1000, focus_z*1000, imagePlot1);
% colormap(gray);
% % axis equal;
% xlabel('y [mm]', 'fontname', 'Times New Roman',  'color', [0 0 0]);
% ylabel('z [mm]', 'fontname', 'Times New Roman', 'color', [0 0 0]);
% title('Y-Z Projection', 'fontname', 'Times New Roman');
% axis equal;
% xlim([-5, 5]);
% ylim([img_params.img_min_depth*1000, img_params.img_max_depth*1000]);
% % xlim([-5, 5]);
% % ylim([7.7, 40]);
% % xlim([-5, 5]);
% % ylim([0.0138, 0.0238]*1000);
% % axis equal;
% % xlim([0, max(focus_x)*1000]);
% % ylim([30, 40]);

% subplot(1, 2, 2);
imagesc(focus_x*1000, focus_z*1000, imagePlot2);
colormap(gray);
xlabel('x [mm]', 'fontname', 'Times New Roman', 'color', [0 0 0]);
ylabel('z [mm]', 'fontname', 'Times New Roman', 'color', [0 0 0]);
title('X-Z Projection', 'fontname', 'Times New Roman');
axis equal;
% xlim([-5, 5]);
% ylim([0.0138, 0.0238]*1000);
xlim([-5, 5]);
ylim([img_params.img_min_depth*1000, img_params.img_max_depth*1000]);
% xlim([0, max(focus_x)*1000]);
% ylim([30, 40]);


curr_date = date;
%%% save the image
% if ~exist(sprintf('./data/tempImage/allInOne/%s/%s', bf_type, curr_date))
%     mkdir(sprintf('./data/tempImage/allInOne/%s/%s', bf_type, curr_date));
% end
% saveas(gcf, sprintf('./data/tempImage/allInOne/%s/%s/imagePlot_F%d_Shift%d_DR%d.fig', bf_type, curr_date, f_number, sub_shift, dynamic_range));
% saveas(gcf, sprintf('./data/tempImage/allInOne/%s/%s/imagePlot_F%d_Shift%d_DR%d.jpg', bf_type, curr_date, f_number, sub_shift, dynamic_range));
% return;
% lateral_width = 10;



