%%%% Plot the image
% close all;
if ~exist('bf_type')
    bf_type = 'sep';
end

paths = set_paths();
load(sprintf('%s/global_params.mat', paths.params), 'sys_params', 'img_params');

% paths.image = sprintf('%s/allInOne/', paths.image);

%%% Load the system parameters
% sub_aps_x = sys_params.sub_aps_x;
% sub_aps_y = sys_params.sub_aps_y;

% sub_x = sys_params.sub_size_x;
% sub_y = sys_params.sub_size_y;
% 
% sub_shift = sys_params.sub_shift;
% sub_shift_x = sys_params.sub_shift;
% sub_shift_y = sys_params.sub_shift;
% 
% sub_aps = sub_aps_x * sub_aps_y;
% 
% pitch_x = sys_params.pitch;
% pitch_y = sys_params.pitch;

% f_number = sys_params.f_number;
% times = sys_params.times;


delta_x = (sys_params.rx_width + sys_params.rx_kerf)/img_params.lateral_interp_rate;
delta_y = (sys_params.rx_height + sys_params.rx_kerf)/img_params.lateral_interp_rate;

center_adjust_x = -(sys_params.tx_kerf + sys_params.tx_width)/2;
center_adjust_y = -(sys_params.tx_kerf + sys_params.tx_height)/2;

first_x = -(sys_params.rx_size_x*img_params.lateral_interp_rate / 2 * delta_x) + center_adjust_x;
first_y = -(sys_params.rx_size_y*img_params.lateral_interp_rate / 2 * delta_y) + center_adjust_y;

% clear sys_params;

%%% load the imaging parameters
r_range = img_params.z_range(1):img_params.range_resolution:img_params.z_range(2);
range_size = max(size(r_range));

% focus_x = first_x + (0:sys_params.rx_size_x*img_params.lateral_interp_rate-1)*delta_x;
focus_y = first_y + (0:sys_params.rx_size_y*img_params.lateral_interp_rate-1)*delta_y;
% focus_y = (- (sub_aps_y - 1) * pitch_y * sub_shift_y / 2) : (pitch_y * sub_shift_y) : ((sub_aps_y - 1) * pitch_y * sub_shift_y / 2);
focus_z = r_range;

clear img_params;
clear sys_params;

%%% load the image file
% load(sprintf('%s/%s/%dby%dshift%dF%d/img_complete.mat', paths.image, bf_type, sub_x, sub_y, sub_shift, f_number), 'total_frame');
load(sprintf('%s/img_complete.mat', paths.image), 'total_frame');

% framePlot = squeeze(total_frame(:,:, 3));
% framePlot = squeeze(max(total_frame,[], 3));
framePlot = total_frame;
% spec = fftshift(fft2(framePlot));
% Nz = size(spec,1);
% Nx = size(spec,2);
% freq_z = -20:40/Nz:20-40/Nz;
% sampling_x = 1/(1540/4e3/2);
% freq_x = -sampling_x/2 : sampling_x/Nx : sampling_x/2-sampling_x/Nx;
% imagesc(freq_x, freq_z, abs(spec))
% xlabel('fx [mm^{-1}]',  'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);
% ylabel('fz [MHz]',  'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);
%%% Compress the image to log scale
log_env = framePlot/max(max(max(framePlot)));
% log_env = 1;
log_env = 20 * log10(log_env);

%%% if the dynamic range is not given, set it to be 40
if ~exist('dynamic_range')
    dynamic_range = 40;
end
log_env = log_env + dynamic_range;
log_env( log_env < 0 ) = 0;

imagePlot_xz  = log_env;
% imagePlot_xz = squeeze( max(log_env, [], 2));
% imagePlot_yz = squeeze( max(log_env, [], 3));
% imagePlot = squeeze( (log_env(:, 23, :)));
% InterpPlot = zeros(size(imagePlot, 1), size(imagePlot, 2)*4-3);
% %%%%% Interpolation %%%%%%
% for depths = 1 : size(imagePlot, 1)
% %     frame_temp = interp2(squeeze(total_frame(depths, :, :)));
%     interp_temp = interp(imagePlot(depths, :), 4);
%     interp_temp = interp_temp(1 : length(interp_temp)-3);
%     InterpPlot(depths, :) = interp_temp;
% end
% imagePlot = InterpPlot;
% focus_x = interp(focus_x, 4);
% focus_x = focus_x(1 : length(focus_x) - 3 );
% focus_y = interp(focus_y, 4);
% focus_y = focus_y(1 : length(focus_y) - 3 );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Plot the image
hfig = figure(333);
% imagesc(focus_x*1000, focus_z*1000, imagePlot_xz);
imagesc(focus_y*1000, focus_z*1000, imagePlot_xz);
xlabel('x [mm]',  'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);
ylabel('z [mm]',  'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);
colormap(gray);
axis equal;
xlim([-0.01, 0.01]*1000);
title({'DAS';'X-Z Plane PSF'}, 'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);

% hfig = figure(444);
% imagesc(focus_x*1000, focus_z*1000, imagePlot_yz);
% xlabel('y [mm]',  'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);
% ylabel('z [mm]',  'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);
% colormap(gray);
% axis equal;
% xlim([-0.01, 0.01]*1000);
% title({'DAS';'Y-Z Plane PSF'}, 'fontname','Times New Roman', 'fontsize', 14, 'color', [0 0 0]);

curr_date = date;
%%% save the image
if ~exist(sprintf('./data/tempImage/allInOne/%s/%s', bf_type, curr_date))
    mkdir(sprintf('./data/tempImage/allInOne/%s/%s', bf_type, curr_date));
end
% 
% figure(555);
% xy_number = 5;
% % seg_start = round(([55, 65, 75, 85, 95] - 0)*1e-3 /c*2/Ts);
% % seg_end   = round(([65, 75, 85, 95, 105] - 0)*1e-3 /c*2/Ts);
% seg_start_mm = [25, 45, 65, 85, 95];
% seg_end_mm   = [35, 55, 75, 95, 105];
% seg_start = round((seg_start_mm - 0) / 110 * size(log_env, 1));
% seg_end   = round((seg_end_mm - 0)/ 110 * size(log_env, 1));
% 
% for pt_idx = 1 : xy_number
%     hold on;
%     subplot(1,xy_number, pt_idx);
%     xy_plane = squeeze(max(log_env(seg_start(pt_idx):seg_end(pt_idx), :, :),...
%         [], 1));
%     imagesc(x_axis*1000, y_axis*1000, xy_plane);
%     colormap(gray);
%     axis equal;
%     xlim([-10, 10]);
%     ylim([-10, 10]);
%     xlabel('x [mm]', 'fontname','Times New Roman','fontsize', 14, 'color', [0 0 0]);
%     ylabel('y [mm]', 'fontname','Times New Roman','fontsize', 14, 'color', [0 0 0]);
%     scatter_depth = ( seg_start_mm(pt_idx) + seg_end_mm(pt_idx) ) / 2;
%     title({'DAS X-Y Plane PSF'; sprintf('Scatter at %d mm', scatter_depth)}, 'fontname','Times New Roman','fontsize', 14, 'color', [0 0 0] );
% end
% 
% saveas(gcf, sprintf('./data/tempImage/allInOne/%s/%s/imagePlot_F%d_Shift%d_DR%d.fig', bf_type, curr_date, f_number, sub_shift, dynamic_range));
% saveas(gcf, sprintf('./data/tempImage/allInOne/%s/%s/imagePlot_F%d_Shift%d_DR%d.jpg', bf_type, curr_date, f_number, sub_shift, dynamic_range));

