% fid = fopen(sprintf('sv_firing_%d_rx_signal_2557x1024.data', firing_id), 'w');
% fwrite(fid, rx_signal', 'double');
% fclose(fid);
%
%
%
% fid = fopen(sprintf('sv_firing_%d_interp_sig_rx_signal_10228x1024.data', firing_id), 'w');
% fwrite(fid, interp_sig', 'double');
% fclose(fid);



% fid = fopen('apd_win_x_20x1.data', 'w');
% fwrite(fid, apd_win_x', 'double');
% fclose(fid);
%
%
%
% fid = fopen('apd_win_y_20x1.data', 'w');
% fwrite(fid, apd_win_y', 'double');
% fclose(fid);



% fid = fopen('tau_a_idx.data', 'w');
% fwrite(fid, tau_a_idx', 'double');
% fclose(fid);



% fid = fopen('interp_sig_selected_1_1_1.data', 'w');
% fwrite(fid, test', 'double');
% fclose(fid);

% fid = fopen('tau_idx_1_1_1.data', 'w');
% fwrite(fid, tau_idx', 'double');
% fclose(fid);

% fid = fopen('tau_idx_bitstream_1_1_1.data', 'w');
% fwrite(fid, asdf', 'uint32');
% fclose(fid);

% fid = fopen('tau_idx_bitstream_1_1.data', 'w');
% fwrite(fid, qwerty', 'uint32');
% fclose(fid);

% fid = fopen('tau_idx_bitstream_1_1TEST.data', 'w');
% fwrite(fid, qwerty, 'uint32');
% fclose(fid);

% fid = fopen('tau_a_bitstream_1_32.data', 'w');
% fwrite(fid, qwerty, 'uint32');
% fclose(fid);

% fid = fopen(sprintf('sv_firing_%d_tau_a_tx1.data', firing_id), 'w');
% fwrite(fid, cleo, 'uint32');
% fclose(fid);
%
% fid = fopen(sprintf('sv_firing_%d_tau_a_rx_tx2.data', firing_id), 'w');
% fwrite(fid, qwerty, 'uint32');
% fclose(fid);


% fid = fopen('test.data', 'wb');
% fwrite(fid, swapbytes(test'), 'int32');
% fclose(fid);
%
win_handle = @(N)kaiser(N, 4.0);
apod_consts = zeros(32, 32);
apod_consts(1:sys_params.sub_size_x, :) = repmat(win_handle(sys_params.sub_size_x), 1, 32);
apod_consts_fixed_point = fi(apod_consts, true, 12, 11);
apod_consts_fixed_point = reinterpretcast(apod_consts_fixed_point, numerictype(true, 12, 0));
apod_consts_fixed_point = int32(apod_consts_fixed_point);
% ** NOTE: MUST MANUALLY ZERO OUT APOD CONSTANTS WHICH ARE NOT NECESSARY
% BEFORE CONTINUING PAST THIS POINT
fid = fopen('sv_stg1_apod_consts_fixed_point.data', 'w');
fwrite(fid, swapbytes(apod_consts_fixed_point), 'int32');
fclose(fid);
fid = fopen('sv_stg2_apod_consts_fixed_point.data', 'w');
fwrite(fid, swapbytes(apod_consts_fixed_point), 'int32');
fclose(fid);

% %%%%%%%%%% Fixed Point Stuff %%%%%%%%%%
% fi_cfg.mode = 'sep_fixed';
% fi_cfg.type = 5;
% fi_cfg = fixed_point_cfg(fi_cfg);
%
% partial_beamsum_fixed = fi(partial_beamsum, true, fi_cfg.bf_stg1_out_q.format(1), fi_cfg.bf_stg1_out_q.format(2));
% partial_beamsum_fixed = reinterpretcast(partial_beamsum_fixed, numerictype(true, fi_cfg.bf_stg1_out_q.format(1), 0));
% fid = fopen(sprintf('CYST_HARMONIC_5_ANGLE_output', firing_id), 'a');
% for scanline_y = 1:sys_params.rx_size_y
%     scanline = squeeze(partial_beamsum_fixed(:, :, scanline_y));
%     scanline_barrel_shift(:, 1:12) = scanline(:, 11:22);
%     scanline_barrel_shift(:, 13:22) = scanline(:, 23:32);
%     scanline_barrel_shift(:, 23:32) = scanline(:, 1:10);
%     for idx_y = 1:size(scanline_barrel_shift, 2)
%         for idx_x = 1:size(scanline_barrel_shift, 1)
%             fprintf(fid, "%s\n", int2str(scanline_barrel_shift(idx_x, idx_y)));
%         end
%     end
% end
% sub_image_fixed = fi(sub_image, true, fi_cfg.bf_stg2_out_q.format(1), fi_cfg.bf_stg2_out_q.format(2));
% sub_image_fixed = reinterpretcast(sub_image_fixed, numerictype(true, fi_cfg.bf_stg2_out_q.format(1), 0));
% for scanline_x = 1:sys_params.rx_size_x
%     scanline2 = squeeze(sub_image_fixed(:, scanline_x, :));
%     scanline2_barrel_shift(:, 1:12) = scanline2(:, 11:22);
%     scanline2_barrel_shift(:, 13:22) = scanline2(:, 23:32);
%     scanline2_barrel_shift(:, 23:32) = scanline2(:, 1:10);
%     for idx_y = 1:size(scanline2_barrel_shift, 2)
%         for idx_x = 1:size(scanline2_barrel_shift, 1)
%             fprintf(fid, "%s\n", int2str(scanline2_barrel_shift(idx_x, idx_y)));
%         end
%     end
% end
% fclose(fid);



%%%%%%%%%% Fixed Point Stuff %%%%%%%%%%
fi_cfg.mode = 'sep_fixed';
fi_cfg.type = 5;
fi_cfg = fixed_point_cfg(fi_cfg);

cleo = uint32(round((tau_a_tx1_offsets)*interp_factor));
rx_signal_fixed_point = fi(rx_signal, true, fi_cfg.input_q.format(1), fi_cfg.input_q.format(2));
rx_signal_fixed_point = reinterpretcast(rx_signal_fixed_point, numerictype(true, fi_cfg.input_q.format(1), 0));
rx_signal_fixed_point = int32(rx_signal_fixed_point);
for slice = 1:32
    rx_signal_fixed_point_slice = rx_signal_fixed_point(:, (1+(slice-1)*32):(slice*32));
    fid = fopen(sprintf('sv_firing_%d_stg1_input_data_SLICE_%d.data', firing_id, slice), 'w');
    fwrite(fid, swapbytes(rx_signal_fixed_point_slice'), 'int32');
    fclose(fid);

    firing_1_tau_a_tx1_SLICE_1 = squeeze(cleo(:, slice, :));
    % firing_1_tau_a_tx1_SLICE_1_barrel_shift(1:12, :) = firing_1_tau_a_tx1_SLICE_1(11:22, :);
    % firing_1_tau_a_tx1_SLICE_1_barrel_shift(13:22, :) = firing_1_tau_a_tx1_SLICE_1(23:32, :);
    firing_1_tau_a_tx1_SLICE_1_barrel_shift(1:(sys_params.rx_size_x-floor(sys_params.sub_size_x/2)), :) = firing_1_tau_a_tx1_SLICE_1((floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x, :);
    firing_1_tau_a_tx1_SLICE_1_barrel_shift((sys_params.rx_size_x-floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x, :) = firing_1_tau_a_tx1_SLICE_1(1:floor(sys_params.sub_size_x/2), :);
    fid = fopen(sprintf('sv_firing_%d_stg1_tau_tx1_SLICE_%d.data', firing_id, slice), 'w');
    fwrite(fid, swapbytes(firing_1_tau_a_tx1_SLICE_1_barrel_shift'), 'uint32');
    fclose(fid);
end

bob = round(tau_1*interp_factor + filter_delay);
qwerty = uint32(zeros(size(interp_sig, 1), sys_params.sub_size_x));
for j = 1:sys_params.sub_size_x
    qwerty(bob(:, j), j) = 1;
end
% output padded to be evenly divisible by 64 for tau_rx_tx2 SRAM
output = uint32(zeros(ceil((size(qwerty, 1) * size(qwerty, 2))/64) * 64, 1));
inner_count = 1;
outer_count = 1;
for row = 1:(size(qwerty, 1)/4)
    for col = 1:size(qwerty, 2)
        output(inner_count) = qwerty(outer_count, col);
        output(inner_count+1) = qwerty(outer_count+1, col);
        output(inner_count+2) = qwerty(outer_count+2, col);
        output(inner_count+3) = qwerty(outer_count+3, col);
        inner_count = inner_count + 4;
    end
    outer_count = outer_count + 4;
end
fid = fopen(sprintf('sv_firing_%d_stg1_tau_rx_tx2.data', firing_id), 'w');
fwrite(fid, swapbytes(output), 'uint32');
fclose(fid);

partial_beamsum_fixed = fi(partial_beamsum, true, fi_cfg.bf_stg1_out_q.format(1), fi_cfg.bf_stg1_out_q.format(2));
partial_beamsum_fixed = reinterpretcast(partial_beamsum_fixed, numerictype(true, fi_cfg.bf_stg1_out_q.format(1), 0));
for scanline_y = 1:sys_params.rx_size_y
    scanline = squeeze(partial_beamsum_fixed(:, :, scanline_y));
    % scanline_barrel_shift(:, 1:12) = scanline(:, 11:22);
    % scanline_barrel_shift(:, 13:22) = scanline(:, 23:32);
    scanline_barrel_shift(:, 1:(sys_params.rx_size_x-floor(sys_params.sub_size_x/2))) = scanline(:, (floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x);
    scanline_barrel_shift(:, (sys_params.rx_size_x-floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x) = scanline(:, 1:floor(sys_params.sub_size_x/2));
    fid = fopen(sprintf('sv_firing_%d_stg1_output_SLICE%d', firing_id, scanline_y), 'w');
    for idx_y = 1:size(scanline_barrel_shift, 2)
        for idx_x = 1:size(scanline_barrel_shift, 1)
            fprintf(fid, "%s\n", int2str(scanline_barrel_shift(idx_x, idx_y)));
        end
    end
    fclose(fid);
end



cleo = uint32(round((tau_b_tx1_offsets)*interp_factor));
partial_beamsum_fixed_point = fi(partial_beamsum, true, fi_cfg.bf_stg1_out_q.format(1), fi_cfg.bf_stg1_out_q.format(2));
partial_beamsum_fixed_point = reinterpretcast(partial_beamsum_fixed_point, numerictype(true, fi_cfg.bf_stg1_out_q.format(1), 0));
partial_beamsum_fixed_point = int32(partial_beamsum_fixed_point);
for slice = 1:32
    partial_beamsum_fixed_point_slice = squeeze(partial_beamsum_fixed_point(:, slice, :));
    fid = fopen(sprintf('sv_firing_%d_stg2_input_data_SLICE_%d.data', firing_id, slice), 'w');
    fwrite(fid, swapbytes(partial_beamsum_fixed_point_slice'), 'int32');
    fclose(fid);

    firing_1_tau_a_tx1_SLICE_1 = squeeze(cleo(slice, :, :));
    % firing_1_tau_a_tx1_SLICE_1_barrel_shift(1:12, :) = firing_1_tau_a_tx1_SLICE_1(11:22, :);
    % firing_1_tau_a_tx1_SLICE_1_barrel_shift(13:22, :) = firing_1_tau_a_tx1_SLICE_1(23:32, :);
    firing_1_tau_a_tx1_SLICE_1_barrel_shift(1:(sys_params.rx_size_x-floor(sys_params.sub_size_x/2)), :) = firing_1_tau_a_tx1_SLICE_1((floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x, :);
    firing_1_tau_a_tx1_SLICE_1_barrel_shift((sys_params.rx_size_x-floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x, :) = firing_1_tau_a_tx1_SLICE_1(1:floor(sys_params.sub_size_x/2), :);
    fid = fopen(sprintf('sv_firing_%d_stg2_tau_tx1_SLICE_%d.data', firing_id, slice), 'w');
    fwrite(fid, swapbytes(firing_1_tau_a_tx1_SLICE_1_barrel_shift'), 'uint32');
    fclose(fid);
end

bob = round(tau_2*interp_factor);
qwerty = uint32(zeros(size(interp_partial_beamsum, 1), sys_params.sub_size_y));
for j = 1:sys_params.sub_size_y
    qwerty(bob(:, j), j) = 1;
end
% output padded to be evenly divisible by 64 for tau_rx_tx2 SRAM
output = uint32(zeros(ceil((size(qwerty, 1) * size(qwerty, 2))/64) * 64, 1));
inner_count = 1;
outer_count = 1;
for row = 1:(size(qwerty, 1)/4)
    for col = 1:size(qwerty, 2)
        output(inner_count) = qwerty(outer_count, col);
        output(inner_count+1) = qwerty(outer_count+1, col);
        output(inner_count+2) = qwerty(outer_count+2, col);
        output(inner_count+3) = qwerty(outer_count+3, col);
        inner_count = inner_count + 4;
    end
    outer_count = outer_count + 4;
end
fid = fopen(sprintf('sv_firing_%d_stg2_tau_rx_tx2.data', firing_id), 'w');
fwrite(fid, swapbytes(output), 'uint32');
fclose(fid);

sub_image_fixed = fi(sub_image, true, fi_cfg.bf_stg2_out_q.format(1), fi_cfg.bf_stg2_out_q.format(2));
sub_image_fixed = reinterpretcast(sub_image_fixed, numerictype(true, fi_cfg.bf_stg2_out_q.format(1), 0));
for scanline_x = 1:sys_params.rx_size_x
    scanline2 = squeeze(sub_image_fixed(:, scanline_x, :));
    % scanline2_barrel_shift(:, 1:12) = scanline2(:, 11:22);
    % scanline2_barrel_shift(:, 13:22) = scanline2(:, 23:32);
    scanline2_barrel_shift(:, 1:(sys_params.rx_size_x-floor(sys_params.sub_size_x/2))) = scanline2(:, (floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x);
    scanline2_barrel_shift(:, (sys_params.rx_size_x-floor(sys_params.sub_size_x/2)+1):sys_params.rx_size_x) = scanline2(:, 1:floor(sys_params.sub_size_x/2));
    fid = fopen(sprintf('sv_firing_%d_stg2_output_SLICE%d', firing_id, scanline_x), 'w');
    for idx_y = 1:size(scanline2_barrel_shift, 2)
        for idx_x = 1:size(scanline2_barrel_shift, 1)
            fprintf(fid, "%s\n", int2str(scanline2_barrel_shift(idx_x, idx_y)));
        end
    end
    fclose(fid);
end



% for sheet=1:32
%     xlswrite(sprintf('firing_%d_tau_b_tx1_along_x.xlsx', firing_id), squeeze(tau_b_tx1_offsets(sheet, :, :)), sheet);
% end
% for sheet=1:32
%     xlswrite(sprintf('firing_%d_tau_b_tx1_along_y.xlsx', firing_id), squeeze(tau_b_tx1_offsets(:, sheet, :)), sheet);
% end