function set_params
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script sets global parameters for a simulation.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m = 1;
s = 1;
cm = 1/100;
mm = 1/1000;
MHz = 1e6;

%%%%%%%%%% Physical Paramseters %%%%%%%%%%

phys_params.interp_factor = 4;

phys_params.fc = 4*MHz;
phys_params.Tc = 1/phys_params.fc;
phys_params.fs = 40*MHz * phys_params.interp_factor;
phys_params.Ts = 1/phys_params.fs;

phys_params.c = 1540*m/s;
phys_params.lambda = phys_params.c/phys_params.fc;
% phys_params.lambda_x = 1.54*mm;
% phys_params.lambda_x = phys_params.c/phys_params.fc;

% phys_params.B_6db = 4*MHz;
phys_params.B_6db = 2*MHz; % harmonic

%%%%%%%%%% Cyst Parameters %%%%%%%%%%


cyst_params.type = 'sphere';
cyst_params.cyst_num = 3;

switch lower(cyst_params.type)
  case 'point'
    cyst_params.scatters = 0; % number of points for cyst, 0 for psf

    switch cyst_params.cyst_num
        case 1
            cyst_params.phantom_positions = [0 0 13]*mm;

            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 2*cm; % max depth of image; used in img_params
        case 2
            cyst_params.phantom_positions = [0 0 13; 0 0 23]*mm;

            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 3*cm; % depth of image; used in img_params
        case 3
            cyst_params.phantom_positions = [0 0 13; 0 0 23; 0 0 33]*mm;

            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 4*cm; % depth of image; used in img_params
        case 999
            cyst_params.phantom_positions = [0 0 13; 0 0 23; 0 0 33]*mm;

%             img_params.img_min_depth = 2*cm; % min depth of image; used in img_params
            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 3*cm; % depth of image; used in img_params
        case 333
            cyst_params.phantom_positions = [0 0 13; 0 0 23; 0 0 33]*mm;
            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 4*cm; % depth of image; used in img_params
    end
  case 'sphere'
    cyst_params.scatters = 60e3; % number of points for cyst, 0 for psf
%     cyst_params.scatters = 30e3; % number of points for cyst, 0 for psf

    switch cyst_params.cyst_num
        case 1
            cyst_params.cyst_x_size  = 1.4*cm;   %  Width of phantom
            cyst_params.cyst_y_size  = 1.4*cm;   %  Height of phantom
            cyst_params.cyst_z_size  = 3.2*cm;   %  Depth of phantom
            cyst_params.cyst_z_start = 0.8*cm;  %  Start of phantom surface

            cyst_params.cyst_r       = [3]*mm;    %  Radius of cyst [mm]
            cyst_params.cyst_xc      = [0]*cm;     %  Center of cyst in x
            cyst_params.cyst_yc      = [0]*cm;     %  Center of cyst in y
            cyst_params.cyst_zc      = [1.3]*cm;  %  Center of cyst in z
            cyst_params.theta_rotate = 0;
            cyst_params.phi_rotate   = 0;

            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 2*cm; % depth of image; used in img_params
        case 2
            cyst_params.cyst_x_size  = 1.4*cm;   %  Width of phantom
            cyst_params.cyst_y_size  = 1.4*cm;   %  Height of phantom
            cyst_params.cyst_z_size  = 3.2*cm;   %  Depth of phantom
            cyst_params.cyst_z_start = 0.8*cm;  %  Start of phantom surface

            cyst_params.cyst_r       = [3, 3]*mm;    %  Radius of cyst [mm]
            cyst_params.cyst_xc      = [0, 0]*cm;     %  Center of cyst in x
            cyst_params.cyst_yc      = [0, 0]*cm;     %  Center of cyst in y
            cyst_params.cyst_zc      = [1.3, 2.3]*cm;  %  Center of cyst in z
            cyst_params.theta_rotate = 0;
            cyst_params.phi_rotate   = 0;

            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 3*cm; % depth of image; used in img_params
        case 3
            cyst_params.cyst_x_size  = 1.4*cm;   %  Width of phantom
            cyst_params.cyst_y_size  = 1.4*cm;   %  Height of phantom
            cyst_params.cyst_z_size  = 3.2*cm;   %  Depth of phantom
            cyst_params.cyst_z_start = 0.8*cm;  %  Start of phantom surface

            cyst_params.cyst_r       = [3, 3, 3]*mm;    %  Radius of cyst [mm]
            cyst_params.cyst_xc      = [0, 0, 0]*cm;     %  Center of cyst in x
            cyst_params.cyst_yc      = [0, 0, 0]*cm;     %  Center of cyst in y
            cyst_params.cyst_zc      = [1.3, 2.3, 3.3]*cm;  %  Center of cyst in z
            cyst_params.theta_rotate = 0;
            cyst_params.phi_rotate   = 0;

%             img_params.img_min_depth = 2*cm; % min depth of image; used in img_params
            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 4*cm; % depth of image; used in img_params
    end
  case 'cylinder'
    cyst_params.scatters = 60e3; % number of points for cyst, 0 for psf

    switch cyst_params.cyst_num
        case 1
            cyst_params.x_size       = 20*mm;   %  Width of phantom [mm]
            cyst_params.y_size       = 20*mm;   %  Transverse width of phantom [mm]
            cyst_params.z_size       = 20*mm;   %  Height of phantom [mm]
            cyst_params.z_start      = (35-10)*mm;  %  Start of phantom surface [mm];
            cyst_params.cyst_r       = 10/2*mm;
            cyst_params.cyst_xc      = 0*mm;
            cyst_params.cyst_zc      = 35*mm;

            img_params.img_min_depth = 0.77*cm; % min depth of image; used in img_params
            img_params.img_max_depth = 7*cm; % depth of image; used in img_params
    end
  case 'elst'
    cyst_params.scatters = 10e3; % number of points for cyst, 0 for psf

    cyst_params.x_size   = 11*mm;   %  Width of phantom [mm]
    cyst_params.y_size   = 11*mm;   %  Transverse width of phantom [mm]
    cyst_params.z_size   = 12*mm;   %  Height of phantom [mm]
    cyst_params.z_start  = 12*mm;  %  Start of phantom surface [mm];
end
%%%%%%%%%% System Paramseters %%%%%%%%%%

sys_params.tx_size_x          = 128;
sys_params.tx_size_y          = 128;
sys_params.rx_size_x          = 32;
sys_params.rx_size_y          = 32;
sys_params.sub_size_x         = 32;
sys_params.sub_size_y         = 32;
sys_params.enable_fixed_f_num = false;
sys_params.fixed_f_num        = 1.0;

sys_params.tx_width           = 0.45*phys_params.lambda;
sys_params.tx_height          = 0.45*phys_params.lambda;
sys_params.tx_kerf            = 0.05*phys_params.lambda;

sys_params.rx_width           = 0.45*phys_params.lambda;
sys_params.rx_height          = 0.45*phys_params.lambda;
sys_params.rx_kerf            = 0.55*phys_params.lambda;

% sys_params.delta_angle = phys_params.lambda/((sys_params.rx_size_x-1)*(sys_params.rx_width+sys_params.rx_kerf));
sys_params.delta_angle        = 3*pi/180; % 3 Degrees


sys_params.num_angles = 13;

switch lower(sys_params.num_angles)
    case 1
        sys_params.firing_angle_alpha = 0;
        sys_params.firing_angle_beta  = 0;
        sys_params.pin_point_coord    = [0 0 0];
    case 5
        %%%                              1   2       3     4      5
        sys_params.firing_angle_alpha = [0,  1,      1,    1,    1].'*sys_params.delta_angle;
        sys_params.firing_angle_beta  = [0,  0,   pi/2,  pi, pi*3/2].';
        edge_pos_x = (sys_params.tx_width + sys_params.tx_kerf)*sys_params.tx_size_x/2;
        edge_pos_y = (sys_params.tx_height + sys_params.tx_kerf)*sys_params.tx_size_y/2;
        sys_params.pin_point_coord = [      0,            0,       0; % 1
                                       -edge_pos_x,       0,       0; % 2
                                            0,     -edge_pos_y,    0; % 3
                                        edge_pos_x,       0,       0; % 4
                                            0,      edge_pos_y,    0];% 5
    case 9
        %%%                              1   2       3     4      5     6      7    8       9
        sys_params.firing_angle_alpha = [0,  1,      1,    1,    1,     2,     2,   2,      2].'*sys_params.delta_angle;
        sys_params.firing_angle_beta  = [0,  0,   pi/2,  pi, pi*3/2,    0,  pi/2,  pi, pi*3/2].';
        edge_pos_x = (sys_params.tx_width + sys_params.tx_kerf)*sys_params.tx_size_x/2;
        edge_pos_y = (sys_params.tx_height + sys_params.tx_kerf)*sys_params.tx_size_y/2;
        sys_params.pin_point_coord = [      0,            0,       0; % 1
                                       -edge_pos_x,       0,       0; % 2
                                            0,     -edge_pos_y,    0; % 3
                                        edge_pos_x,       0,       0; % 4
                                            0,      edge_pos_y,    0; % 5
                                       -edge_pos_x,       0,       0; % 6
                                            0,     -edge_pos_y,    0; % 7
                                        edge_pos_x,       0,       0; % 8
                                            0,      edge_pos_y,    0]; % 9
    case 13
        %%%                              1   2       3     4      5     6      7    8       9    10    11    12   13
        sys_params.firing_angle_alpha = [0,  1,      1,    1,    1,     2,     2,   2,      2,   3,    3,    3,   3].'*sys_params.delta_angle;
        sys_params.firing_angle_beta  = [0,  0,   pi/2,  pi, pi*3/2,    0,  pi/2,  pi, pi*3/2,   0,  pi/2,  pi, pi*3/2].';
        edge_pos_x = (sys_params.tx_width + sys_params.tx_kerf)*sys_params.tx_size_x/2;
        edge_pos_y = (sys_params.tx_height + sys_params.tx_kerf)*sys_params.tx_size_y/2;
        sys_params.pin_point_coord = [      0,            0,       0; % 1
                                       -edge_pos_x,       0,       0; % 2
                                            0,     -edge_pos_y,    0; % 3
                                        edge_pos_x,       0,       0; % 4
                                            0,      edge_pos_y,    0; % 5
                                       -edge_pos_x,       0,       0; % 6
                                            0,     -edge_pos_y,    0; % 7
                                        edge_pos_x,       0,       0; % 8
                                            0,      edge_pos_y,    0; % 9
                                       -edge_pos_x,       0,       0; % 10
                                            0,     -edge_pos_y,    0; % 11
                                        edge_pos_x,       0,       0; % 12
                                            0,      edge_pos_y,    0];% 13
%     case 17
%         %%%                              1        2   3       4     5      6    7       8      9      10    11     12    13      14   15      16   17
%         sys_params.firing_angle_alpha = [0.5,   0.5, 0.5,    0.5,  1.5,   1.5, 1.5,    1.5,   2.5,   2.5,  2.5,    2.5,  3.5,    3.5, 3.5,    3.5, 0].'*sys_params.delta_angle;
%         sys_params.firing_angle_beta  = [ 0,   pi/2,  pi, pi*3/2,    0,  pi/2,  pi, pi*3/2,     0,   pi/2,  pi, pi*3/2,    0,   pi/2,  pi, pi*3/2, 0].';
%         edge_pos_x = (sys_params.tx_width + sys_params.tx_kerf)*sys_params.tx_size_x/2;
%         edge_pos_y = (sys_params.tx_height + sys_params.tx_kerf)*sys_params.tx_size_y/2;
%         sys_params.pin_point_coord = [ edge_pos_x,       0,       0;   % (x,y,z) coordinates of the reference pin point
%                                             0,     edge_pos_y,    0; % 2
%                                       -edge_pos_x,       0,       0; % 3
%                                             0,    -edge_pos_y,    0; % 4
%                                        edge_pos_x,       0,       0; % 5
%                                             0,     edge_pos_y,    0; % 6
%                                       -edge_pos_x,       0,       0; % 7
%                                             0,    -edge_pos_y,    0; % 8
%                                        edge_pos_x,       0,       0; % 9
%                                             0,     edge_pos_y,    0; % 10
%                                       -edge_pos_x,       0,       0; % 11
%                                             0,    -edge_pos_y,    0; % 12
%                                        edge_pos_x,       0,       0; % 13
%                                             0,     edge_pos_y,    0; % 14
%                                       -edge_pos_x,       0,       0; % 15
%                                             0,    -edge_pos_y,    0; % 16
%                                             0,           0,       0]; % 17
end


%%%%%%%%%% Image Paramseters %%%%%%%%%%

img_params.range_resolution = phys_params.lambda/(2*phys_params.fs/phys_params.interp_factor/phys_params.fc);
img_params.focus = [0 0 1]*1e6;
% img_params.z_range = round([0.77, 5]*cm / img_params.range_resolution)*img_params.range_resolution; % image depth
img_params.z_range = round([img_params.img_min_depth, img_params.img_max_depth] / img_params.range_resolution)*img_params.range_resolution; % image depth
img_params.lateral_interp_rate = 1;

img_params.noise_flag = false;
img_params.atten_flag = true;
img_params.atten_value = 0.7; %dB/[cm*MHz]
img_params.enable_time_gain = true;
img_params.enable_freespace_gain = false;

img_params.win_type = 'kaiser40'; % hamming, blackman or kaiser40

sep_params.sep_z_range = round([0, 6]*cm / img_params.range_resolution)*img_params.range_resolution;
sep_params.sep_phase1_beamsum_cusion = [150 260]; % Number of samples to be calculated outside the z_range in phase 1
sep_params.sep_z_pt = floor((img_params.z_range - sep_params.sep_z_range(1))./img_params.range_resolution)+1;
sep_params.sep_method = 'min_rms';  % Possible values 'ref', 'min_rms' and 'iter'

%%%%%%%%%% Elastography Parameters %%%%%%%%%
elst_params.is_elst_mode = false;   % turn the elasticiy estimation on or off
elst_params.sim_duration = 0.02*s; % COMSOL simulation duration
elst_params.firing_time_interval = 1.1111e-4*s; % firing time interval
elst_params.frame_skip = 5;  % 5 means using only the the first frame of every 5 frames for elasticity estimation
% elst_params.comsol_grid_size = [120, 120, 200]; % the comsol grid is 120-by-120-by-200 in x,y,z
elst_params.comsol_grid_size = [60, 60, 100]; % the comsol grid is 120-by-120-by-200 in x,y,z

%%%%%%%%%% Writing Values %%%%%%%%%%

paths = set_paths();
save(sprintf('%s/global_params.mat', paths.params), 'phys_params', 'cyst_params', 'sys_params', 'img_params', 'sep_params', 'elst_params');
clear paths phys_params cyst_params sys_params img_params sep_params;

