function paths = set_paths()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script sets sets the paths for all scripts. Please put
% all paths needed here and load this file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(strcmp(computer('arch'), 'win64'))
    paths.field = './Field_II_ver_3_24_windows';
elseif(strcmp(computer('arch'), 'glnxa64'))
    paths.field = './Field_II_ver_3_24_linux';
else
    fprintf('Error: only 64-bit versions of Windows and Linux are supported./n');
    return;
end
paths.params = './data/param';
paths.delay = './data/delay_sep';
paths.echo = './data/echos_elst';
paths.image = './data/image_elst';
paths.err_map = './data/err_map';
paths.comsol_data = './data/comsol_data';
paths.elst_data = './data/elst_data';
paths.param_friends = './param_friends';
paths.constant_gen = './constants_gen';
paths.bf_functions = './bf_functions';
paths.elst = './elst';
paths.misc = './misc';